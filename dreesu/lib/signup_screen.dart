import 'package:flutter/material.dart';
import 'package:dreesu/signin_screen.dart';
import 'package:dreesu/api/app_bloc.dart';

class SignUpScreen extends StatefulWidget {
  final AppBloc appBloc;

  const SignUpScreen({Key key, this.appBloc}) : super(key: key);
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  Color _color = Colors.white;

  String _errorMessage;
  bool _isLoading = false;
  String _userNameRegister;
  String _emailRegister;
  String _passwordRegister;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.black,
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 30),
                  alignment: Alignment.topCenter,
                  child: Text(
                    'Sign Up',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.white),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextField(
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/username_icon.png',
                              height: 19.44,
                            ),
                            hintText: 'User Name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: Colors.white)),
                        onChanged: (value) {
                          print(value);
                          _userNameRegister = value;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        // obscureText: true,
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/email_icon.png',
                              width: 17.61,
                            ),
                            hintText: 'Email',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: Colors.white)),
                        onChanged: (value) {
                          print(value);
                          _emailRegister = value;
                        },
                      ),
                      TextField(
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/password_icon.png',
                              width: 17.61,
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: Colors.white)),
                        onChanged: (value) {
                          print(value);
                          _passwordRegister = value;
                        },
                      ),
                      // Container(
                      //     padding: EdgeInsets.only(top: 5, left: 80),
                      //     alignment: Alignment.bottomRight,
                      //     child: Text(
                      //       "",
                      //       style: TextStyle(
                      //           color: Colors.grey[300],
                      //           fontSize: 10,
                      //           fontWeight: FontWeight.w300),
                      //     ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () {
                      widget.appBloc.postRegister(
                          _emailRegister, _userNameRegister, _passwordRegister);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(2)),
                      child: Center(
                          child: Text(
                        'Sign Up',
                        style: TextStyle(color: Colors.white),
                      )),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w200,
                            color: Colors.white,
                            fontSize: 11),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => SignInScreen())),
                        child: Container(
                          height: 12,
                          width: 40,
                          margin: EdgeInsets.only(left: 5, right: 5),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(color: Colors.white))),
                          child: Text('Sign in',
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white,
                                  fontSize: 11)),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
