import 'dart:async';

import 'package:dreesu/intro/intro_2_category.dart';
import 'package:dreesu/screen1/screen1.dart';
import 'package:flutter/material.dart';

class Intro1 extends StatefulWidget {
  final Widget child;

  Intro1({Key key, this.child}) : super(key: key);

  _Intro1State createState() => _Intro1State();
}

class _Intro1State extends State<Intro1> {
  startTime() async {
    var _duration = new Duration(seconds: 5);
    return Timer(_duration, navigationPage);
  }

  Future navigationPage() async {
         Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (_) => Intro2Category()));
  }

  @override
  void initState() {
    super.initState();
    startTime();

  }
  // startTime() async {
  //   var _duration = new Duration(seconds: 5);
  //   return Timer(_duration, navigationPage);
  // }

  // Future navigationPage() async {
  //   Navigator.of(context)
  //       .pushReplacement(MaterialPageRoute(builder: (_) => Intro2Category()));
  // }

  // @override
  // void initState() {
  //   startTime();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Image.asset(
                  'images/dress1_dresu_spash.png',
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 5),
                alignment: Alignment.center,
                child: Image.asset('images/dress3_dresu_splash.png'),
              ),
              Container(
                padding: EdgeInsets.only(top: 58),
                alignment: Alignment.bottomCenter,
                child: Image.asset('images/dress_dresu_spash.png'),
              ),
            ],
          ),
          Container(
            // padding: EdgeInsets.only(top: 10),
            child: Text('Dreesu',style: TextStyle(color: Colors.white,fontSize :20)),
          ),
        ],
      ),
     
    );
  }
}
