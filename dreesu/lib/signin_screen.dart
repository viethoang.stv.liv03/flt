import 'package:dreesu/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:dreesu/api/app_bloc.dart';

class SignInScreen extends StatefulWidget {
  final AppBloc appBloc;

  const SignInScreen({Key key, this.appBloc}) : super(key: key);
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  
  Color _color = Colors.white;

  String _errorMessage;
  bool _isLoading = false;
  String _userName;
  String _email;
  String _password;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.black,
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 30),
                  alignment: Alignment.topCenter,
                  child: Text(
                    'Sign In',
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.white),
                  ),
                ),
                // Align(
                //   child: Image.asset('images/Group 482.png',height: 100,),

                //   alignment: Alignment(0, -0.6),
                // ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextField(
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/username_icon.png',
                              height: 19.44,
                            ),
                            hintText: 'User Name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: Colors.white)),
                        onChanged: (value) {
                          print(value);
                          _userName = value;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/password_icon.png',
                              width: 17.61,
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: Colors.white)),
                        onChanged: (value) {
                          print(value);
                          _password = value;
                        },
                      ),
                      // Container(
                      //     padding: EdgeInsets.only(top: 5, left: 80),
                      //     alignment: Alignment.bottomRight,
                      //     child: Text(
                      //       "",
                      //       style: TextStyle(
                      //           color: Colors.grey[300],
                      //           fontSize: 10,
                      //           fontWeight: FontWeight.w300),
                      //     ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () {
                      widget.appBloc.postLogin(_email, _password);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(2)),
                      child: Center(
                          child: Text(
                        'Sign In',
                        style: TextStyle(color: Colors.white),
                      )),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Dont\'t have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w200,
                            color: Colors.white,
                            fontSize: 11),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => SignUpScreen())),
                        child: Container(
                          height: 12,
                          width: 40,
                          margin: EdgeInsets.only(left: 5, right: 5),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(color: Colors.white))),
                          child: Text('Sign up',
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white,
                                  fontSize: 11)),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
