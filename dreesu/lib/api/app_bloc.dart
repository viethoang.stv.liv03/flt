import 'dart:async';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:dreesu/api/app_state.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AppBloc {
  int check = 0;
  final _appState = BehaviorSubject<AppState>();
  updateUser(AppState state) {
    _appState.add(state);
  }

  Stream<AppState> get appState => _appState.stream;
  bool isLoading = false;
  String _accessTokenLogin;
  Future<bool> saveLogged() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoadingSave =
        (sharedPreferences.getBool('isLoadingSave') ?? isLoading);
    if (check == 1) isLoadingSave = isLoading;
    await sharedPreferences.setBool('isLoadingSave', isLoadingSave);
    return isLoadingSave;
  }

  Future<String> saveaccessToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String saveAccess =
        (sharedPreferences.getString('saveAccess') ?? _accessTokenLogin);
    if (check == 1) saveAccess = _accessTokenLogin;
    print(check);
    await sharedPreferences.setString('saveAccess', saveAccess);
    return saveAccess;
  }

  Future<String> postLogin(String email, String password) async {
    var response =
        await http.post("http://dreesu.stdiohue.com/api/v1/login", headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, body: {
      'grant_type': 'password',
      'client_id': '2',
      'client_secret': '0V25NYfBGo6oRtZx7y9MFs2vJD6QCIVTBSZXbghL',
      'email': 'member1@gmail.com',
      'password': 'member'
    });
    if (response.statusCode == 200) {
      if (json.decode(response.body)["message"] == "success") {
        _accessTokenLogin =
            await json.decode(response.body)["data"]["access_token"];
        isLoading = true;
        check = 1;
        saveLogged();
        saveaccessToken();
      } else {
        isLoading = false;
        check = 1;
        saveLogged();
      }
      updateUser(AppState(isLoading));
      return _accessTokenLogin;
    }
    updateUser(AppState(isLoading));
    return Future.error("error");
  }

  String _accessTokenRegister;
  Future<String> postRegister(String email, String name, String password) async {
    var response =
        await http.post("http://dreesu.stdiohue.com/api/v1/register", headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }, body: {
      'email': 'member8@gmail.com',
      'password': 'member',
      'client_id': '2',
      'name': 'member1',
      'client_secret': '0V25NYfBGo6oRtZx7y9MFs2vJD6QCIVTBSZXbghL',
    });
    if (response.statusCode == 200) {
      if (json.decode(response.body)["message"] == "success") {
        _accessTokenRegister =
            json.decode(response.body)["data"]["access_token"];
        isLoading = true;
        check = 1;
        saveLogged();
      } else {
        isLoading = false;
        check = 1;
        saveLogged();
      }
      updateUser(AppState(isLoading));
      return _accessTokenRegister;
    }
    return Future.error("error");
  }

  bool getIsLoading() {
    saveLogged().then((value) {
      isLoading = value;
    });
    saveLogged();
    return isLoading;
  }

  String getAccessToken() {
    saveaccessToken().then((onValue) {
      _accessTokenLogin = onValue;
    });
    print(_accessTokenLogin);
    saveaccessToken().then((onValue) {
      _accessTokenLogin = onValue;
    });
    return _accessTokenLogin;
  }
}
