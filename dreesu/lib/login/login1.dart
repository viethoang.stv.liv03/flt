import 'package:dreesu/login/login1_birthday.dart';
import 'package:flutter/material.dart';

class Login1 extends StatefulWidget {
  Login1State createState() => Login1State();
}

class Login1State extends State<Login1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 80),
                alignment: Alignment.center,
                child: Image.asset(
                  'images/dress1_dresu_spash.png',
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 85),
                alignment: Alignment.center,
                child: Image.asset('images/dress3_dresu_splash.png'),
              ),
              Container(
                padding: EdgeInsets.only(top: 138, bottom: 30),
                alignment: Alignment.bottomCenter,
                child: Image.asset('images/dress_dresu_spash.png'),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 0),
            child: Text(
              'Sign up to view and exchange',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 55),
            child: Text(
              'with other Dreesu members.',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.all(30),
            alignment: Alignment.center,
            height: 55,
            color: Colors.black,
            child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white30,
                ),
                child: FlatButton(
                  child: Text(
                    'Sign up with phone or email',
                    style: TextStyle(
                        color: Colors.amber,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => Login1_Birthday()));
                  },
                )),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Image.asset('images/Line 97.png'),
                padding: EdgeInsets.only(right: 10),
              ),
              Container(
                child: Text('OR',
                    style: TextStyle(color: Colors.white, fontSize: 17)),
                padding: EdgeInsets.all(10),
              ),
              Container(
                child: Image.asset('images/Line 97.png'),
                padding: EdgeInsets.only(left: 10),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Image.asset('images/Group 521.png'),
                padding: EdgeInsets.all(20),
              ),
              Container(
                child: Image.asset('images/Group 522.png'),
                padding: EdgeInsets.all(20),
              ),
              Container(
                child: Image.asset('images/Group 575.png'),
                padding: EdgeInsets.all(20),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(top: 20),
            child: Text(
              'By signing up you confirm that you agree',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 0),
            child: Text(
              'to our Terms of Use and have read and',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 20),
            child: Text(
              'understood Our Privacy Policy.',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            // margin: EdgeInsets.all(30),
            alignment: Alignment.center,
            height: 45,
            color: Colors.black,
            child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                  color: Colors.white30,
                ),
                child: FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        ),
                      ),
                      Text(
                        'Login',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            decoration: TextDecoration.underline),
                      ),
                    ],
                  ),
                  onPressed: () {},
                )),
          ),
        ],
      ),
    );
  }
}
