import 'package:flutter/material.dart';

class CmScreen extends StatefulWidget {
  @override
  CmState createState() {
    return CmState();
  }
}

class CmState extends State<CmScreen> {
  @override
  Widget _buildRow(String text) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              color: Colors.blue,
              fontSize: 10.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text("..."),
        ),
        body: new ListView(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(bottom: 10.0, right: 10.0, left: 10.0),
                child: new Card(
                  child: new Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://www.w3schools.com/howto/img_forest.jpg",
                            scale: 2.0),
                        radius: 15.0,
                      ),
                      Material(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(15.0)),
                        child: MaterialButton(
                          minWidth: 15.0,
                          padding: EdgeInsets.all(0.0),
                          child: _buildRow("Name"),
                          onPressed: () {},
                        ),
                      ),
                      Text(
                        'Hello',
                        style: TextStyle(fontSize: 10.0),
                      )
                    ],
                  ),
                ))
          ],
        ));
  }
}
