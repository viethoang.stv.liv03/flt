import 'package:flutter/material.dart';
import 'package:welcome_app/group.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text("Login"),
        ),
        body: new Stack(
          children: <Widget>[
            new Container(
              child: Center(
                child: Column(
                  children: <Widget>[
                    Image.asset('images/connect3.jpg', fit: BoxFit.contain),
                    Container(
                      child: Material(
                        child: RaisedButton(
                            padding: EdgeInsets.only(
                                left: 50.0, right: 50.0, top: 0.0, bottom: 0.0),
                            color: Colors.blue,
                            child: Text(
                              'Sign in with FaceBook',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 15.0),
                            ),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => GroupScreen()),
                              );
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(15.0))),
                      ),
                    ),
                    Container(
                      child: Material(
                        child: RaisedButton(
                            padding: EdgeInsets.only(
                                left: 60.0, right: 60.0, top: 0.0, bottom: 0.0),
                            color: Colors.red,
                            child: Text(
                              'Sign in with Google',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 15.0),
                            ),
                            onPressed: () {},
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(15.0))),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
