import 'package:flutter/material.dart';

class PostScreen extends StatefulWidget {
  @override
  PostState createState() {
    return PostState();
  }
}

class PostState extends State<PostScreen> {
  TextEditingController _textTextController;

  @override
  void initState() {
    super.initState();
    _textTextController = new TextEditingController();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text("..."),
        ),
        body: new ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 10.0, right: 10.0, left: 10.0),
              child: new Column(
                children: <Widget>[
                  new Card(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                      child: new Column(
                        children: <Widget>[
                          TextField(
                            maxLines: null,
                            keyboardType: TextInputType.multiline,
                            controller: _textTextController,
                            decoration: InputDecoration(
                                icon: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      "https://www.w3schools.com/howto/img_forest.jpg",
                                      scale: 2.0),
                                  radius: 15.0,
                                ),
                                hintText: 'What do you think?',
                                counterStyle: TextStyle(fontSize: 10.0),
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                suffixIcon: IconButton(
                                  icon: Icon(Icons.tag_faces),
                                  onPressed: () {},
                                  color: Colors.blue,
                                  iconSize: 20.0,
                                )),
                          ),
                          new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 18.0,
                                  margin: EdgeInsets.only(
                                      left: 2.0,
                                      right: 2.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                      minWidth: 20.0,
                                      padding: EdgeInsets.only(
                                        left: 25.0,
                                        right: 25.0,
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.book,
                                            size: 10.0,
                                            color: Colors.red,
                                          ),
                                          Text(
                                            ' aaa ',
                                            style: TextStyle(
                                                color: Colors.blue,
                                                fontSize: 10.0),
                                          ),
                                        ],
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 18.0,
                                  margin: EdgeInsets.only(
                                      left: 2.0,
                                      right: 2.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                      minWidth: 20.0,
                                      padding: EdgeInsets.only(
                                        left: 25.0,
                                        right: 25.0,
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.ac_unit,
                                            size: 10.0,
                                            color: Colors.blue,
                                          ),
                                          Text(
                                            ' bbb ',
                                            style: TextStyle(
                                                color: Colors.blue,
                                                fontSize: 10.0),
                                          ),
                                        ],
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 18.0,
                                  margin: EdgeInsets.only(
                                      left: 2.0,
                                      right: 2.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                      minWidth: 20.0,
                                      padding: EdgeInsets.only(
                                        left: 25.0,
                                        right: 25.0,
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.person_add,
                                            size: 10.0,
                                            color: Colors.greenAccent,
                                          ),
                                          Text(
                                            ' ccc ',
                                            style: TextStyle(
                                                color: Colors.blue,
                                                fontSize: 10.0),
                                          ),
                                        ],
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 18.0,
                                  margin: EdgeInsets.only(
                                      left: 2.0,
                                      right: 2.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                      minWidth: 18.0,
                                      padding: EdgeInsets.only(
                                        left: 2.0,
                                        right: 2.0,
                                      ),
                                      child: Text(
                                        '...',
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 15.0),
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                ),
                              ]),
                          new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 15.0,
                                  margin: EdgeInsets.only(
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                        minWidth: 20.0,
                                        padding: EdgeInsets.only(
                                          left: 25.0,
                                          right: 25.0,
                                        ),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.settings,
                                              size: 10.0,
                                              color: Colors.grey,
                                            ),
                                            Text(
                                              ' View more ',
                                              style: TextStyle(
                                                  color: Colors.blue,
                                                  fontSize: 10.0),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {},
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    15.0))),
                                  ),
                                ),
                                Container(
                                  height: 15.0,
                                  margin: EdgeInsets.only(
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 5.0,
                                      top: 5.0),
                                  child: Material(
                                    child: MaterialButton(
                                        minWidth: 20.0,
                                        padding: EdgeInsets.only(
                                          left: 80.0,
                                          right: 80.0,
                                        ),
                                        color: Colors.blueAccent,
                                        child: Text(
                                          'Share',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10.0),
                                        ),
                                        onPressed: () {},
                                        shape: new RoundedRectangleBorder(
                                            borderRadius:
                                                new BorderRadius.circular(
                                                    15.0))),
                                  ),
                                ),
                              ])
                        ],
                      )),
                ],
              ),
            ),
          ],
        ));
  }
}
