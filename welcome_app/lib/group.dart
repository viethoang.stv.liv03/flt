import 'package:flutter/material.dart';
import 'package:welcome_app/add_user.dart';
import 'package:welcome_app/status.dart';

class GroupScreen extends StatefulWidget {
  @override
  GroupScreenState createState() {
    return GroupScreenState();
  }
}

class GroupScreenState extends State<GroupScreen> {
  @override
  Widget _buildRow(String text) {
    return Container(
      padding: EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(text),
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.greenAccent,
          title: Text("Chat Group"),
        ),
        floatingActionButton: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Material(
            child: RaisedButton(
                color: Colors.greenAccent,
                child: Text('Create Group'),
                onPressed: () {},
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15.0))),
          ),
        ),
        body: new ListView(
          children: <Widget>[
            Container(
                height: 50.0,
                padding: EdgeInsets.only(right: 10.0, left: 10.0),
                child: new Card(
                  child: new Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://www.w3schools.com/howto/img_forest.jpg",
                            scale: 2.0),
                        radius: 20.0,
                      ),
                      Material(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(15.0)),
                        child: MaterialButton(
                          minWidth: 280.0,
                          padding: EdgeInsets.all(0.0),
                          child: _buildRow("Forest"),
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => PostScreen()),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                )),
            Container(
                height: 50.0,
                padding: EdgeInsets.only(right: 10.0, left: 10.0),
                child: new Card(
                  child: new Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://www.w3schools.com/howto/img_snow_wide.jpg",
                            scale: 2.0),
                        radius: 20.0,
                      ),
                      Material(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(15.0)),
                        child: MaterialButton(
                          minWidth: 280.0,
                          padding: EdgeInsets.all(0.0),
                          child: _buildRow("The Snow"),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                )),
          ],
        ));
  }

  Future<bool> dialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Details'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('I love you so much.'),
                ],
              ),
            ),
          );
        });
  }
}
