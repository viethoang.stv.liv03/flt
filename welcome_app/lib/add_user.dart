import 'package:flutter/material.dart';
import 'package:welcome_app/group.dart';

class AddScreen extends StatefulWidget {
  @override
  AddScreenState createState() {
    return AddScreenState();
  }
}

class AddScreenState extends State<AddScreen> {
  List<String> _names = ['Cuc Cuc', 'Beta', 'Con cho'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text("List User"),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
                color: Colors.greenAccent,
                child: Text('FINISH'),
                onPressed: () {})
          ],
        ),
      ),
      body: new Container(
        child: new ListView.builder(
          itemBuilder: (_, int index) => ListUser(this._names[index]),
          itemCount: this._names.length,
        ),
      ),
    );
  }
}

class ListUser extends StatelessWidget {
  final String name;

  ListUser(this.name);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: RaisedButton(
        padding: EdgeInsets.all(0.0),
        child: new Card(
          child: new Container(
            child: new Row(
              children: <Widget>[
                new CircleAvatar(
                  child: new Text(name[0]),
                  backgroundColor: Colors.blue,
                ),
                new Padding(padding: EdgeInsets.only(right: 10.0)),
                new Text(name)
              ],
            ),
          ),
        ),
        onPressed: () {
          dialog(context);
        },
      ),
    );
  }

  Future<bool> dialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Xác nhận kết bạn?'),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: Text('Có')),
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: Text('Trở về')),
            ],
          );
        });
  }
}
