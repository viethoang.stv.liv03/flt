export 'package:lol/tabs/profile/widget/change_password.dart';
export 'package:lol/tabs/profile/widget/my_address.dart';
export 'package:lol/tabs/profile/widget/orders_history.dart';
export 'package:lol/tabs/profile/widget/add_address.dart';
export 'package:lol/tabs/profile/widget/address_item.dart';
export 'package:lol/tabs/profile/widget/address_action.dart';
export 'package:lol/tabs/profile/widget/shipment_details.dart';
export 'package:lol/tabs/profile/widget/my_address_edit.dart';