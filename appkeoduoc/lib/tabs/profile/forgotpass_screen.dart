import 'package:flutter/material.dart';
import 'package:lol/tabs/tabs.dart';

class ForgotPassScreen extends StatefulWidget {
  ForgotPass forgotPass = ForgotPass();
  ForgotPassScreen({Key key, this.forgotPass}) : super(key: key);
  @override
  _ForgotPassScreenState createState() => _ForgotPassScreenState();
}

class _ForgotPassScreenState extends State<ForgotPassScreen> {
  Color colorIsChoosed = Color.fromARGB(170, 51, 204, 204);
  TextEditingController _emailTextController;

  @override
  void initState() {
    super.initState();
    _emailTextController = new TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text('Quên Mật Khẩu',style: TextStyle(fontSize: 15),),
        backgroundColor: colorIsChoosed,
      ),
      body: Center(
        child: Container(
          height: 500,
          child: Card(
            margin: EdgeInsets.all(7),
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: ListView(
                children: <Widget>[
                  Center(
                      child: Container(
                        child: Text(
                          'QUÊN MẬT KHẨU',
                          style: TextStyle(fontSize: 17),
                        ),
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                      )),
                  Text(
                    'Nếu bạn quên mật khẩu, hãy nhập địa chỉ email của bạn vào ô này và nhấp vào khôi phục mật khẩu. Bạn sẽ nhật được một mật khẩu mới và một liên kết để đăng nhập. Bạn có thể thay đổi sau.',
                    style: TextStyle(fontSize: 14, color: Colors.grey),
                  ),
                  TextFormField(
                    controller: _emailTextController,
                    decoration: InputDecoration(
                      labelText: "Nhập email",
                    ),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 15),
                      child: RaisedButton(
                        elevation: 0,
                        child: Text(
                          'KHÔI PHỤC MẬT KHẨU',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          widget.forgotPass
                              .postForgotPassword(_emailTextController.text)
                              .then((onValue) {
                            showDialog(
                                context: context,
                                child: AddItemDialog(message: onValue));
                          });
                        },

                        color: colorIsChoosed,
                      ),
                      height: 50,
                      width: 400,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}