export 'package:lol/tabs/model/slide.dart';
export 'package:lol/tabs/model/product.dart';
export 'package:lol/tabs/model/categories.dart';
export 'package:lol/tabs/model/categories_menu.dart';
export 'package:lol/tabs/model/address.dart';