import 'package:lol/main/app.dart';
import 'package:lol/main/app_bloc.dart';
import 'package:lol/tabs/tabs.dart';

void main() {
  AppBloc appBloc = AppBloc();
  runApp(new MyApp(
    appBloc: appBloc,
  ));
}