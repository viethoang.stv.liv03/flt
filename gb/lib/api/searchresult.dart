import 'package:http/http.dart' as http;
import 'package:lol/tabs/tabs.dart';
import 'dart:async';
import 'dart:convert';
class FetchSearchResult {
  Future<List<ProductsItem>> fetchSearchResult(String keyword) async {
    Map key = {
      'filter': {'keyword': keyword}
    };
    String data = json.encode(key);
    var response = await http.post(
        "https://groupbuy.vn/api/v1/products-all",
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        body: data);
    if (response.statusCode == 200) {
      if (json.decode(response.body)['message'] == "success") {
        final parsedProducts =
        json.decode(response.body)['data'].cast<Map<String, dynamic>>();
        return parsedProducts
            .map<ProductsItem>((value) => ProductsItem.internalFromJson(value))
            .toList();
      }
    }
    return Future.error("error");
  }
}