import 'dart:async';
import 'package:flutter/material.dart';
import 'package:foura/tab/tab3/foura_s13_tab3.dart';
import 'package:foura/tab/tab1/foura_s3_search.dart';
import 'package:foura/tab/tab2/foura_s8_register.dart';
import 'package:foura/login_reg/foura_s2_signin.dart';
import 'package:foura/tab/tab1/foura_s7_tab1.dart';
import 'package:foura/tab/tab2/foura_s9_tab2.dart';
// import 'package:foura/foura_icon.dart' as CustomIcon;
// import 'package:foura/user_info_class.dart';

class TabsScreen extends StatefulWidget {
  String accessToken;

  TabsScreen({Key key, this.accessToken}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TabsScreenState();
  }
}

class _TabsScreenState extends State<TabsScreen>
    with SingleTickerProviderStateMixin {
  TabController controller;
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  bool isP = false;

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
// int index = 0;
  var iconHeight = 22.5;
  @override
  Widget build(BuildContext context) {
     
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        bottomNavigationBar: Container(
      
          height: 60.0,
          child: TabBar(
            indicatorColor: Colors.white,
            labelColor: _colorB,
            unselectedLabelColor: Colors.grey,
            labelStyle: TextStyle(fontWeight: FontWeight.w400),
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w400),
            controller: controller,
            tabs: <Tab>[
              Tab(
                icon: Container(
                  child: Image.asset(
                    'images/list1.png',
                    scale: 1.6,
                  ),
                  // child: Image.asset('images/list1.png',scale: 1.7,),
                  margin: EdgeInsets.only(bottom: 0.0),
                ),
                // icon: controller.index == 0
                //     ? Image.asset(
                //         'images/list2.png',
                //         height: iconHeight,
                //       )
                //     : Image.asset(
                //         'images/list1.png',
                //         height: iconHeight,
                //       ),
                // icon: Icon(CustomIcon.MyFlutterApp.trophy,size: 24,),
                child: Text('Médicaments', style: TextStyle(fontSize: 11)),
                // text: 'abc',
              ),
              Tab(
                icon: Container(
                  child: Image.asset(
                    'images/arrow1.png',
                    scale: 1.6,
                  ),
                  margin: EdgeInsets.only(bottom: 0.0),
                ),

                child: Text('Petites Annonces',
                    style: TextStyle(fontSize: 11)),
                // icon: controller.index == 1
                //     ? Image.asset(
                //         'images/arrow2.png',
                //         height: iconHeight,
                //       )
                //     : Image.asset(
                //         'images/arrow1.png',
                //         height: iconHeight,
                //       ),
                // text: 'áđá',
              ),
              Tab(
                icon: Container(
                  child: Image.asset(
                    'images/person1.png',
                    scale: 1.6,
                  ),
                  margin: EdgeInsets.only(bottom: 0.0),
                ),
                child: Text('Profil', style: TextStyle(fontSize: 11)),
              )
            ],
          ),
        ),
        body: TabBarView(
          controller: controller,
          children: <Widget>[
            // HomeScreen(),
            S7_2Screen(accessToken: widget.accessToken,),
            // Anl(),
            S9Screen(accessToken: widget.accessToken,),
            LoggedScreen(accessToken: widget.accessToken,)
          //  UserInfoItem()
          ],
        ),
      ),
    );
  }
}
