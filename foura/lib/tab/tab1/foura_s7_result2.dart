import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura/tab/tab1/foura_s8_detail.dart';
class S7_Result2Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return S7_Result2ScreenState();
  }
}

class S7_Result2ScreenState extends State<S7_Result2Screen> {
  TextEditingController search = TextEditingController();
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        elevation: 1,
        backgroundColor: _colorWhite,
        centerTitle: true,
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Foura',
            style: TextStyle(
                fontSize: 40, color: Colors.black54, fontFamily: 'Gugi'),
          ),
        ),
      ),
      body: Column(
        
        children: <Widget>[
          
          Container(
            padding: EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => S8_3Screen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 70,
                decoration: BoxDecoration(
                    color: _colorWhite, borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, bottom: 5, top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Novalgin',
                            style: TextStyle(color: Colors.red, fontSize: 14),
                          ),
                          Text('24/04/2019',
                              style: TextStyle(
                                  color: Colors.black45, fontSize: 12))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 10,
                        right: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('3413257',
                              style: TextStyle(
                                  color: Colors.black45, fontSize: 12)),
                          Text('3.5€',
                              style: TextStyle(
                                  color: Colors.green[300], fontSize: 15))
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 10, right: 10, bottom: 5, top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Cher',
                              style: TextStyle(
                                  color: Colors.green[300], fontSize: 12)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          
        ],
      ),
    );
  }
}
