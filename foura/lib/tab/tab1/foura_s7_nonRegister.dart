import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura/tab/tab1/foura_s8_detail.dart';
class S7_3Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return S7_3ScreenState();
  }
}

class S7_3ScreenState extends State<S7_3Screen> {
  TextEditingController search = TextEditingController();
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: _colorWhite,
          centerTitle: true,
          title: Container(
            padding: EdgeInsets.only(top: 6),
            child: Text(
              'Foura',
              style: TextStyle(
                  fontSize: 40, color: Colors.black54, fontFamily: 'Gugi'),
            ),
          ),
          leading: BackButton(
            color: Colors.grey,
          ),
        ),
        body: ListView(children: <Widget>[
          Card(
            margin: EdgeInsets.all(0),
            child: Container(
              margin: EdgeInsets.all(20),
              child: Text(
                "Novalgin",
                style: TextStyle(
                    fontSize: 25,
                    color: Colors.red,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 1),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Code ACL",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    "3107750",
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Lieu de travail",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    "Cher (cher)",
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Quantité",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    "1",
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Prix",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    "3.5€ soit 2.06 Unité de Paracétamol",
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Date dé péremption",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    "24/04/2019",
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Text('Vous devez étre un abonné actif pour afficher les coordonnées.',
                    style: TextStyle(fontSize: 15, color: Colors.black45),)
          ),
          Container(
            padding: EdgeInsets.only(top: 20, left: 40, right: 40),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).push(
                //     MaterialPageRoute(builder: (_) => SearchResultScreen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                    child: Text(
                  'S\'enregistrer',
                  style: TextStyle(fontSize: 16,color: _colorWhite,fontWeight: FontWeight.bold),
                )),
              ),
            ),
          ),
        ]));
  }
}
