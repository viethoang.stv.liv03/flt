import 'package:flutter/material.dart';
import 'package:foura/tab/tab2/foura_s8_register.dart';
class SearchResultScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchResultScreenState();
  }
}

class SearchResultScreenState extends State<SearchResultScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          elevation: 1,
          backgroundColor: _colorWhite,
          centerTitle: true,
          title: Container(
            padding: EdgeInsets.only(top: 6),
            child: Text(
              'Rechercher',
              style: TextStyle(
                  fontSize: 16, color: Colors.black54,),
            ),
          ),
          leading: BackButton(color: Colors.grey,),
        ),
        body: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(30),
              // alignment: Alignment.center,
              child: Text(
                  'Aucun résultat pour votre recherche. Soyez le premier à soumettre une annonce pour ce département.',style: TextStyle(color: Colors.black54,fontSize: 15),),
            ),
            Container(
            padding: EdgeInsets.only(top: 300, left: 40, right: 40),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => S8_RegisterScreen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(5)),
                child: Center(
                    child: Text(
                  'SOUMETTRE UNE ANNONCE',
                  style: TextStyle(color: _colorWhite,fontWeight: FontWeight.bold),
                )),
              ),
            ),
          ),
          ],
        ));
  }
}
