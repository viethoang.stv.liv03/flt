import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura/api/get_view_ad.dart';
import 'package:foura/api/post_user_info.dart';
import 'package:foura/model/ad.dart';
import 'package:foura/api/app_bloc.dart';

import 'package:foura/model/user.dart';
import 'package:foura/parse_number.dart';

// import 'api/list_ads.dart';

class S8_3Screen extends StatefulWidget {
  String accessToken;

  Ads ads;
  S8_3Screen({this.ads,this.accessToken});
  @override
  State<StatefulWidget> createState() {
    return S8_3ScreenState();
  }
}

class S8_3ScreenState extends State<S8_3Screen> {
  TextEditingController search = TextEditingController();
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  bool loading = true;

  int id;

  User user;
  Future getUser() async {
    await PostUser().getUser(widget.accessToken).then((data) {
      print('name: ' + data.username);

      setState(() {
        user = data;
        loading = false;
      });
    });
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  ParseNumber parse = ParseNumber();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: _colorWhite,
          centerTitle: true,
          title: Container(
            padding: EdgeInsets.only(top: 6),
            child: Text(
              // 'Adynoprox Xenarinex 50mg'
              widget.ads == null ? "" : widget.ads.title,
              style: TextStyle(
                fontSize: 16,
                color: Colors.black87,
              ),
            ),
          ),
          leading: BackButton(
            color: Colors.grey,
          ),
        ),
        body: ListView(children: <Widget>[
          Card(
            margin: EdgeInsets.all(0),
            child: Container(
              margin: EdgeInsets.all(20),
              child: Text(
                widget.ads == null ? "" : widget.ads.title,
                style: TextStyle(
                    fontSize: 22,
                    color: Colors.red,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.only(top: 1),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Code ACL",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    widget.ads.alc_code,
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Lieu de travail",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    widget.ads.workplace_name,
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Quantité",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    widget.ads.amount.toString(),
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Prix",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    widget.ads.price + '€',
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Card(
            margin: EdgeInsets.only(top: 0),
            child: Container(
                child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    top: 15,
                    left: 20,
                    bottom: 5,
                  ),
                  child: Text("Date dé péremption",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(
                    left: 20,
                    bottom: 15,
                  ),
                  child: Text(
                    DateTime.fromMillisecondsSinceEpoch(
                                widget.ads.expire_date * 1000)
                            .toString()
                            .substring(8, 10) +
                        '/' +
                        DateTime.fromMillisecondsSinceEpoch(
                                widget.ads.expire_date * 1000)
                            .toString()
                            .substring(5, 7) +
                        '/' +
                        DateTime.fromMillisecondsSinceEpoch(
                                widget.ads.expire_date * 1000)
                            .toString()
                            .substring(0, 4),
                    style: TextStyle(fontSize: 15, color: Colors.black45),
                  ),
                )
              ],
            )),
          ),
          Container(
              padding: EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Text(
                user == null ? "loading" : user.username,
                style: TextStyle(fontSize: 15, color: Colors.black,
                          fontWeight: FontWeight.bold),
              )),
          Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Vous devez étre un abonné actif pour afficher les coordonnées.',
                style: TextStyle(fontSize: 15, color: Colors.black45),
              )),
          Container(
            padding: EdgeInsets.only(top: 0, left: 40, right: 40),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).push(
                //     MaterialPageRoute(builder: (_) => SearchResultScreen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: _colorB, borderRadius: BorderRadius.circular(10)),
                child: Center(
                    child: Text(
                      user == null ? "loading" : user.phone_number
                  // '83027347'
                  ,
                  style: TextStyle(
                      fontSize: 16,
                      color: _colorWhite,
                      fontWeight: FontWeight.bold),
                )),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10, left: 40, right: 40, bottom: 20),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).push(
                //     MaterialPageRoute(builder: (_) => SearchResultScreen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                    child: Text(
                  'Envoyer un message',
                  style: TextStyle(
                      fontSize: 16,
                      color: _colorWhite,
                      fontWeight: FontWeight.bold),
                )),
              ),
            ),
          ),
        ]));
  }
}
