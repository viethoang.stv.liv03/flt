import 'package:flutter/material.dart';
import 'package:foura/tab/tab1/foura_s7_result1.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchScreenState();
  }
}

class SearchScreenState extends State<SearchScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  int _selectedTrierIndex = 0;
  int _selectedDepartIndex = 0;

  bool isPick = false;
  var _defaultDepartement = 'Tous les départements';
  var _departement = [
    'Tous les départements',
    'Ain',
    'Aisne',
    'Allier',
    'Alpes-de-Haute-Provence',
    'Alpes-Maritimes',
    'Ardèche',
    'Ardennes',
    'Ariège',
    'Aube',
    'Aude',
    'Aveyron',
    'Bas-Rhin',
    'Bouches-du-Rhône',
    'Calvados',
    'Cantal',
    'Charente',
    'Charente-Maritime',
    'Cher',
    'Corrèze',
    'Corse-du-Sud',
    'Côte-d\'Or',
    'Côtes-d\'Armor',
    'Creuse',
    'Deux-Sèvres',
    'Dordogne',
    'Doubs',
    'Drôme',
    'Essonne',
    'Eure',
    'Eure-et-Loir',
    'Finistère',
    'Gard',
    'Gers',
    'Gironde',
    'Haut-Rhin',
    'Haute-Corse',
    'Haute-Garonte',
    'Haute-Loire',
    'Haute-Marne',
    'Haute-Saône',
    'Haute-Savoie',
    'Haute-Vienne',
    'Hautes-Alpes',
    'Hautes-Pyrénées',
    'Hauts-de-Seine',
    'Hérault',
    'Ille-et-Vilaine',
    'Indre',
    'Indre-et-Loire',
    'Isère',
    'Jura',
    'Landes',
    'Loir-et-Cher',
    'Loire',
    'Loire-Atlantique',
    'Loiret',
    'Lot',
    'Lot-et-Garonne',
    'Lozère',
    'Maine-et-Loire',
    'Manche',
    'Marne',
    'Mayenne',
    'Meurthe-et-Moselle',
    'Meuse',
    'Morbihan',
    'Moselle',
    'Nièvre',
    'Nord',
    'Oise',
    'Orne',
    'Pas-de-Calais',
    'Puy-de-Dôme',
    'Pyrénées-Atlantiques',
    'Pyrénées-Orientales',
    'Rhône',
    'Saône-et-Loire',
    'Sarthe',
    'Savoie',
    'Seine-et-Marne',
    'Seine-Maritime',
    'Seine-Saint-Denis',
    'Somme',
    'Tarn',
    'Tarn-et-Garonne',
    'Territoire de Belfort',
    'Val-d\'Oise',
    'Val-de-Marne',
    'Var',
    'Vaucluse',
    'Vendée',
    'Vienne',
    'Ville de Paris',
    'Vosges',
    'Yonne',
    'Yvelines',
  ];

  var _defaultTrier = 'Trier par';
  var _trier = [
    'Trier par',
    'Prix',
    'Date de Publication',
  ];
  @override
  Widget build(BuildContext context) {
    final FixedExtentScrollController scrollController1 =
        FixedExtentScrollController(initialItem: _selectedDepartIndex);
    final FixedExtentScrollController scrollController2 =
        FixedExtentScrollController(initialItem: _selectedTrierIndex);
    TextEditingController search = TextEditingController();
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          elevation: 1,
          backgroundColor: _colorWhite,
          centerTitle: true,
          title: Container(
            padding: EdgeInsets.only(top: 6),
            child: Text(
              'Rechercher',
              style: TextStyle(
                fontSize: 16,
                color: Colors.black87,
              ),
            ),
          ),
          leading: BackButton(
            color: Colors.grey,
          ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white),
              margin: EdgeInsets.only(
                  top: 20, left: 10.0, right: 10.0, bottom: 5.0),
              height: 50.0,
              child: TextField(
                  controller: search,
                  onSubmitted: null,
                  decoration: (InputDecoration(
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      Icons.search,
                      size: 20.0,
                    ),
                    hintText: 'Nom du produit, code ACL',
                    hintStyle: TextStyle(fontSize: 13.0),
                    contentPadding: EdgeInsets.only(top: 15, left: 5),
                  ))),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(10.0),
                  color: _colorWhite),
              margin: EdgeInsets.only(
                  top: 20, left: 10.0, right: 10.0, bottom: 5.0),
              height: 50.0,
              child: ListTile(
                title: GestureDetector(
                  onTap: () async {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          height: 250,
                          child: CupertinoPicker(
                            scrollController: scrollController1,
                            itemExtent: 40,
                            backgroundColor: CupertinoColors.white,
                            onSelectedItemChanged: (int index) {
                              setState(() => _selectedDepartIndex = index);
                            },
                            children: List<Widget>.generate(_departement.length,
                                (int index) {
                              return Center(
                                child: Text(
                                  _departement[index],
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              );
                            }),
                          ),
                        );
                      },
                    );
                  },
                  child: _buildMenu(
                    <Widget>[
                      // Text(
                      //   'Départements',
                      //   style: TextStyle(color: Colors.black54),
                      // ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 260,
                            child: Text(
                              _departement[_selectedDepartIndex],
                              style: const TextStyle(
                                  color: CupertinoColors.inactiveGray),
                            ),
                          ),
                          Icon(
                            Icons.arrow_drop_down,
                            color: Colors.black54,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.white),
                  borderRadius: BorderRadius.circular(10.0),
                  color: _colorWhite),
              margin: EdgeInsets.only(
                  top: 20, left: 10.0, right: 10.0, bottom: 5.0),
              height: 50.0,
              child: ListTile(
                title: GestureDetector(
                  onTap: () async {
                    showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          height: 150,
                          // color: _colorWhite,
                          child: CupertinoPicker(
                            scrollController: scrollController2,
                            itemExtent: 40,
                            backgroundColor: CupertinoColors.white,
                            onSelectedItemChanged: (int index) {
                              setState(() => _selectedTrierIndex = index);
                            },
                            children: List<Widget>.generate(_trier.length,
                                (int index) {
                              return Center(
                                child: Text(
                                  _trier[index],
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 14),
                                ),
                              );
                            }),
                          ),
                        );
                      },
                    );
                  },
                  child: _buildMenu(
                    <Widget>[
                      // Text(
                      //   'Départements',
                      //   style: TextStyle(color: Colors.black54),
                      // ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 260,
                            child: Text(
                              _trier[_selectedTrierIndex],
                              style: const TextStyle(
                                  color: CupertinoColors.inactiveGray),
                            ),
                          ),
                          Icon(
                            Icons.arrow_drop_down,
                            color: Colors.black54,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 250, left: 40, right: 40),
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => SearchResultScreen()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  decoration: BoxDecoration(
                      color: _colorB, borderRadius: BorderRadius.circular(5)),
                  child: Center(
                      child: Text(
                    'RECHERCHER',
                    style: TextStyle(color: _colorWhite),
                  )),
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: SafeArea(
          top: true,
          bottom: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }
}
