// import 'dart:convert';
import 'package:foura/api/get_view_ad.dart';
import 'package:foura/api/post_list_ads.dart';
// import 'package:foura/api/user_info.dart';
import 'package:foura/model/user.dart';
// import 'package:http/http.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foura/api/app_bloc.dart';
import 'package:foura/tab/tab1/foura_s8_detail.dart';
import 'package:foura/tab/tab1/foura_s3_search.dart';
// import 'package:foura/api/list_ads.dart';
import 'package:foura/model/ad.dart';
import 'package:foura/parse_number.dart';
import 'package:loadmore/loadmore.dart';

class S7_2Screen extends StatefulWidget {
  String accessToken;
  int id;
  S7_2Screen({Key key, this.accessToken, this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return S7_2ScreenState();
  }
}

class S7_2ScreenState extends State<S7_2Screen> {
  TextEditingController search = TextEditingController();
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  ParseNumber parse = ParseNumber();
  int get count => listAd.length;
  bool loading = true;
  bool _isLoading = false;
  List<Ads> listAd;
  String accessToken;
  
  Widget showCircularProgress() {
    if (_isLoading) {
      print('aaaaaaaaaaaaaaaa');
      return Center(
          child: CircularProgressIndicator(
        backgroundColor: Colors.white,
      ));
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Future getListAd() async {
    await ListAds().getListAds().then((data) {
      setState(() {
        listAd = data;
      });
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    getListAd();
    super.initState();
  }

  // bool isEmpty = false;
  // Future<bool> _loadMore() async {
  //   print("onLoadMore");
  //   await Future.delayed(Duration(seconds: 0, milliseconds: 2000));
  //   getListAd();
  //   return true;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        elevation: 1,
        backgroundColor: _colorB,
        centerTitle: true,
        title: Container(
          child:
              // Text(
              //   'Foura',
              //   style: TextStyle(
              //       fontSize: 40, color: Colors.black54, fontFamily: 'Gugi'),
              // ),
              Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.circular(10.0),
                color: Colors.white),
            margin: EdgeInsets.all(5),
            height: 50.0,
            child: ListTile(
              contentPadding:
                  EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => SearchScreen()));
              },
              title: Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Text(
                  'Nom du produit, code ACL',
                  style: TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ),
              leading: Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Icon(
                  Icons.search,
                  size: 20.0,
                ),
              ),
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              loading
                  ? showCircularProgress()
                  : SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.only(
                            top: 10, bottom: 10, left: 10, right: 10),
                        child: Column(
                          children: listAd.map((adItem) {
                            return Container(
                              margin: EdgeInsets.only(
                                  top: 5, bottom: 10, left: 5, right: 5),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => S8_3Screen(
                                            ads: adItem,
                                            accessToken: widget.accessToken,
                                          )));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: _colorWhite,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 10,
                                            right: 10,
                                            bottom: 5,
                                            top: 5),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              adItem.title,
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  fontSize: 14),
                                            ),
                                            Text(
                                                // '24/04/2019'
                                                DateTime.fromMillisecondsSinceEpoch(
                                                            adItem.expire_date *
                                                                1000)
                                                        .toString()
                                                        .substring(8, 10) +
                                                    '/' +
                                                    DateTime.fromMillisecondsSinceEpoch(
                                                            adItem.expire_date *
                                                                1000)
                                                        .toString()
                                                        .substring(5, 7) +
                                                    '/' +
                                                    DateTime.fromMillisecondsSinceEpoch(
                                                            adItem.expire_date *
                                                                1000)
                                                        .toString()
                                                        .substring(0, 4),
                                                style: TextStyle(
                                                    color: Colors.black45,
                                                    fontSize: 12))
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(adItem.alc_code,
                                                style: TextStyle(
                                                    color: Colors.black45,
                                                    fontSize: 12)),
                                            Text(adItem.price + "€",
                                                style: TextStyle(
                                                    color: Colors.green[300],
                                                    fontSize: 15))
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 10,
                                            right: 10,
                                            bottom: 5,
                                            top: 5),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(adItem.workplace_name,
                                                style: TextStyle(
                                                    color: Colors.green[300],
                                                    fontSize: 12)),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
            ],
          ),
        ],
      ),
    );
  }
}
