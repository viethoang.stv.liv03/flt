import 'package:flutter/material.dart';
import 'package:foura/api/post_change_address.dart';
import 'package:foura/api/post_create_ads.dart';

import 'package:foura/tab/tab2/foura_s10_suggestion.dart';
import 'package:flutter/cupertino.dart';
import 'package:foura/tab/tab3/foura_s13_2_register.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';

class S9Screen extends StatefulWidget {
  String accessToken;
  CreateAds create = CreateAds();
  int id;
  S9Screen({this.accessToken, this.id});
  @override
  _S9ScreenState createState() => _S9ScreenState();
}

class _S9ScreenState extends State<S9Screen> {
  bool _isLoading = false;
  DateTime date2;
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  String _title;
  String _alccode;
  String _price;
  String _amount;
  String _expireday;
  String _workplaceid;
  int id;
  String accessToken;
  String _errorMessage = "";
  int _selectedColorIndex = 0;
  bool isLoading = false;
  bool isChecked = false;
  bool isPick = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController title;
  TextEditingController alccode;
  TextEditingController price;
  TextEditingController quantity;
  TextEditingController expiredate;
  TextEditingController workplaceid;
  DateTime selectedDate = DateTime.now();

  DateFormat _format = DateFormat("dd/MM/yyyy");

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2019, 1, 1),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  void initState() {
    super.initState();

    title = new TextEditingController();
    alccode = new TextEditingController();
    price = new TextEditingController();
    quantity = new TextEditingController();
    // expiredate = new TextEditingController();
    workplaceid = new TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    title.dispose();
    alccode.dispose();
    price.dispose();
    // expiredate.dispose();
    quantity.dispose();
    workplaceid.dispose();
  }
// Future changeAddress() async {
//     await ().viewCard(widget.accessToken).then((data) {
//       setState(() {
//         list = data;
//       });
//     });
//     setState(() {
//       loading = false;
//     });
//   }

  // @override
  // void initState() {

  //   super.initState();
  //   // changeAddress();
  // }
  @override
  Widget build(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedColorIndex);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorB,
        elevation: 1,
        centerTitle: true,
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Petites Annonces',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(right: 5),
                          child: Text(
                            'Ajouter une annonce',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black54,
                            ),
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.info,
                            color: _colorB,
                          ),
                          onPressed: () {
                            dialog(context);
                          },
                        )
                      ],
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: title,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Title',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          keyboardType: TextInputType.number,
                          controller: alccode,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Code ACL',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          keyboardType: TextInputType.number,
                          controller: quantity,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Quantité',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          keyboardType: TextInputType.number,
                          controller: price,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Prix TTC + honoraires',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: 10, left: 10.0, right: 5.0, bottom: 5.0),
                              child: Text(
                                "${_format.format(selectedDate)}",
                                style: TextStyle(color: Colors.black54),
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.calendar_today),
                              color: Colors.black54,
                              onPressed: () => _selectDate(context),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          keyboardType: TextInputType.number,
                          controller: workplaceid,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Départements',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 50, bottom: 0, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            _isLoading = true;
                          });

                          CreateAds()
                              .createAds(
                                  title.text,
                                  alccode.text,
                                  price.text,
                                  quantity.text,
                                  selectedDate.millisecondsSinceEpoch ~/ 1000,
                                  workplaceid.text,
                                  widget.accessToken)
                              .then((respone) {
                            int id = respone["data"]["id"];
                            print('id: ' + '$id');
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text(respone["message"].toString()),
                            ));
                            print('create ad done');
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => S13_2_RegisterScreen(
                                      accessToken: widget.accessToken,
                                      id: id,
                                    )));
                          });
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Créer une nouvelle annonce',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: SafeArea(
          top: true,
          bottom: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }

  Future<bool> dialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(30),
            title: ListTile(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => S10Screen()));
              },
              title: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 10),
                        child: Text(
                          'Ajouter une annonce',
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      IconButton(
                        padding: EdgeInsets.all(1),
                        icon: Icon(
                          Icons.close,
                          size: 15,
                        ),
                        // child: Text('x'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Text(
                      '3 conseils à respecter pour annonce efficace: ',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(
                      '1. Utilisez votre logiciel pour créer une liste de produits invendus depuis une à deux annéas. Facile à faire, elle vous permettra de cibler les produits risquant de périmer ; ',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(
                      '2. Ne déposez pas une annonce  avec une date de péremption trop proche. Le produit aura peu de chance d\'étre échangé ;',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text(
                      '3. N\'hésitez pas à parler du site à vos confrères aprés le dépót d\'une annonce. Cela augmentera l\'efficacité de la plateforme ;',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

// title: Row(
//   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//   children: <Widget>[
//     Container(
//       padding: EdgeInsets.only(bottom: 5.0),
//       child: Text(
//         "Pays",
//         style: TextStyle(
//             fontSize: 16, color: Colors.black45),
//       ),
//     ),
//     Container(
//       child: Container(
//         height: 35,
//         decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(10.0),
//             color: Colors.grey[250]),
//         child: Row(
//           children: <Widget>[
//             Container(
//               padding: EdgeInsets.only(bottom: 5.0),
//               child: DropdownButtonHideUnderline(
//                 child: DropdownButton<String>(
//                   value: _defaultPay2,
//                   onChanged: (String newValueSelected) {
//                     setState(() {
//                       _defaultPay2 = newValueSelected;
//                       setState(() {
//                         isPick2 = true;
//                       });
//                     });
//                   },
//                   items: _pay2.
//                       map((String dropDownListItem) {
//                     return DropdownMenuItem<String>(
//                       value: dropDownListItem,
//                       child: Container(
//                         child: Text(
//                           dropDownListItem,
//                           style: TextStyle(
//                             color: Colors.black54,
//                             fontSize: 15,
//                           ),
//                         ),
//                       ),
//                     );
//                   }).toList(),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ),
//   ],
// ),
