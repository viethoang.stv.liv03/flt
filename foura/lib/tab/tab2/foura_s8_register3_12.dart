import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foura/tab/tab3/foura_s13_3_register.dart';

class S8_Register3_12Screen extends StatefulWidget {
  String accessToken;
  S8_Register3_12Screen({this.accessToken});
  @override
  S8_Register3_12ScreenState createState() => S8_Register3_12ScreenState();
}

class S8_Register3_12ScreenState extends State<S8_Register3_12Screen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  Color _black = Colors.black87;

  bool isPick = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Commande recue',
            style: TextStyle(
              fontSize: 16,
              color: _black,
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                  alignment: Alignment.topLeft,
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                  // decoration: BoxDecoration(
                  //     border: Border.all(color: Colors.grey[200]),
                  //     borderRadius: BorderRadius.circular(0),
                  //     color: Colors.white),
                  child: Column(
                    
                    children: <Widget>[
                      Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                        'Merci. Votre commande été recue.',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black45,
                        ),
                      ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                     Container(
                       alignment: Alignment.bottomLeft,
                       child:  Text(
                        'NUMÉRO DE COMMANDE:',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black45,
                        ),
                      ),
                     ),
                      Padding(
                        padding: EdgeInsets.all(2),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                        'GRATUIT-23',
                        style: TextStyle(
                          fontSize: 14,
                          color: _black,
                        ),
                      ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                     Container(
                       alignment: Alignment.bottomLeft,
                       child:  Text(
                        'DATE:',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black45,
                        ),
                      ),
                     ),
                      Padding(
                        padding: EdgeInsets.all(2),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                        '30/05/2019',
                        style: TextStyle(
                          fontSize: 14,
                          color: _black,
                        ),
                      ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                     Container(
                       alignment: Alignment.bottomLeft,
                       child:  Text(
                        'E-MAIL',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black45,
                        ),
                      ),
                     ),
                      Padding(
                        padding: EdgeInsets.all(2),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                        'joannejemison121@gmail.com',
                        style: TextStyle(
                          fontSize: 14,
                          color: _black,
                        ),
                      ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                      ),
                     Container(
                       alignment: Alignment.bottomLeft,
                       child:  Text(
                        'TOTAL:',
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.black45,
                        ),
                      ),
                     ),
                      Padding(
                        padding: EdgeInsets.all(2),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                        '0.00€',
                        style: TextStyle(
                          fontSize: 14,
                          color: _black,
                        ),
                      ),
                      ),
                    ],
                  ),
                ),
              Container(
                padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 50),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey[200]),
                            borderRadius: BorderRadius.circular(0),
                            color: Colors.white),
                        child: Column(
                          children: <Widget>[
                            Text(
                              'Votre abonnement sera atif quand votre règlement sera effectué',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(5),
                            ),
                            Text(
                              'Voir le statut de votre abonnement dans votre compte',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                          ],
                        )),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[200]),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Produit',
                                    style: TextStyle(
                                      fontSize: 14,
                                    )),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  'Total',
                                  style: TextStyle(
                                      fontSize: 14, color: _black),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[200]),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Container(
                                      // alignment: Alignment.centerLeft,
                                      padding:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Text('Abonnement 3',
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: Colors.black45)),
                                    ),
                                    Container(
                                      // alignment: Alignment.topLeft,
                                      padding:
                                          EdgeInsets.only(bottom: 15, left: 15),
                                      child: Text('mois à Foura x 1',
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: Colors.black45)),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerRight,
                                      padding: EdgeInsets.only(
                                          right: 15, bottom: 5, top: 5),
                                      child: Text('0.00€',
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: _black)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: _colorWhite),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Sous-total'),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  '0.00€',
                                  style: TextStyle(
                                      fontSize: 12.5, color: _black),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[200]),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Total'),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  '0.00€',
                                  style: TextStyle(
                                      fontSize: 12.5, color: _black),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 50, bottom: 40, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => S13_3_RegisterScreen()));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                              child: Text(
                            'Votre compte',
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> dialogCard(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => S10Screen()));
              },
              title: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 15, left: 10),
                            child: Text(
                              'Sélectionez la carte',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 10, left: 5),
                            child: Text(
                              'bancaire',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                      IconButton(
                        padding: EdgeInsets.only(top: 15),
                        icon: Icon(
                          Icons.close,
                          size: 18,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                  Container(
                    child: ListTile(
                      title: Container(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          '* 1220',
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                      ),
                      leading: Container(
                        margin: EdgeInsets.only(bottom: 10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                        ),
                        // padding: EdgeInsets.only(bottom: 10),
                        child: Image.asset(
                          'images/creditcard.png',
                          height: 30,
                          width: 40,
                        ),
                      ),
                      onTap: () {
                        isPick = true;
                      },
                    ),
                  ),
                  Container(
                    child: ListTile(
                      title: Container(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          '* 3242',
                          style: TextStyle(fontSize: 14, color: Colors.black),
                        ),
                      ),
                      leading: Container(
                        margin: EdgeInsets.only(bottom: 10),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                        ),
                        // padding: EdgeInsets.only(bottom: 10),
                        child: Image.asset(
                          'images/mastercard2.png',
                          height: 30,
                          width: 40,
                        ),
                      ),
                      onTap: () {
                        isPick = true;
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
