import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foura/api/get_view_card.dart';
import 'package:foura/model/card.dart';
import 'package:foura/model/product.dart';
import 'package:foura/parse_number.dart';
import 'package:foura/tab/tab3/foura_s13_6_addpayment.dart';
import 'package:foura/tab/tab2/foura_s8_register3_12.dart';

class S8_2Screen extends StatefulWidget {
  String accessToken;
  Product product;
  S8_2Screen({this.accessToken, this.product});
  @override
  _S8_2ScreenState createState() => _S8_2ScreenState();
}

class _S8_2ScreenState extends State<S8_2Screen> {
  static DateTime now = DateTime.now();
  var nowAdd90 = now.add(Duration(days: 90));

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  ParseNumber parse = ParseNumber();

  TextEditingController first_name;
  TextEditingController phone;
  TextEditingController country;
  TextEditingController last_name;
  TextEditingController company;
  TextEditingController siren;
  TextEditingController rpps;
  TextEditingController street;
  TextEditingController city;
  TextEditingController email;
  TextEditingController postal_code;

  List<CreditCard> listCreditCard;

  bool isPick = false;
  bool loading = true;
  bool isPress = false;

  Future getViewCard() async {
    await ViewCard().viewCard(widget.accessToken).then((data) {
      setState(() {
        listCreditCard = data;
      });
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    getViewCard();
    super.initState();
    first_name = TextEditingController();
    phone = TextEditingController();
    country = TextEditingController();
    last_name = TextEditingController();
    company = TextEditingController();
    siren = TextEditingController();
    rpps = TextEditingController();
    street = TextEditingController();
    city = TextEditingController();
    email = TextEditingController();
    postal_code = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    first_name.dispose();
    phone.dispose();
    country.dispose();
    last_name.dispose();
    company.dispose();
    siren.dispose();
    rpps.dispose();
    street.dispose();
    city.dispose();
    email.dispose();
    postal_code.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Validation de la commande',
            style: TextStyle(
              fontSize: 16,
              color: Colors.black87,
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(right: 5),
                      child: Text(
                        'Validation de la commande',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(right: 5),
                      child: Text(
                        'Détails de facturation',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: first_name,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Prénom',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: last_name,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: company,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom de l\'entreprise',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: siren,
                          keyboardType: TextInputType.number,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'SIREN',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: rpps,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'RPPS',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: country,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Pays',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: street,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Numéro et nome de rue',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: postal_code,
                          keyboardType: TextInputType.number,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Code postal (facultatif)',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: city,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Ville',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: phone,
                          keyboardType: TextInputType.numberWithOptions(),
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Téléphone',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: email,
                          keyboardType: TextInputType.text,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Adresse de messagerie',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                  ],
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                padding: EdgeInsets.only(bottom: 20, left: 20),
                child: Text(
                  'Votre commande',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.black,
                  ),
                ),
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Produit',
                              style: TextStyle(
                                fontSize: 14,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            'Total',
                            style:
                                TextStyle(fontSize: 14, color: Colors.black45),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 120,
                                // alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(
                                    top: 10, left: 15, bottom: 10),
                                child: Text(
                                    widget.product.name
                                    // 'Abonnement 3'
                                    ,
                                    style: TextStyle(
                                        fontSize: 12.5, color: Colors.black45)),
                              ),
                              // Container(
                              //   alignment: Alignment.centerLeft,
                              //   padding: EdgeInsets.only(bottom: 15, left: 5),
                              //   child: Text('mois à Foura x 1',
                              //       style: TextStyle(
                              //           fontSize: 12.5, color: Colors.black45)),
                              // ),
                            ],
                          ),
                          Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.bottomRight,
                                padding: EdgeInsets.only(
                                    right: 17, top: 10, bottom: 0),
                                child: Text(
                                    widget.product.id == 1
                                        ? (parse.parseNumber(widget
                                                .product.subtotal
                                                .toString()) +
                                            ' HT/année avec 1 année')
                                        : (parse.parseNumber(widget
                                                        .product.subtotal
                                                        .toString() !=
                                                    0
                                                ? '000'
                                                : widget.product.subtotal
                                                    .toString()) +
                                            ' HT/mois pour 3 mois'),
                                    style: TextStyle(
                                        fontSize: 12.5, color: Colors.black45)),
                              ),
                              Container(
                                alignment: Alignment.centerRight,
                                padding: widget.product.id == 1
                                    ? EdgeInsets.only(bottom: 10, left: 25)
                                    : EdgeInsets.only(bottom: 10, left: 5),
                                child: Text(
                                  'avec ' +
                                      widget.product.trial_date.toString() +
                                      ' d\'essai gratuit',
                                  style: TextStyle(
                                      fontSize: 12.5, color: Colors.black45),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Sous-total'),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            widget.product.id == 1
                                ? parse.parseNumber(
                                        widget.product.subtotal.toString()) +
                                    '€'
                                : (parse.parseNumber(
                                        widget.product.subtotal.toString() != 0
                                            ? '000'
                                            : widget.product.subtotal
                                                .toString())) +
                                    '€',
                            style: TextStyle(
                                fontSize: 12.5, color: Colors.black45),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Total'),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            widget.product.id == 1
                                ? parse.parseNumber(
                                        widget.product.total.toString()) +
                                    '€'
                                : (parse.parseNumber(
                                        widget.product.total.toString() != 0
                                            ? '000'
                                            : widget.product.total
                                                .toString())) +
                                    '€',
                            style: TextStyle(
                                fontSize: 12.5, color: Colors.black45),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Totaux Récurrent'),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Sous-total'),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            widget.product.id == 1
                                ? (parse.parseNumber(
                                        widget.product.subtotal.toString()) +
                                    ' HT/année avec 1 année')
                                : (parse.parseNumber(
                                        widget.product.subtotal.toString() != 0
                                            ? '000'
                                            : widget.product.subtotal
                                                .toString()) +
                                    ' HT/mois pour 3 mois'),
                            style: TextStyle(
                                fontSize: 12.5, color: Colors.black45),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(0.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Total récurrent'),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text(
                            widget.product.id == 1
                                ? (parse.parseNumber(
                                        widget.product.total.toString()) +
                                    ' HT/année avec 1 année')
                                : (parse.parseNumber(
                                        widget.product.total.toString() != 0
                                            ? '000'
                                            : widget.product.total.toString()) +
                                    ' HT/mois pour 3 mois'),
                            style: TextStyle(
                                fontSize: 12.5, color: Colors.black45),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: ListTile(
                        // leading: Text('Total récurrent'),
                        trailing: Text(
                          '1er renouvellement: ' +
                              nowAdd90.toString().substring(8, 10) +
                              '/' +
                              nowAdd90.toString().substring(5, 7) +
                              '/' +
                              nowAdd90.toString().substring(0, 4),
                          style:
                              TextStyle(fontSize: 12.5, color: Colors.black45),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(5.0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Text('Carte Bancaire'),
                        ),
                      ),
                    ),
                    Container(
                      height: 40,

                      // width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[300]),
                          borderRadius: BorderRadius.circular(5.0),
                          color: Colors.grey[50]),
                      margin: EdgeInsets.only(left: 30, right: 30),
                      child: ListTile(
                        title: Container(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Text(
                            'Carte Bancaire',
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(bottom: 15),
                          child: Icon(
                            Icons.arrow_drop_down,
                            color: Colors.grey[350],
                          ),
                        ),
                        onTap: () {
                          dialogCard(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(0.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 1.0,
                    left: 0.0,
                    right: 0.0,
                  ),
                  height: 45.0,
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        child: IconButton(
                          icon: Icon(Icons.add_box),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => S13_6_AddPaymentScreen(
                                      accessToken: widget.accessToken,
                                    )));
                          },
                          color: Colors.grey,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 0, right: 30),
                        child: Text(
                          'Ajouter une nouvelle carter',
                          style: TextStyle(fontSize: 13, color: Colors.black45),
                        ),
                      ),
                    ],
                  )),
              Container(
                padding:
                    EdgeInsets.only(top: 50, bottom: 40, left: 20, right: 20),
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S8_Register3_12Screen(
                              accessToken: widget.accessToken,
                            )));
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    decoration: BoxDecoration(
                        color: _colorB, borderRadius: BorderRadius.circular(5)),
                    child: Center(
                        child: Text(
                      'S\'ABONNER',
                      style: TextStyle(color: Colors.white),
                    )),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> dialogCard(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
              onTap: () {
                Navigator.pop(context);
              },
              title: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(bottom: 0, top: 15, left: 10),
                            child: Text(
                              'Sélectionez la carte bancaire',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  loading
                      ? Container()
                      : SingleChildScrollView(
                          child: Container(
                            height: MediaQuery.of(context).size.height / 3.5,
                            width: MediaQuery.of(context).size.width,
                            child: ListView(
                              children: listCreditCard.map((cardItem) {
                                return GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    //  isPress=true;
                                    // });

                                    //  Navigator.push(
                                    //     context,
                                    //     MaterialPageRoute(
                                    //         builder: (_) => CreditCardItem(
                                    //               creditCard: cardItem,
                                    //             )));
                                  },
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(left: 25, right: 15),
                                    margin: EdgeInsets.only(bottom: 1),
                                    height: 35,
                                    width:
                                        MediaQuery.of(context).size.width - 50,
                                    color: Colors.white,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Image.asset(
                                              cardItem.brand == "Visa"
                                                  ? "images/visacard.png"
                                                  : cardItem.brand ==
                                                          "MasterCard"
                                                      ? "images/mastercard.png"
                                                      : cardItem.brand ==
                                                              "American Express"
                                                          ? "images/amexcard.png"
                                                          : 'images/Group 699.png',
                                              height: 30,
                                            ),
                                            Text(
                                              '* ' + cardItem.last4,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13),
                                            )
                                          ],
                                        ),
                                        //  isPress==true? Icon(Icons.check,color: Colors.red,):Icon(Icons.check,color: _colorWhite,)
                                      ],
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                ],
              ),
            ),
          );
        });
  }
}
