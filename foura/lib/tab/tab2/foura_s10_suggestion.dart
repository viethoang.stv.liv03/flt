import 'package:flutter/material.dart';
import 'package:foura/tab/tab2/foura_s8_register.dart';

class S10Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return S10ScreenState();
  }
}

class S10ScreenState extends State<S10Screen> {
  Color _colorBlack = Colors.black54;
  Color _colorBlue = Color.fromARGB(255, 0, 162, 224);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: BackButton(
            color: Colors.grey,
          ),
        title: Container(
          padding: EdgeInsets.only(bottom: 0),
          child: Text(
            'Créer une liste de produits invendus',
            style: TextStyle(
                fontSize: 20, color: Colors.black54, ),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Comment créer une liste de médicaments bientôt périmés ?",
              style: TextStyle(fontSize: 20, color: Colors.black,),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15, bottom: 15,right:15),
            child: Text("LGPI",style: TextStyle(fontSize: 25, color: Colors.black,fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Pour établir une liste de produits susceptibles de périmer voilà comment procéder sur LGPI.",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Tapez G (utilitaires / listes) puis tapez J (logiciel) puis tapez N (codification produits).",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Faire F9 (critères)",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Dans la colonne « Stock » sélectionnez la case Stock Total et sélectionnez la ligne « est supérieur à » puis inscrivez « 0 ».",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Dans la colonne « Généralités » sélectionnez la case « date de dernière vente » et sélectionnez « est inférieur à » et inscrivez une date (18 à 24 mois antérieur à la date du jour).",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Dans la case « Gestion » sélectionnez la case « prix de vente » et sélectionnez « est supérieur à » et inscrivez un prix (supérieur à 30 € pour limiter la liste)",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Voilà il ne vous reste plus qu’à vérifier la date de péremption.",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text("WINPHARMA",style: TextStyle(fontSize: 25, color: Colors.black,fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Allez dans « Stock », « État du stock », « Voir le stock… »",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Sélectionnez « Détail », « Nom », « Sans Groupe »",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Décochez « Tous les produits en stock »",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Les produits sans vente : rentrer le « nombre » de jours",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Remplir ou non des éventuels filtres : TVA…",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "(Vous pouvez contacter l’assistance afin de mettre en place un filtre prix.)",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Pour imprimer la liste : « Écran », « Imprimer »",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text("WINPHARMA",style: TextStyle(fontSize: 25, color: Colors.black,fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Procédure pour sortir une liste de produits, allez dans Fichiers > Produits > Produits (et entrée).",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Appuyez sur F1 : liste puis Modification des paramétres.",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "Faites Impression: imprimante ou écran > Format: liste produit (et non liste des produits) > Classement: alphabétique > Sélection: oui.",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "32: date de péremption; périmé entre : choisir les mois",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "33: données stock: réel supérieur à zéro",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "03 : prix public mini ( si on veut se fixer un prix )",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 15,right:15),
            child: Text(
              "06: TVA (si on veut lister par TVA)",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15,bottom: 35,right:15),
            child: Text(
              "Entrée jusqu’à impression ou affichage",
              style: TextStyle(fontSize: 16, color: _colorBlack),
            ),
          ),
        ],
      ),
    );
  }
}
