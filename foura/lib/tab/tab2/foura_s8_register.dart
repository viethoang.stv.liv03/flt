import 'package:flutter/material.dart';
import 'package:foura/tab/tab2/foura_s8_order.dart';
import 'package:foura/tab/tab1/foura_s8_detail.dart';
import 'package:foura/tab/tab2/foura_s9_tab2.dart';
class S8_RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return S8_RegisterScreenState();
  }
}

class S8_RegisterScreenState extends State<S8_RegisterScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: _colorWhite,
        elevation: 1,
        leading: BackButton(
            color: Colors.grey,
          ),
        title: Container(
            padding: EdgeInsets.only(top: 6),
            child: Text(
              'S\'abonner',
              style: TextStyle(fontSize: 16,color: Colors.black87),
            ),
          ),
      ),
      backgroundColor: _colorWhite,
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20, bottom: 10,left: 10, right: 10),
            child: Text(
              "Désolé, vous devez être un abonné actif pour avoir accès.",
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 10,left: 10, right: 10),
            child: Text(
              "Abonnement 3 mois à Stock2Big",
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 10,left: 10, right: 10),
            child: Text(
              "0,00 € HT / mois pour 3 mois avec 3 mois d’essai gratuit",
              style: TextStyle(color: Colors.green),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 10,left: 10, right: 10),
            child: Text(
              "L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.",
              style: TextStyle(color: _colorB),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 10,left: 10, right: 10),
            child: Text(
              "Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.",
              style: TextStyle(color: _colorB),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 80,left: 10, right: 10),
            child: Text(
              "Testez gratuitement pendant 3 mois notre service : vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.",
              style: TextStyle(color: _colorB),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 40, left: 20, right: 20),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => S8_2Screen()));
                    // Navigator.of(context).push(
                    // MaterialPageRoute(builder: (_) => S9Screen()));
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: _colorB,
                    borderRadius: BorderRadius.circular(5)),
                child: Center(
                    child: Text(
                  'S\'ABONNER',
                  style: TextStyle(color: _colorWhite),
                )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
