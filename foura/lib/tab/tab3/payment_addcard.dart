// import 'package:flutter/material.dart';
// import 'package:flutter_country_picker/flutter_country_picker.dart';

// class PaymentAddCard extends StatefulWidget {
//   @override
//   _PaymentAddCardState createState() => _PaymentAddCardState();
// }

// class _PaymentAddCardState extends State<PaymentAddCard> {
//   var _colorWhite = Colors.white;
//   var _color = Colors.black;

//     TextEditingController _cvv = TextEditingController();
//   TextEditingController nom2 = TextEditingController();
//   TextEditingController nomDeEnterprise2 = TextEditingController();
//   TextEditingController siren2 = TextEditingController();
//   TextEditingController rpps2 = TextEditingController();
//   TextEditingController ville2 = TextEditingController();
//   TextEditingController adresse2 = TextEditingController();
//   TextEditingController telephone2 = TextEditingController();
//   TextEditingController numero2 = TextEditingController();
//   TextEditingController code2 = TextEditingController();

//   Country _country;
//   String _cardNumber = '';
//   String _mmdd = '';
//   // String _cvv = '';
//   String _errorCVV = " ";
//   var _countrySize = [
//     'Australia',
//     'Vietnam',
//     'Singapore',
//     'Korea',
//     'China',
//   ];
//   var _defaultCountrySize = 'Australia';
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       resizeToAvoidBottomPadding: false,
//       appBar: AppBar(
        
//         title: Text(
//           'Add Card',
//           style: TextStyle(color: _color, fontWeight: FontWeight.w400),
//         ),
//         backgroundColor: _colorWhite,
//         elevation: 1,
//         leading: BackButton(color: Colors.grey,),
//         centerTitle: true,
//       ),
//       backgroundColor: _colorWhite,
//       body: Stack(
//         children: <Widget>[
//           // Positioned(
//           //     top: 85,
//           //     right: 20,
//           //     left: 20,
//           //     child: Divider(
//           //       color: _cardNumber == '' ? Colors.grey : Colors.amber,
//           //     )),
//           // Positioned(
//           //     top: 147,
//           //     left: 20,
//           //     right: MediaQuery.of(context).size.width / 2 + 25,
//           //     child: Divider(
//           //       color: _mmdd == '' ? Colors.grey : Colors.amber,
//           //     )),
//           // Positioned(
//           //     top: 147,
//           //     right: 20,
//           //     left: MediaQuery.of(context).size.width / 2 + 25,
//           //     child: Divider(
//           //       color: _cvv == '' ? Colors.grey : Colors.amber,
//           //     )),
          
//           Positioned(
//             right: 20,
//             left: 20,
//             top: 230,
//             child: Divider(color: Colors.grey),
//           ),
//           Positioned(
//             top: 130,
//             right: MediaQuery.of(context).size.width / 2 + 25,
//             child: Container(
//               height: 18,
//               width: 18,
//               child: Center(
//                   child: Text(
//                 '?',
//                 style: TextStyle(color: _color, fontSize: 15),
//               )),
//               decoration: BoxDecoration(
//                   border: Border.all(color: _color),
//                   borderRadius: BorderRadius.circular(10)),
//             ),
//           ),
//           Positioned(
//             top: 130,
//             right: 20,
//             child: Container(
//               height: 18,
//               width: 18,
//               child: Center(
//                   child: Text(
//                 '?',
//                 style: TextStyle(color: _color, fontSize: 15),
//               )),
//               decoration: BoxDecoration(
//                   border: Border.all(color: _color),
//                   borderRadius: BorderRadius.circular(10)),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 20, right: 20),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 SizedBox(
//                   height: 40,
//                 ),
//                 Text(
//                   'Card Number',
//                   style: TextStyle(
//                       color: _color, fontSize: 10, fontWeight: FontWeight.w300),
//                 ),
//                 TextField(
//                   keyboardType: TextInputType.numberWithOptions(),
//                   cursorColor: _color,
//                   cursorWidth: 1,
//                   style: TextStyle(color: _color, fontSize: 18),
//                   decoration: InputDecoration(
//                     focusedBorder: UnderlineInputBorder(
//                         borderSide: BorderSide(color: Colors.grey)
//                         ),
//                     fillColor: Colors.amber,
//                     icon: Image.asset(_cardNumber == '34'
//                         ? 'images/amexcard.png'
//                         : _cardNumber == '43'
//                             ? 'images/visacard.png'
//                             : _cardNumber == '243'
//                                 ? 'images/mastercard.png'
//                                 : 'images/paymentandmethod1.png'),
//                   ),
//                   onChanged: (value) {
//                     setState(() {
//                       _cardNumber = value;
//                     });
//                   },
//                 ),
//               ],
//             ),
//           ),
//           // Positioned(
//           //   top: 57,
//           //   right: 20,
//           //   child: Icon(
//           //     Icons.photo_camera,
//           //     color: Colors.grey,
//           //     size: 35,
//           //   ),
//           // ),
//           Positioned(
//             top: 120,
//             left: 20,
            
//             right: MediaQuery.of(context).size.width / 2 + 25,
//             child: TextField(
              
//               keyboardType: TextInputType.phone,
//               maxLength: 5,
//               cursorColor: Colors.white,
//               cursorWidth: 1,
//               style: TextStyle(color: _color, fontSize: 18),
//               decoration: InputDecoration(
                
//                 hintStyle:
//                     TextStyle(color: _color, fontWeight: FontWeight.w300),
//                 hintText: 'MM / YY',
//                 focusedBorder: UnderlineInputBorder(
//                     borderSide: BorderSide(color: Colors.grey)),
//                 fillColor: Colors.amber,
//               ),
//               onChanged: (value) {
//                 setState(() {
//                   _mmdd = value;
//                 });
//               },
//             ),
//           ),
//           Positioned(
//             top: 120,
//             right: 20,
//             left: MediaQuery.of(context).size.width / 2 + 25,
//             child:  Container(
//                       decoration: BoxDecoration(
//                           border: Border.all(color: Colors.white),
//                           borderRadius: BorderRadius.circular(10.0),
//                           color: Colors.grey[200]),
//                       margin: EdgeInsets.only(
//                           top: 20, left: 5.0, right: 5.0, bottom: 5.0),
//                       height: 50.0,
//                       child: TextField(
//                           controller: adresse2,
//                           onSubmitted: null,
//                           decoration: (InputDecoration(
//                             border: InputBorder.none,
//                             hintText: 'Adresse de messagerie',
//                             hintStyle: TextStyle(fontSize: 15.0),
//                             contentPadding: EdgeInsets.only(top: 15, left: 15),
//                           ))),
//                     ),
           
//           ),
//           Positioned(
//             left: 20,
//             right: 20,
//             top: 150,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 SizedBox(
//                   height: 40,
//                 ),
//                 Text(
//                   'Country',
//                   style: TextStyle(
//                       color: _color, fontSize: 10, fontWeight: FontWeight.w300),
//                 ),
//                 Container(
//                   width: MediaQuery.of(context).size.width,
//                   child: CountryPicker(
//                     onChanged: (Country country) {
//                       setState(() {
//                         _country = country;
//                       });
//                     },
//                     selectedCountry: _country,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Positioned(
//             bottom: 130,
//             left: MediaQuery.of(context).size.width / 2 - 30,
//             child: GestureDetector(
//               onTap: () {
//                 if (_cvv == '')
//                   setState(() {
//                     _errorCVV = "Enter a valid card code";
//                   });
//               },
//               child: Container(
//                 height: 60,
//                 width: 60,
//                 decoration: BoxDecoration(
//                     color: _cardNumber != ''
//                         ? Colors.amber
//                         : Colors.amber.withOpacity(0.5),
//                     borderRadius: BorderRadius.circular(40)),
//                 child: Center(
//                   child: Icon(
//                     Icons.arrow_forward,
//                     color: Colors.black,
//                     size: 32,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             top: 157,
//             right: 20,
//             child: Text(
//               _errorCVV,
//               style: TextStyle(color: Colors.amber, fontSize: 12),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
