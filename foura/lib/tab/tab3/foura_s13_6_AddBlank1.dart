import 'package:flutter/material.dart';
import 'package:foura/tab/tab3/foura_s13_5_1.dart';

import 'package:foura/tab/tab3/foura_s13_6_addpayment.dart';

class S13_6_AddBlank1Screen extends StatefulWidget {
  String accessToken;
  S13_6_AddBlank1Screen({this.accessToken});
  @override
  S13_6_AddBlank1ScreenState createState() => S13_6_AddBlank1ScreenState();
}

class S13_6_AddBlank1ScreenState extends State<S13_6_AddBlank1Screen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          leading: BackButton(
            color: Colors.grey,
          ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Moyen de paiment',
              // 'Foura',
              style: TextStyle(
                fontSize: 16, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.circular(0.0),
                        color: Colors.white),
                    margin: EdgeInsets.only(
                      top: 1,
                      left: .0,
                      right: 0.0,
                    ),
                    height: 50.0,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Text(
                          'Aucun moyen sauvegardé trouvé.',
                          style:
                              TextStyle(fontSize: 14, color: Colors.black45)),
                    )),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(0.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 1,
                    left: 0.0,
                    right: 0.0,
                  ),
                  height: 55.0,
                  child: ListTile(
                    contentPadding: EdgeInsets.only(
                      left: 10.0,
                      right: 10.0,
                    ),
                    onTap: () {
                      // Navigator.of(context).push(
                      //     MaterialPageRoute(builder: (_) => PaymentAddCard()));
                    },
                    title: Text(
                      'Ajouter un moyen de paiment',
                      style: TextStyle(fontSize: 14, color: Colors.black),
                    ),
                    leading: Icon(Icons.credit_card),
                    trailing: Icon(Icons.arrow_drop_down),
                  ),
                ),
                Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(0.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 1.0,
                  left: 0.0,
                  right: 0.0,
                ),
                height: 45.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 30.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    // Navigator.of(context).push(MaterialPageRoute(
                    //     builder: (_) => PaymentAddCard()));
                     Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S13_6_AddPaymentScreen(
                          accessToken: widget.accessToken,
                        )));
                  },
                  title: Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                      'Ajouter une nouvelle carter',
                      style: TextStyle(fontSize: 13, color: Colors.black),
                    ),
                  ),
                  leading: Container(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Icon(Icons.add_box),
                  ),
                ),
              ),
              ],
            ),
          ],
        ));
  }
}
