import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura/api/get_view_ad.dart';
import 'package:foura/api/post_list_ads.dart';
import 'package:foura/api/get_view_card.dart';
import 'package:foura/model/ad.dart';
import 'package:foura/parse_number.dart';
import 'package:foura/tab/tab3/foura_s13_2_editDrug.dart';
import 'package:foura/tab/tabs_screen.dart';

class S13_2_RegisterScreen extends StatefulWidget {
  String accessToken;
  int id;
  Ads ads;
  S13_2_RegisterScreen({this.accessToken, this.id,this.ads});
  @override
  S13_2_RegisterScreenState createState() => S13_2_RegisterScreenState();
}

class S13_2_RegisterScreenState extends State<S13_2_RegisterScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  ParseNumber parse = ParseNumber();

  bool loading = true;

  Ads ad;
  Future getViewAd() async {
    await ViewAds().getViewAds(widget.id).then((data) {
      setState(() {
        // print('aa');
        // print('code:'+ ad.alc_code);
        ad = data;
        loading = false;
        print('id000000: '+'${ad.id}');
      });
    });
  }

  @override
  void initState() {
    getViewAd();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => TabsScreen(
                      accessToken: widget.accessToken,
                    )));
          },
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Mes annonces',
            // 'Foura',
            style: TextStyle(
              fontSize: 16, color: Colors.black,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              loading
                  ? Container()
                  : SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.only(
                            top: 10, bottom: 10, left: 10, right: 10),
                        child: Column(children: <Widget>[
                          // children: listAd.map((adItem) {
                          Container(
                            margin: EdgeInsets.only(
                                top: 5, bottom: 10, left: 5, right: 5),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) => S13_2_EditDrugScreen(
                                          accessToken: widget.accessToken,
                                        )));
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: _colorWhite,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          bottom: 5,
                                          top: 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            // '${ad.title}'
                                            ad.title,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 14),
                                          ),
                                          Text(
                                              // '24/04/2019'
                                              // DateTime.fromMillisecondsSinceEpoch(
                                              //             widget.ads.expire_date * 1000)
                                              //         .toString()
                                              //         .substring(8, 10) +
                                              //     '/' +
                                              //     DateTime.fromMillisecondsSinceEpoch(
                                              //             widget.ads.expire_date * 1000)
                                              //         .toString()
                                              //         .substring(5, 7) +
                                              //     '/' +
                                              //     DateTime.fromMillisecondsSinceEpoch(
                                              //             widget.ads.expire_date * 1000)
                                              //         .toString()
                                              //         .substring(0, 4)
                                              '${ad.expire_date.toString()}'
                                              ,
                                              style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                              '${ad.alc_code}'
                                              // '3453455'
                                              ,
                                              style: TextStyle(
                                                  color: Colors.black45,
                                                  fontSize: 12)),
                                          Text(
                                              // '6'
                                              '${ad.price}' + "€",
                                              style: TextStyle(
                                                  color: Colors.green[300],
                                                  fontSize: 15))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          bottom: 5,
                                          top: 5),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                              // '6'
                                              '${ad.workplace_name}',
                                              style: TextStyle(
                                                  color: Colors.green[300],
                                                  fontSize: 12)),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ),
                    ),
            ],
          ),
        ],
      ),
    );
  }
}
