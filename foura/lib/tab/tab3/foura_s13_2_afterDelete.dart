import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura/tab/tab3/foura_s13_2_editDrug.dart';
import 'package:foura/tab/tabs_screen.dart';

class S13_2_AfterDeleteScreen extends StatefulWidget {
  String accessToken;
  S13_2_AfterDeleteScreen({Key key, this.accessToken}) : super(key: key);
  @override
  S13_2_AfterDeleteScreenState createState() => S13_2_AfterDeleteScreenState();
}

class S13_2_AfterDeleteScreenState extends State<S13_2_AfterDeleteScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => TabsScreen(
                        accessToken: widget.accessToken,
                      )));
            },
            color: Colors.grey,
          ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Mes annonces',
              // 'Foura',
              style: TextStyle(
                fontSize: 16, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            // ListView(
            //   children: <Widget>[
            //     Container(
            //   padding: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
            //   child: GestureDetector(
            //     onTap: () {
            //       Navigator.of(context)
            //           .push(MaterialPageRoute(builder: (_) => S13_2_EditDrugScreen()));
            //     },

            //     child: Container(
            //       width: MediaQuery.of(context).size.width,
            //       height: 70,
            //       decoration: BoxDecoration(
            //           color: _colorWhite,
            //           borderRadius: BorderRadius.circular(10)),
            //       child: Column(
            //         children: <Widget>[
            //           Container(
            //             padding: EdgeInsets.only(
            //                 left: 10, right: 10, bottom: 5, top: 5),
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               children: <Widget>[
            //                 Text(
            //                   'Novalgin',
            //                   style: TextStyle(color: Colors.red, fontSize: 14),
            //                 ),
            //                 Text('24/04/2019',
            //                     style: TextStyle(
            //                         color: Colors.black45, fontSize: 12))
            //               ],
            //             ),
            //           ),
            //           Container(
            //             padding: EdgeInsets.only(
            //               left: 10,
            //               right: 10,
            //             ),
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               children: <Widget>[
            //                 Text('3413257',
            //                     style: TextStyle(
            //                         color: Colors.black45, fontSize: 12)),
            //                 Text('3.5€',
            //                     style: TextStyle(
            //                         color: Colors.green[300], fontSize: 15))
            //               ],
            //             ),
            //           ),
            //           Container(
            //             padding: EdgeInsets.only(
            //                 left: 10, right: 10, bottom: 5, top: 5),
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               children: <Widget>[
            //                 Text('Cher',
            //                     style: TextStyle(
            //                         color: Colors.green[300], fontSize: 12)),
            //               ],
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            //   ],
            // )
          ],
        ));
  }
}
