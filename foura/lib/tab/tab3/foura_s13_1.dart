import 'package:flutter/material.dart';
import 'package:foura/api/post_user_info.dart';
import 'package:foura/tab/tab3/foura_s13_tab3.dart';
import 'package:foura/model/user.dart';

class S13_1Screen extends StatefulWidget {
  User user;
  String accessToken;
  S13_1Screen({this.user, this.accessToken});
  @override
  S13_1ScreenState createState() => S13_1ScreenState();
}

class S13_1ScreenState extends State<S13_1Screen> {
  bool _isLoading = false;

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  String _prenom = "";
  String _nom = "";
  String _nomAffiche = "";
  String _email = "";
  String _password = "";
  String _confirmPassword = "";
  String _newPassword = "";

  String accessToken;
  User user;
  Future getUser() async {
    await PostUser().getUser(widget.accessToken).then((data) {
      print('name: ' + data.username);
      setState(() {
        user = data;
      });
    });
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Détails du compte',
            // 'Foura',
            style: TextStyle(
              fontSize: 16, color: Colors.black,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          // controller: prenom,
                          onChanged: (value) {
                            _prenom = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Prénom',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          // controller: nom,
                          onChanged: (value) {
                            _nom = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          
                          onChanged: (value) {
                            _nomAffiche = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom affiché',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      child: Text(
                        "Indique comment votre nom apparaitra dans la section relative eau compte et dans les avis",
                        style: TextStyle(fontSize: 15, color: Colors.black45),
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.grey[200]),
                        margin: EdgeInsets.only(
                            top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                        height: 50.0,
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          padding: EdgeInsets.only(top: 15, left: 10),
                          child: Text(
                            user == null ? "loading" : user.email
                            // 'Cecilia Santiago'
                            ,
                            style:
                                TextStyle(fontSize: 15, color: Colors.black45),
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Changement de mot de passe",
                            style:
                                TextStyle(fontSize: 15, color: Colors.black45),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          // controller: password,
                          onChanged: (value) {
                            _password = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Mot de passe actuel',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          // controller: newPassword,
                          onChanged: (value) {
                            _newPassword = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Noiveau not de passe',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          // controller: confirmPassword,
                          onChanged: (value) {
                            _confirmPassword = value;
                          },
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Confirmer le mot de passe',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 50, bottom: 40, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          // setState(() {
                          //   _prenom = widget.user.firstname;
                          // });
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                              builder: (_) => LoggedScreen(
                                    accessToken: widget.accessToken,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Enregistrer les modifications',
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
