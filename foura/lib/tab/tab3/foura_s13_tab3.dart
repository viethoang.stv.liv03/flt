import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foura/api/post_user_info.dart';
import 'package:foura/model/user.dart';
import 'package:foura/login_reg/foura_s0.dart';
import 'package:foura/api/app_bloc.dart';
import 'package:foura/tab/tab3/foura_s13_1.dart';
import 'package:foura/tab/tab3/foura_s13_2.dart';
import 'package:foura/tab/tab3/foura_s13_3.dart';
import 'package:foura/tab/tab3/foura_s13_4.dart';
import 'package:foura/tab/tab3/foura_s13_5.dart';
import 'package:foura/tab/tab3/foura_s13_6.dart';

class LoggedScreen extends StatefulWidget {
  String accessToken;
  User user;

  LoggedScreen({this.accessToken, this.user});

  @override
  LoggedScreenState createState() {
    return LoggedScreenState();
  }
}

class LoggedScreenState extends State<LoggedScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  bool loading = true;
  User user;
  Future getUser() async {
    await PostUser().getUser(widget.accessToken).then((data) {
      print('name: ' + data.username);

      setState(() {
        user = data;
        loading = false;
      });
    });
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: _colorB,
        elevation: 1,
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Profil',
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
      backgroundColor: Colors.grey[300],
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                // alignment: Alignment(0.3, -0.8),
              
                padding: EdgeInsets.only(
                    left: 140.0, right: 140.0, top: 10.0, bottom: 0),
                child: CircleAvatar(
                  foregroundColor: _colorWhite,
                  // backgroundColor: Colors.grey[300],ome6
                  backgroundImage: AssetImage('images/avt.png'),
                  radius: 40.0,
                ),
              ),
              // Container(
              //     alignment: Alignment(0.5, 1),
              //     child: Image.asset(
              //       'images/matched_icon1.png',
              //       height: 40,
              //     )),
              Container(
                  padding: EdgeInsets.only(top: 0, bottom: 10),
                  child: Center(
                    child: Text(
                      user == null ? "loading" : user.username
                      // 'Cecilia Santiago'
                      ,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          color: Colors.black),
                    ),
                  )),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S13_1Screen(
                              accessToken: widget.accessToken,
                            )));
                  },
                  title: Text(
                    'Modifier mon compte',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 8.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => S13_2Screen()));
                  },
                  title: Text(
                    'Mes annonces',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 9.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S13_3Screen(
                              accessToken: widget.accessToken,
                            )));
                  },
                  title: Text(
                    'Mon abonnement',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 10.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => S13_4Screen()));
                  },
                  title: Text(
                    'Mes commandes',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 11.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) =>
                            S13_5Screen(accessToken: widget.accessToken)));
                  },
                  title: Text(
                    'Modifier mon adresse',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 12.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S13_6Screen(
                              accessToken: widget.accessToken,
                            )));
                  },
                  title: Container(
                    padding: EdgeInsets.only(top: 5),
                    child: ListView(
                      children: <Widget>[
                        Text(
                          'Méthodes de',
                          style: TextStyle(fontSize: 14, color: Colors.black45),
                        ),
                        Text(
                          'paiement',
                          style: TextStyle(fontSize: 14, color: Colors.black45),
                        ),
                      ],
                    ),
                  ),
                  leading: Image.asset(
                    'images/Group 13.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 15,
                  left: 10.0,
                  right: 10.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    // Navigator.of(context)
                    //     .push(MaterialPageRoute(builder: (_) => SearchScreen()));
                  },
                  title: Text(
                    'Mon abonnement',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 14.png',
                    height: 30,
                    width: 30,
                  ),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                    top: 30, left: 10.0, right: 10.0, bottom: 30),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => LoginScreen()));
                  },
                  title: Text(
                    'Deconnexion',
                    style: TextStyle(fontSize: 14, color: Colors.black45),
                  ),
                  leading: Image.asset(
                    'images/Group 15.png',
                    height: 30,
                    width: 30,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
