import 'package:flutter/material.dart';
import 'package:foura/tab/tab3/foura_s13_3_1.dart';

class S13_3Screen extends StatefulWidget {
  String accessToken;
  S13_3Screen({Key key, this.accessToken});
  @override
  S13_3ScreenState createState() => S13_3ScreenState();
}

class S13_3ScreenState extends State<S13_3Screen> {
  

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Mes annonces',
            // 'Foura',
            style: TextStyle(
              fontSize: 16, color: Colors.black,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20,top: 20),
                child: Text('Vous n\'avez pas d\'abonnements actifs. Trouvez votre premier abonnements dans la boutique.',style: TextStyle(color: Colors.black45)),
              )
            ],
          ),
          Container(
                      padding: EdgeInsets.only(
                           bottom: 40, left: 40, right: 40),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => S13_3_1Screen(accessToken: widget.accessToken,)));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                              child: Text(
                            'Boutique',
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                      ),
                    ),
        ],
      )
    );
  }
}