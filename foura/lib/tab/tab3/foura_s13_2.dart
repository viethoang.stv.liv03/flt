import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class S13_2Screen extends StatefulWidget {
  @override
  S13_2ScreenState createState() => S13_2ScreenState();
}

class S13_2ScreenState extends State<S13_2Screen> {
  

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Mes annonces',
            // 'Foura',
            style: TextStyle(
              fontSize: 16, color: Colors.black,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20,top: 20),
                child: Text('Vous n\'avez encore publié aucune annone.',style: TextStyle(color: Colors.black45),),
              )
            ],
          )
        ],
      )
    );
  }
}
