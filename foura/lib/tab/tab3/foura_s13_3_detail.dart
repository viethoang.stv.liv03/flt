import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foura/tab/tab3/foura_s13_3_cancelDetail.dart';

class S13_3_DetailScreen extends StatefulWidget {
  @override
  S13_3_DetailScreenState createState() => S13_3_DetailScreenState();
}

class S13_3_DetailScreenState extends State<S13_3_DetailScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  Color _black = Colors.black;

  bool isPick = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Abonnement #1009',
            style: TextStyle(
              fontSize: 16,
              color: _black,
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[200]),
                    borderRadius: BorderRadius.circular(0),
                    color: Colors.white),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text('État:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            '30/05/2019',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text('Date de départ:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            'il y a 30 minutes',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text('Dernière date de commande:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            'il y a 30 minutes',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text('Prochain date de paiement:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            '31/08/2019',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text('Date de fin:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 5),
                          child: Text(
                            '31/08/2019',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 35,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.all(0),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 15),
                          child: Text('Date de fin d\'essai:',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              )),
                        ),
                        trailing: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 15),
                          child: Text(
                            '31/08/2019',
                            style: TextStyle(fontSize: 14, color: _black),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      decoration: BoxDecoration(
                          // border: Border.all(color: Colors.grey[200]),
                          borderRadius: BorderRadius.circular(0),
                          color: Colors.white),
                      margin: EdgeInsets.only(bottom: 20),
                      child: ListTile(
                        leading: Container(
                          padding: EdgeInsets.only(top: 5, bottom: 15),
                          child: Text(
                            'Actions:',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black45,
                            ),
                          ),
                        ),
                        trailing: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            padding: EdgeInsets.only(top: 5, bottom: 15),
                            child: RaisedButton(
                              color: Colors.red,
                              child: Text(
                                'Annuler',
                                style: TextStyle(color: _colorWhite),
                              ),
                              onPressed: () {
                                dialogButton(context);
                              },
                            )),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 50),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Produit',
                                    style: TextStyle(
                                      fontSize: 14,
                                    )),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  'Total',
                                  style: TextStyle(fontSize: 14, color: _black),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[200]),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Container(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Text('Abonnement 3',
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: Colors.black45)),
                                    ),
                                    Container(
                                      // alignment: Alignment.topLeft,
                                      padding:
                                          EdgeInsets.only(bottom: 15, left: 15),
                                      child: Text('mois à Foura x 1',
                                          style: TextStyle(
                                              fontSize: 12.5,
                                              color: Colors.black45)),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerRight,
                                      padding: EdgeInsets.only(
                                          right: 15, bottom: 5, top: 5),
                                      child: Text('0.00€',
                                          style: TextStyle(
                                              fontSize: 12.5, color: _black)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: _colorWhite),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Sous-total'),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  '0.00€',
                                  style:
                                      TextStyle(fontSize: 12.5, color: _black),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey[200]),
                                borderRadius: BorderRadius.circular(0),
                                color: Colors.white),
                            margin: EdgeInsets.all(0),
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text('Total'),
                              ),
                              trailing: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: Text(
                                  '0.00€',
                                  style:
                                      TextStyle(fontSize: 12.5, color: _black),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 10, bottom: 10),
                      // decoration: BoxDecoration(
                      //     border: Border.all(color: Colors.grey[200]),
                      //     borderRadius: BorderRadius.circular(0),
                      //     color: Colors.white),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Addresse de facturation',
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(5),
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Alphabet',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Joanne Jemison',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              '93 Rue de Rivoli',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              '75001 PARIS',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black45,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Container(
                              alignment: Alignment.bottomLeft,
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.phone,
                                    color: Colors.grey[350],
                                    size: 20,
                                  ),
                                  Text(
                                    ' 33 1 49 26 06 60',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black45,
                                    ),
                                  )
                                ],
                              )),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Container(
                              alignment: Alignment.bottomLeft,
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.email,
                                    color: Colors.grey[350],
                                    size: 20,
                                  ),
                                  Text(
                                    ' joannejemison@gmail.com',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black45,
                                    ),
                                  )
                                ],
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> dialogButton(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
                onTap: () {},
                title: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                      height: 30,
                      child: Text('Voulez-vous vous désabonner?'),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: FlatButton(
                              child: Text(
                                'Annuler',
                                style: TextStyle(color: _colorB),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Container(
                            child: FlatButton(
                              child: Text(
                                'D\'accord',
                                style: TextStyle(color: _colorB),
                              ),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) =>
                                        S13_3_CancelDetailScreen()));
                              },
                            ),
                          ),
                        ]),
                  ],
                )
                
                ),
          );
        });
  }
}
