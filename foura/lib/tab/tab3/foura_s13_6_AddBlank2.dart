import 'package:flutter/material.dart';
import 'package:foura/api/get_view_card.dart';
import 'package:foura/tab/tab3/credit_card_item.dart';
import 'package:foura/tab/tab3/foura_s13_5_1.dart';
import 'package:foura/model/card.dart';

import 'package:foura/tab/tab3/foura_s13_6_addpayment.dart';

class S13_6_AddBlank2Screen extends StatefulWidget {
  String accessToken;
  // CreditCard creditCard;
  S13_6_AddBlank2Screen({
    this.accessToken,
  });
  @override
  S13_6_AddBlank2ScreenState createState() => S13_6_AddBlank2ScreenState();
}

class S13_6_AddBlank2ScreenState extends State<S13_6_AddBlank2Screen> {
  Color _colorWhite = Colors.white;

  // Color _colorB = Color.fromARGB(255, 0, 162, 224);

  List<CreditCard> listCreditCard;
  int index;
  bool loading = true;
  bool isPress = false;
 

  Future getViewCard() async {
    await ViewCard().viewCard(widget.accessToken).then((data) {
      setState(() {
        listCreditCard = data;
      });
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    getViewCard();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          // leading: BackButton(
          //   color: Colors.grey,
          // ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Moyen de paiment',
              // 'Foura',
              style: TextStyle(
                fontSize: 16, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.circular(0.0),
                        color: Colors.white),
                    margin: EdgeInsets.only(
                      top: 1,
                      left: .0,
                      right: 0.0,
                    ),
                    height: 50.0,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Text('Aucun moyen sauvegardé trouvé.',
                          style:
                              TextStyle(fontSize: 14, color: Colors.black45)),
                    )),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(0.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 1,
                    bottom: 1,
                    left: 0.0,
                    right: 0.0,
                  ),
                  height: 55.0,
                  child: ListTile(
                      contentPadding: EdgeInsets.only(
                        left: 10.0,
                        right: 10.0,
                      ),
                      onTap: () {
                        // Navigator.of(context).push(
                        //     MaterialPageRoute(builder: (_) => PaymentAddCard()));
                      },
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Carte de crédit ou de débit',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black),
                            ),
                          ),
                          Container(
                            
                            child: Text(
                              '* ' + listCreditCard[2].last4
                             
                              ,
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black),
                            ),
                          ),
                        ],
                      ),
                      leading: Image.asset(
                        'images/Group 13.png',
                        height: 30,
                        width: 30,
                      ),
                      trailing: Icon(Icons.arrow_drop_down)),
                ),
                loading
                    ? Container()
                    : SingleChildScrollView(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: ListView(
                              children: listCreditCard.map((cardItem) {
                                var index = listCreditCard.indexOf(cardItem);
                                
                                return GestureDetector(
                                  onTap: () {
                                    for(int i=0;i <listCreditCard.length;index++){
                                      cardItem.isSelected=false;
                                    }
                                    listCreditCard[index].isSelected=true;
                               
                                    // print('${listCreditCard[index].brand}');
                                   
                                   
                               
                                  
                                  //  Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (_) => CreditCardItem(
                                  //               creditCard: cardItem,
                                  //             )));
                                  }
                                  ,
                                  child: Container(
                                    padding:
                                        EdgeInsets.only(left: 25, right: 15),
                                    margin: EdgeInsets.only(bottom: 1),
                                    height: 50,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.white,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Image.asset(
                                              cardItem.brand == "Visa"
                                                  ? "images/visacard.png"
                                                  : cardItem.brand ==
                                                          "MasterCard"
                                                      ? "images/mastercard.png"
                                                      : cardItem.brand ==
                                                              "American Express"
                                                          ? "images/amexcard.png"
                                                          : 'images/Group 699.png',
                                              height: 30,
                                            ),
                                            Text(
                                              '* ' + cardItem.last4,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13),
                                            )
                                          ],
                                        ),
                                         listCreditCard[index].isSelected==true? Icon(Icons.check,color: Colors.red,):Icon(Icons.check,color: _colorWhite,)
                                      ],
                                    ),
                                  ),
                                );
                                
                              }).toList(),
                            )),
                      ),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(0.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 0.0,
                    left: 0.0,
                    right: 0.0,
                  ),
                  height: 45.0,
                  child: ListTile(
                    contentPadding: EdgeInsets.only(
                      left: 30.0,
                      right: 10.0,
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => S13_6_AddPaymentScreen(
                                accessToken: widget.accessToken,
                              )));
                    },
                    title: Container(
                      padding: EdgeInsets.only(bottom: 15),
                      child: Text(
                        'Ajouter une nouvelle carter',
                        style: TextStyle(fontSize: 13, color: Colors.black),
                      ),
                    ),
                    leading: Container(
                      padding: EdgeInsets.only(bottom: 15),
                      child: Icon(Icons.add_box),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
