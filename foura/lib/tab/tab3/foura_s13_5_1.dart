import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura/api/post_change_address.dart';

import 'package:foura/tab/tab3/foura_s13_tab3.dart';
import 'package:foura/tab/tab3/foura_s13_5_showAddress.dart';

class S13_5_1Screen extends StatefulWidget {
  String accessToken;
  AddAddress add = AddAddress();
  S13_5_1Screen({Key key, this.accessToken}) : super(key: key);
  @override
  S13_5_1ScreenState createState() => S13_5_1ScreenState();
}

class S13_5_1ScreenState extends State<S13_5_1Screen> {
  bool isLoading = false;

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  TextEditingController firstname;
  // = TextEditingController();
  TextEditingController lastname;
  // = TextEditingController();
  TextEditingController company;
  // = TextEditingController();
  TextEditingController street;
  // = TextEditingController();
  TextEditingController postalcode;
  // = TextEditingController();
  TextEditingController city;
  // = TextEditingController();
  TextEditingController country;
  // = TextEditingController();
  TextEditingController phone;
  // = TextEditingController();

  @override
  void initState() {
    super.initState();
    firstname = TextEditingController();
    lastname = TextEditingController();
    company = TextEditingController();
    street = TextEditingController();
    postalcode = TextEditingController();
    city = TextEditingController();
    country = TextEditingController();
    phone = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    firstname.dispose();
    lastname.dispose();
    company.dispose();
    street.dispose();
    postalcode.dispose();
    city.dispose();
    country.dispose();
    phone.dispose();
  }

  bool isPick2 = false;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          // padding: EdgeInsets.only(top: 6),
          child: Text(
            'Modifier mon adresse',
            // 'Foura',
            style: TextStyle(
              fontSize: 20, color: Colors.black54,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(right: 5),
                      child: Text(
                        'Les adresses suivantes seront utilisées par défaut sur la page de commande.',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: firstname,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Prénom',
                            // hintText: 'Jemison',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: lastname,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom',
                            // hintText: 'Joanne',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: company,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Nom de l\'entreprise',
                            // hintText: 'STDIOHUE',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: street,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Numéro et nome de rue',
                            // hintText: '37 Rue de Rivoli',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: postalcode,
                          onSubmitted: null,
                          keyboardType: TextInputType.number,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Code postal (facultatif)',
                            // hintText: '75004',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: city,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Ville',
                            // hintText: 'Paris',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: country,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Pays',
                            // hintText: 'Country',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: phone,
                          onSubmitted: null,
                          keyboardType: TextInputType.number,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Téléphone',

                            // hintText: '+33 1 42 78 14 18',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 30, bottom: 40, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            isLoading = true;
                          });
                          AddAddress()
                              .addAddress(
                                  firstname.text,
                                  lastname.text,
                                  company.text,
                                  street.text,
                                  city.text,
                                  country.text,
                                  postalcode.text,
                                  phone.text,
                                  widget.accessToken)
                              .then((message) {
                            // _scaffoldKey.currentState.showSnackBar(SnackBar(
                            //   content: Text(message),
                            // ));
                            
                          });
                          if (firstname.text.isEmpty ||
                              lastname.text.isEmpty ||
                              company.text.isEmpty ||
                              street.text.isEmpty ||
                              city.text.isEmpty ||
                              country.text.isEmpty ||
                              postalcode.text.isEmpty ||
                              phone.text.isEmpty) {
                                print('Plz check');
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text('Please check your information'),
                            ));
                              }
                              else {
                                Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => S13_5_ShowScreen(
                                      accessToken: widget.accessToken,
                                    )));
                              }
                            
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Enregistrer les modifications',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: SafeArea(
          top: true,
          bottom: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }
}
