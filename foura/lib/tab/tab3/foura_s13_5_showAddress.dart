import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura/api/post_user_info.dart';

import 'package:foura/model/user.dart';
import 'package:foura/tab/tab3/foura_s13_5_1.dart';
import 'package:foura/tab/tabs_screen.dart';

class S13_5_ShowScreen extends StatefulWidget {
  String accessToken;
  User user;
  S13_5_ShowScreen({this.accessToken, this.user});
  @override
  S13_5_ShowScreenState createState() => S13_5_ShowScreenState();
}

class S13_5_ShowScreenState extends State<S13_5_ShowScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  bool loading = true;
  User user;
  Future getUser() async {
    await PostUser().getUser(widget.accessToken).then((data) {
      // print('name: ' + data.username);
      // print('id: ' + data.id.toString());
      setState(() {
        user = data;
        loading = false;
      });
    });
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => TabsScreen(
                        accessToken: widget.accessToken,
                      )));
            },
            color: Colors.grey,
          ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Modifier mon adresse',
              // 'Foura',
              style: TextStyle(
                fontSize: 16, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: BorderRadius.circular(0.0),
                        color: Colors.white),
                    margin: EdgeInsets.only(
                      top: 1,
                      left: .0,
                      right: 0.0,
                    ),
                    height: 50.0,
                    child: Container(
                      padding: EdgeInsets.all(7),
                      child: Text(
                          'Les adresses suivantes seront utilisées par défaut sur la page de commande.',
                          style:
                              TextStyle(fontSize: 14, color: Colors.black45)),
                    )),
                loading
                    ? Container()
                    : Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.white),
                            borderRadius: BorderRadius.circular(0.0),
                            color: Colors.white),
                        margin: EdgeInsets.only(
                          top: 1.0,
                          left: 0.0,
                          right: 0.0,
                        ),
                        height: 100.0,
                        child: ListTile(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => S13_5_1Screen(
                                        accessToken: widget.accessToken,
                                      )));
                            },
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: Colors.black26,
                                      size: 24,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        user.company
                                        // 'STDIOHUE'
                                        ,
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: _colorWhite,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        user.first_name + ' ' + user.last_name
                                        // 'Joanne Jemison'
                                        ,
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: _colorWhite,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        user.street
                                        // '37 Rue de Rivoli'
                                        ,
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: _colorWhite,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        user.postal_code + ' ' + user.city
                                        // '75004 PARIS'
                                        ,
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            trailing: Container(
                              child: Icon(Icons.chevron_right),
                              padding: EdgeInsets.only(top: 20),
                            )),
                      ),
              ],
            ),
          ],
        ));
  }
}
