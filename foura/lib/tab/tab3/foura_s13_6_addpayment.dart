import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:foura/api/post_add_card_payment.dart';
import 'package:foura/tab/tab3/foura_s13_6_AddBlank2.dart';
import 'package:foura/model/card_info.dart';
// import 'package:foura/model/card.dart';
import 'package:stripe_api/stripe_api.dart';

import 'package:credit_card_field/credit_card_field.dart';

class S13_6_AddPaymentScreen extends StatefulWidget {
  String accessToken;
  CardInfo cardInfo;
  S13_6_AddPaymentScreen({this.accessToken});
  @override
  S13_6_AddPaymentState createState() => S13_6_AddPaymentState();
}

class S13_6_AddPaymentState extends State<S13_6_AddPaymentScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  TextEditingController name = TextEditingController();
  TextEditingController cardNumber = TextEditingController();
  TextEditingController mmyy = TextEditingController();
  TextEditingController cvv = TextEditingController();
  String _cardNumber = '';
  String _mmyy = '';
  String _cvv = '';
  String _errorCVV = " ";

  String oldText = "";
  List<String> listMonthYear = List();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  @override
  void initState() {
    super.initState();
    Stripe.init('pk_test_u6D6abcpm6v1anhGIFFNQ87j00RdMbJ8vj');

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          // padding: EdgeInsets.only(top: 6),
          child: Text(
            'Moyens de paiement',
            // 'Foura',
            style: TextStyle(
              fontSize: 20, color: Colors.black54,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Card(
                  margin: EdgeInsets.all(0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            right: 15, left: 15, bottom: 10, top: 15),
                        child: Text(
                          'Payez avec votre carte bancaire en toute sécurité. MODE TEST ACTIVÉ. En mode test, vous pouvez utiliser le numéro de carte 4242424242424242 avec n\'importe quel cryptogramme visuel et une date d\'expiration valide on consulter la documentation Test Stripe pour obtenir plus de numéros de carte.',
                          // widget.ads.title,
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding:
                                EdgeInsets.only(right: 0, left: 5, bottom: 10),
                            child: Image.asset(
                              'images/visacard.png',
                              scale: 0.8,
                            ),
                          ),
                          Container(
                            padding:
                                EdgeInsets.only(right: 5, left: 5, bottom: 10),
                            child: Image.asset(
                              'images/mastercard.png',
                              scale: 0.85,
                            ),
                          ),
                          Container(
                            padding:
                                EdgeInsets.only(right: 5, left: 0, bottom: 10),
                            child: Image.asset(
                              'images/amexcard.png',
                              scale: 0.8,
                            ),
                          ),
                        ],
                      )
                    ],
                  )),
              Container(
                padding:
                    EdgeInsets.only(top: 0, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.bottomLeft,
                      padding: EdgeInsets.only(
                          top: 10, left: 10.0, right: 0.0, bottom: 0.0),
                      child: Text('Name on card'),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 15.0),
                      height: 50.0,
                      child: TextField(
                          controller: name,
                          onSubmitted: null,
                          keyboardType: TextInputType.text,
                          cursorColor: Colors.black,
                          decoration: (InputDecoration(
                            hintText: 'Type name on card',
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.only(top: 15, left: 15),
                          ))),
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      padding: EdgeInsets.only(
                          top: 0, left: 10.0, right: 0.0, bottom: 0.0),
                      child: Text('Card Number'),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 10, left: 5.0, right: 5.0, bottom: 15.0),
                      height: 50.0,
                      child: TextField(
                        // maxLength: 12,
                        maxLines: 2,

                        keyboardType: TextInputType.number,
                        controller: cardNumber,
                        cursorColor: Colors.black,
                        onSubmitted: null,
                        decoration: InputDecoration(
                          hintText: 'Type card number',
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                            top: 15,
                            left: 0,
                          ),
                          icon: Image.asset(
                            _cardNumber.length >= 1 &&
                                    _cardNumber.substring(0, 1).contains('3')
                                ? 'images/amexcard.png'
                                : _cardNumber.length >= 1 &&
                                        _cardNumber
                                            .substring(0, 1)
                                            .contains('4')
                                    ? 'images/visacard.png'
                                    : _cardNumber.length >= 1 &&
                                            _cardNumber
                                                .substring(0, 1)
                                                .contains('5')
                                        ? 'images/mastercard.png'
                                        : 'images/Group 699.png',
                            height: 20,
                            width: 50,
                          ),
                        ),
                        onChanged: (value) {
                          setState(() {
                            _cardNumber = value;
                          });
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              top: 0, left: 10.0, right: 0.0, bottom: 0.0),
                          child: Text('Expiry day'),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 0, left: 0.0, right: 70.0, bottom: 0.0),
                          child: Text('Security code'),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 150,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[200]),
                              margin: EdgeInsets.only(
                                  top: 10, left: 5.0, right: 5.0, bottom: 5.0),
                              height: 50.0,
                              child: ExpirationFormField(
                                controller: mmyy,
                                decoration: InputDecoration(
                                  hintText: 'MM / YY',
                                  hintStyle: TextStyle(fontSize: 13),
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                    top: 15,
                                    left: 15,
                                  ),
                                ),
                              ),
                              
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              width: 150,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[200]),
                              margin: EdgeInsets.only(
                                  top: 10, left: 0.0, right: 5.0, bottom: 5.0),
                              height: 50.0,
                              child: TextField(
                                maxLength: 3,
                                // maxLines: 2,
                                keyboardType: TextInputType.phone,
                                cursorWidth: 1,
                                // controller: cvv,
                                onSubmitted: null,
                                cursorColor: Colors.black,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    padding: EdgeInsets.only(top: 20),
                                    icon: Icon(Icons.help),
                                    iconSize: 18,
                                    onPressed: () {
                                      dialogCvv(context);
                                    },
                                  ),
                                  border: InputBorder.none,
                                  contentPadding: EdgeInsets.only(
                                    top: 40,
                                    left: 15,
                                  ),
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    _cvv = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 50, bottom: 40, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          // brand: _cardNumber.substring(0, 1).contains('3')
                          //     ? 'American Express'
                          //     : _cardNumber.substring(0, 1).contains('4')
                          //         ? 'Visa'
                          //         : _cardNumber
                          //                 .substring(0, 1)
                          //                 .contains('5')
                          //             ? 'Mastercard'
                          //             : 'Credit card',
                          if (_cvv == '') {
                            setState(() {
                              _errorCVV = "Enter a valid card code";
                            });
                            // else if (mmyy.text.contains("/") == false) {
                            //   _scaffoldKey.currentState.showSnackBar(SnackBar(
                            //     content:
                            //         Text("Wrong month or year or card number"),
                            //   ));
                          } else {
                            var split = mmyy.text.split('/');
                            int m;
                            int y;
                            m = int.parse(split[0]);
                            y = int.parse(split[1]);
                            StripeCard card = new StripeCard(
                                // name: name.text,
                                number: cardNumber.text,
                                cvc: _cvv,
                                expMonth: m,
                                expYear: y);
                            print(card);
                          
                            Stripe.instance
                                .createCardToken(card)
                                .then((token) async {
                              print(token.id.toString() + '_aa');
                              PostAddCardMethod()
                                  .addCardPayment(widget.accessToken, token.id)
                                  .then((respon) {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text(respon["message"].toString()),
                                ));
                                Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => S13_6_AddBlank2Screen(
                                      accessToken: widget.accessToken,
                                    )));
                              });
                            }).catchError((error) {
                              print(error);
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text(error.toString()),
                              ));
                            });
                          
                            
                          }
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Enregistrer les modifications',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<bool> dialogCvv(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
              onTap: () {},
              title: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 15, left: 10),
                            child: Text(
                              'Qu\'est-ce qu\'un code',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(bottom: 10, left: 5),
                            child: Text(
                              'de sécurité (CVV) ?',
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ],
                      ),
                      IconButton(
                        padding: EdgeInsets.only(top: 15),
                        icon: Icon(
                          Icons.close,
                          size: 18,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Le numéro de code de sécurité (CVV) est imprimé au dos de la carte, y compris 3 chiffres. Vous entrez ces 3 chiffres dans le formulaire pour compléter les informations.',
                      style: TextStyle(fontSize: 13, color: Colors.black54),
                    ),
                  ),
                  Container(
                    height: 100,
                    width: 200,
                    padding: EdgeInsets.only(
                      bottom: 20,
                    ),
                    decoration: BoxDecoration(
                        color: Colors.grey[350],
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 200,
                          child: Text(
                            '',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.grey[600],
                          width: 200,
                          child: Text(''),
                        ),
                        Container(
                          height: 10,
                          color: Colors.grey[350],
                          width: 200,
                          child: Text(''),
                        ),
                        Container(
                            height: 20,
                            // padding: EdgeInsets.only(left: 10, right: 10),
                            color: Colors.white,
                            width: 150,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(right: 5),
                                  child: Text(
                                    '1220',
                                    style: TextStyle(
                                        fontSize: 10, color: Colors.black26),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.redAccent,
                                        style: BorderStyle.solid,
                                        width: 1),
                                  ),
                                  child: Text(
                                    '123',
                                    style: TextStyle(fontSize: 10),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
