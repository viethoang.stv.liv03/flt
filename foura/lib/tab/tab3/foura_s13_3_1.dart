import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:foura/api/get_list_product.dart';
import 'package:foura/api/get_view_product.dart';
import 'package:foura/model/product.dart';
import 'package:foura/parse_number.dart';
import 'package:foura/tab/tab2/foura_s8_order.dart';

class S13_3_1Screen extends StatefulWidget {
  String accessToken;
  String stripeCardToken;
  int id;
  S13_3_1Screen({this.accessToken, this.stripeCardToken, this.id});
  @override
  S13_3_1ScreenState createState() => S13_3_1ScreenState();
}

class S13_3_1ScreenState extends State<S13_3_1Screen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  bool loading = true;
  ParseNumber parse = ParseNumber();
  List<Product> listProducts;
  String accessToken;

  Future getList() async {
    await ListProducts()
        .getListProducts(widget.stripeCardToken, widget.accessToken)
        .then((data) {
      setState(() {
        listProducts = data;
      });
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    getList();
    // getView();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: _colorWhite,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          leading: BackButton(
            color: Colors.grey,
          ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Boutique',
              // 'Foura',
              style: TextStyle(
                fontSize: 20, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20, top: 20),
                  child: Text('Voici les 2 résultats',
                      style: TextStyle(color: Colors.black45)),
                ),
                loading
                    ? Container()
                    : SingleChildScrollView(
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 10, bottom: 10, left: 10, right: 10),
                          child: Column(
                            children: listProducts.map((productItem) {
                              return Container(
                                margin: EdgeInsets.only(
                                    top: 5, bottom: 10, left: 5, right: 5),
                                child: GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) => S8_2Screen()));
                                    },
                                    child: Container(
                                      margin: EdgeInsets.all(10),
                                      height: 150,
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: productItem.id == 1
                                              ? Colors.green
                                              : _colorB),
                                      child: Row(
                                        // mainAxisAlignment:
                                        //     MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Container(
                                                padding: productItem.id == 1
                                                    ? EdgeInsets.only(
                                                        left: 10,
                                                        top: 20,
                                                        right: 5)
                                                    : EdgeInsets.only(
                                                        left: 10,
                                                        top: 20,
                                                        right: 0),
                                                child: Text(
                                                  // 'Abonnement 12 mois'
                                                  productItem.name,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 13),
                                                ),
                                              ),
                                              Container(
                                                padding: productItem.id == 1
                                                    ? EdgeInsets.only(
                                                        left: 0,
                                                        top: 5,
                                                        right: 0)
                                                    : EdgeInsets.only(
                                                        left: 0,
                                                        top: 5,
                                                        right: 20),
                                                child: Text(
                                                  productItem.id == 1
                                                      ? (parse.parseNumber(
                                                              productItem
                                                                  .subtotal
                                                                  .toString()) +
                                                          ' HT/année avec 1 année')
                                                      : (parse.parseNumber(productItem
                                                                      .subtotal
                                                                      .toString() !=
                                                                  0
                                                              ? '000'
                                                              : productItem
                                                                  .subtotal
                                                                  .toString()) +
                                                          ' HT/mois pour 3 mois'),
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14),
                                                ),
                                              ),
                                              Container(
                                                padding: productItem.id == 1
                                                    ? EdgeInsets.only(
                                                        left: 0,
                                                        top: 5,
                                                        right: 45)
                                                    : EdgeInsets.only(
                                                        left: 0,
                                                        top: 5,
                                                        right: 45),
                                                child: Text(
                                                  'avec ' +
                                                      productItem.trial_date
                                                          .toString() +
                                                      ' d\'essai gratuit',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.only(
                                                    top: 20,
                                                    right: 45,
                                                    left: 20),
                                                alignment:
                                                    Alignment.bottomCenter,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (_) =>
                                                                S8_2Screen(
                                                                  accessToken:
                                                                      widget
                                                                          .accessToken,
                                                                  product:
                                                                      productItem,
                                                                )));
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.only(
                                                        left: 20, right: 20),
                                                    height: 30,
                                                    width: 150,
                                                    decoration: BoxDecoration(
                                                        color: _colorWhite,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15)),
                                                    child: Center(
                                                        child: Text(
                                                      'S\'abonner',
                                                      style: TextStyle(
                                                          color:
                                                              productItem.id ==
                                                                      1
                                                                  ? Colors.green
                                                                  : _colorB,
                                                          fontSize: 13),
                                                    )),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              IconButton(
                                                // alignment: Alignment(0.5, 1),
                                                padding: EdgeInsets.only(
                                                    left: 30, bottom: 25),
                                                onPressed: () {
                                                  productItem.id == 1
                                                      ? dialog3month(context)
                                                      : dialog12month(context);
                                                },
                                                icon: Icon(
                                                  Icons.info,
                                                  color: _colorWhite,
                                                ),
                                              ),
                                              Container(
                                                alignment: Alignment(0.7, -0.5),
                                                padding: EdgeInsets.only(
                                                    bottom: 8, right: 30),
                                                child: Image.asset(
                                                  'images/logo.png',
                                                  fit: BoxFit.cover,
                                                  scale: 1.4,
                                                ),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    )),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
              ],
            ),
          ],
        ));
  }

  Future<bool> dialog3month(BuildContext context) {
    // Future getView() async {
    //   await ViewProduct()
    //       .viewProduct(widget.id, widget.accessToken)
    //       .then((data) {
    //     setState(() {
    //       listProducts = data;
    //     });
    //   });
    //   setState(() {
    //     loading = false;
    //   });
    // }

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => S10Screen()));
              },
              title: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 15, bottom: 10, left: 10),
                        child: Text(
                          'Abonnement 3 mois a Foura',
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      // IconButton(
                      //   padding: EdgeInsets.only(top: 15),
                      //   icon: Icon(
                      //     Icons.close,
                      //     size: 18,
                      //   ),
                      //   // child: Text('x'),
                      //   onPressed: () {
                      //     Navigator.pop(context);
                      //   },
                      // )
                      // CloseButton()
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20, left: 10),
                    child: Text(
                      '30.00€ HT / année avec 1 avec mois 90 d\'essai gratuit',
                      style: TextStyle(fontSize: 13, color: Colors.green),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      //   listProducts[0].description,

                      'L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.',
                      style: TextStyle(fontSize: 13, color: Colors.black45),
                    ),
                    // child: HtmlView(data: listProducts[0].description),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.',
                      style: TextStyle(fontSize: 13, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Testez gratuitement pendant 3 mois notre service : vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Vous avez déjà un abonnement actif pour ce produit.',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<bool> dialog12month(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => S10Screen()));
              },
              title: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 15, bottom: 10, left: 10),
                        child: Text(
                          'Abonnement 12 mois a Foura',
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      // IconButton(
                      //   padding: EdgeInsets.only(top: 15),
                      //   icon: Icon(
                      //     Icons.close,
                      //     size: 18,
                      //   ),
                      //   onPressed: () {
                      //     Navigator.pop(context);
                      //   },
                      // )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20, left: 10),
                    child: Text(
                      '0.00€ HT / mois pour 3 mois avec 90 d\'essai gratuit',
                      style: TextStyle(fontSize: 13, color: Colors.green),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      //   listProducts[1].description,
                      'L’utilisation de la plateforme pendant quelques mois a montré qu’une pharmacie peut facilement écouler plus de 500 € de médicaments qui allaient périmer.',
                      style: TextStyle(fontSize: 13, color: Colors.black45),
                    ),
                    // child: HtmlView(data: listProducts[1].description),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Certaines pharmacies ont aussi utilisé la plateforme pour se fournir en médicaments chers avec une marge supérieure à 50 %.',
                      style: TextStyle(fontSize: 13, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'L’intérêt de s’abonner à Stock2Big est donc évident : pour 30 € HT par an, vous publierez vos annonces en illimité et vous aurez accès aux coordonnées de ceux qui ont publiés.',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 15, left: 10, right: 10),
                    child: Text(
                      'Vous avez déjà un abonnement actif pour ce produit.',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
