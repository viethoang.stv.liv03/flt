import 'package:flutter/material.dart';
import 'package:foura/model/ad.dart';
import 'package:foura/tab/tab2/foura_s10_suggestion.dart';
import 'package:flutter/cupertino.dart';
import 'package:foura/tab/tab3/foura_s13_2_register.dart';
import 'package:foura/tab/tab3/foura_s13_2_afterDelete.dart';

class S13_2_EditDrugScreen extends StatefulWidget {
  String accessToken;
  Ads ads;
  S13_2_EditDrugScreen({Key key, this.accessToken, this.ads}) : super(key: key);
  @override
  S13_2_EditDrugScreenState createState() => S13_2_EditDrugScreenState();
}

class S13_2_EditDrugScreenState extends State<S13_2_EditDrugScreen> {
  bool _isLoading = false;

  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  TextEditingController t1 = TextEditingController();
  TextEditingController t2 = TextEditingController();
  TextEditingController t3 = TextEditingController();
  TextEditingController t4 = TextEditingController();
  TextEditingController t5 = TextEditingController();
  TextEditingController t6 = TextEditingController();

  int _selectedItemIndex = 0;
  int _selectedColorIndex = 0;

  bool isPick = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 6),
          child: Text(
            'Novalgin',
            style: TextStyle(fontSize: 20, color: Colors.black),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: t1,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Novalgin',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: t2,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: '3413257',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: t3,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: '1',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: TextField(
                          controller: t4,
                          onSubmitted: null,
                          decoration: (InputDecoration(
                            border: InputBorder.none,
                            hintText: '3.5',
                            hintStyle: TextStyle(fontSize: 15.0),
                            contentPadding: EdgeInsets.only(top: 15, left: 10),
                          ))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: ListTile(
                        title: GestureDetector(
                          // onTap: () async {
                          //   showModalBottomSheet(
                          //     context: context,
                          //     builder: (BuildContext context) {
                          //       return CupertinoPicker(
                          //         scrollController: scrollController,
                          //         itemExtent: 40,
                          //         backgroundColor: CupertinoColors.white,
                          //         onSelectedItemChanged: (int index) {
                          //           setState(() => _selectedColorIndex = index);
                          //         },
                          //         children: List<Widget>.generate(
                          //             _departement.length, (int index) {
                          //           return Center(
                          //             child: Text(
                          //               _departement[index],
                          //               style: TextStyle(
                          //                   color: Colors.black, fontSize: 14),
                          //             ),
                          //           );
                          //         }),
                          //       );
                          //     },
                          //   );
                          // },
                          child: _buildMenu(
                            <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 250,
                                    child: Text(
                                      // _departement[_selectedColorIndex],
                                      '31/05/2019',
                                      style: const TextStyle(
                                          color: CupertinoColors.inactiveGray),
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black54,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.grey[200]),
                      margin: EdgeInsets.only(
                          top: 20, left: 5.0, right: 5.0, bottom: 5.0),
                      height: 50.0,
                      child: ListTile(
                        title: GestureDetector(
                          // onTap: () async {
                          //   showModalBottomSheet(
                          //     context: context,
                          //     builder: (BuildContext context) {
                          //       return CupertinoPicker(
                          //         scrollController: scrollController,
                          //         itemExtent: 40,
                          //         backgroundColor: CupertinoColors.white,
                          //         onSelectedItemChanged: (int index) {
                          //           setState(() => _selectedColorIndex = index);
                          //         },
                          //         children: List<Widget>.generate(
                          //             _departement.length, (int index) {
                          //           return Center(
                          //             child: Text(
                          //               _departement[index],
                          //               style: TextStyle(
                          //                   color: Colors.black, fontSize: 14),
                          //             ),
                          //           );
                          //         }),
                          //       );
                          //     },
                          //   );
                          // },
                          child: _buildMenu(
                            <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width: 250,
                                    child: Text(
                                      // _departement[_selectedColorIndex],
                                      'Cher',
                                      style: const TextStyle(
                                          color: CupertinoColors.inactiveGray),
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black54,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 50, bottom: 10, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) => S13_2_RegisterScreen(
                                    accessToken: widget.accessToken,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              color: _colorB,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Modifier l\'annonce',
                            style: TextStyle(color: Colors.white, fontSize: 17),
                          )),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 10, bottom: 40, left: 20, right: 20),
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          dialogButton2(context);
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.red),
                              color: _colorWhite,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                              child: Text(
                            'Effacer',
                            style: TextStyle(color: Colors.red, fontSize: 17),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: SafeArea(
          top: true,
          bottom: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }

  Future<bool> dialogButton2(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
                onTap: () {},
                title: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                      height: 30,
                      child: Text('Voulez-vous vous désabonner?'),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: FlatButton(
                              child: Text(
                                'Annuler',
                                style: TextStyle(color: _colorB),
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Container(
                            child: FlatButton(
                              child: Text(
                                'D\'accord',
                                style: TextStyle(color: _colorB),
                              ),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) => S13_2_AfterDeleteScreen(accessToken: widget.accessToken,)));
                              },
                            ),
                          ),
                        ]),
                  ],
                )),
          );
        });
  }
}
