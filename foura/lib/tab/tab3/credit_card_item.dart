import 'package:foura/model/card.dart';
import 'package:flutter/material.dart';

class CreditCardItem extends StatefulWidget {
  CreditCard creditCard;
  CreditCardItem({this.creditCard});
  @override
  _CreditCardItemState createState() => _CreditCardItemState();
}

class _CreditCardItemState extends State<CreditCardItem> {
  double _sizeText = 15.0;
  var _white = Colors.white;
  var _black = Colors.black;
  double _heightContainer = 63.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Colors.grey,
        ),
        elevation: 1,
        centerTitle: true,
        title: Text(
          widget.creditCard.brand,
          style: TextStyle(
              fontSize: 20.0, color: _black, fontWeight: FontWeight.w400),
        ),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: _heightContainer,
            padding: EdgeInsets.only(left: 16, right: 15),
            margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 2.0),
            alignment: Alignment.centerLeft,
            child: Text(
              '**** **** **** ' + widget.creditCard.last4,
              style: TextStyle(color: _black, fontSize: _sizeText),
            ),
            color: Colors.grey.withOpacity(0.2),
          ),
          Container(
            height: _heightContainer,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(left: 16, right: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Expiry date',
                    style: TextStyle(
                        color: _black,
                        fontWeight: FontWeight.w200,
                        fontSize: 12)),
                Text(
                    widget.creditCard.expMonth.toString() +
                        "/" +
                        widget.creditCard.expYear.toString(),
                    style: TextStyle(color: _black, fontSize: _sizeText))
              ],
            ),
            color: Colors.grey.withOpacity(0.2),
          ),
        ],
      ),
    );
  }
}
