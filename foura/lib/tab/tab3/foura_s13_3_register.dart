import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:foura/tab/tab3/foura_s13_3_detail.dart';
import 'package:foura/tab/tab3/foura_s13_tab3.dart';
import 'package:foura/tab/tabs_screen.dart';

class S13_3_RegisterScreen extends StatefulWidget {
  @override
  S13_3_RegisterScreenState createState() => S13_3_RegisterScreenState();
}

class S13_3_RegisterScreenState extends State<S13_3_RegisterScreen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: _colorWhite,
          elevation: 1,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => TabsScreen()));
          },
            color: Colors.grey,
          ),
          title: Container(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              'Modifier mon adresse',
              // 'Foura',
              style: TextStyle(
                fontSize: 16, color: Colors.black,
                // fontFamily: 'Gugi'
              ),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
               
              
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 10.0,
                    left: 10.0,
                    right: 10.0,
                  ),
                  height: 100.0,
                  child: ListTile(
                      // color: Colors.white,
                      // onPressed: () {},
                      title: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top:5,left: 5,bottom: 5),
                                child: Text(
                                  'ID:',
                                  style: TextStyle(
                                      color: Colors.black45, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top:5,left: 2,bottom: 5),
                                child: Text(
                                  '#1009',
                                  style: TextStyle(
                                      color: Colors.black87, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 5,bottom: 5),
                                child: Text(
                                  'État:',
                                  style: TextStyle(
                                      color: Colors.black45, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 2,bottom: 5),
                                child: Text(
                                  'Actif',
                                  style: TextStyle(
                                      color: Colors.black87, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 5,bottom: 5),
                                child: Text(
                                  'Prochain paiement:',
                                  style: TextStyle(
                                      color: Colors.black45, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 2,bottom: 5),
                                child: Text(
                                  '30/08/2019',
                                  style: TextStyle(
                                      color: Colors.black87, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 5,bottom: 5),
                                child: Text(
                                  'Total:',
                                  style: TextStyle(
                                      color: Colors.black45, fontSize: 14),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 2,bottom: 5),
                                child: Text(
                                  '0,00€',
                                  style: TextStyle(
                                      color: Colors.black87, fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      trailing: Container(
                        child: Icon(Icons.chevron_right),
                        padding: EdgeInsets.only(top: 20),
                      ),
                      onTap: (){
                        Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => S13_3_DetailScreen()));
                      },
                      ),
                ),
              ],
            ),
          ],
        ));
  }
}
