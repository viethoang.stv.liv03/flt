import 'package:flutter/material.dart';
import 'package:foura/tab/tab3/foura_s13_6_AddBlank1.dart';

class S13_6Screen extends StatefulWidget {
  String accessToken;
  S13_6Screen({this.accessToken});
  @override
  S13_6ScreenState createState() => S13_6ScreenState();
}

class S13_6ScreenState extends State<S13_6Screen> {
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: _colorWhite,
        elevation: 1,
        centerTitle: true,
        leading: BackButton(
          color: Colors.grey,
        ),
        title: Container(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            'Moyen de paiment',
            // 'Foura',
            style: TextStyle(
              fontSize: 16, color: Colors.black,
              // fontFamily: 'Gugi'
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(0.0),
                      color: Colors.white),
                  margin: EdgeInsets.only(
                    top: 1,
                    left: .0,
                    right: 0.0,
                  ),
                  height: 50.0,
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: Text('Aucun moyen sauvegardé trouvé.',
                        style: TextStyle(fontSize: 14, color: Colors.black45)),
                  )),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    borderRadius: BorderRadius.circular(0.0),
                    color: Colors.white),
                margin: EdgeInsets.only(
                  top: 1,
                  left: 0.0,
                  right: 0.0,
                ),
                height: 55.0,
                child: ListTile(
                  contentPadding: EdgeInsets.only(
                    left: 10.0,
                    right: 10.0,
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => S13_6_AddBlank1Screen(accessToken: widget.accessToken,)));
                  },
                  title: Text(
                    'Ajouter un moyen de paiment',
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                  leading: Icon(Icons.credit_card),
                  trailing: Icon(Icons.chevron_right),
                ),
              ),
              
            ],
          ),
        ],
      ),
    );
  }
}
