import 'package:flutter/material.dart';
import 'package:foura/api/app_bloc.dart';
import 'package:foura/model/workplace.dart';
import 'package:foura/api/get_workplace.dart';
class MyAddress extends StatefulWidget {
  AppBloc appBloc;
  GetWorkplace getWorkplace = GetWorkplace() ;
  MyAddress({Key key, this.appBloc}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return MyAddressState();
  }
}

class MyAddressState extends State<MyAddress> {
  List<WorkPlace> list = List();
  _fetchWorkplace() async {
    try{
      var workplace = await widget.getWorkplace.getWorkplace(widget.appBloc.getAccessToken());
       setState(() {
      list.addAll(workplace);
    });
    }catch(e){
      print('111111111111111111111');
    }
      
   
  }

  @override
  void initState() {
   
    super.initState();
    _fetchWorkplace();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Color.fromARGB(170, 0, 204, 204),
          title: Text('Address',style: TextStyle(fontSize: 15),),
          actions: <Widget>[
            
          ],
        ),
        body: ListView(
            children: list.map((workplace) {
        }).toList()),
        );
  }
}
