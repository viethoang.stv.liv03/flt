class CardInfo {
  String cvv;
  int expMonth;
  int expYear;
  String brand;
  String last4;
  String country;
  String name;
  CardInfo(
      {this.name,
      this.brand,
      this.cvv,
      this.expMonth,
      this.expYear,
      this.last4,
      this.country});
  factory CardInfo.internalFromJson(Map jsonMap) {
    return CardInfo(
      name: jsonMap["name"] as String ?? " ",
      cvv: jsonMap["pur_pcvc_checkice"] as String ?? " ",
      expMonth: jsonMap["exp_month"] as int,
      expYear: jsonMap["exp_year"] as int,
      last4: jsonMap["last4"] as String ?? " ",
      brand: jsonMap["brand"] as String ?? " ",
      country: jsonMap["country"] as String ?? " ",
    );
  }
}
