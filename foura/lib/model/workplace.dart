class WorkPlace {
  int id;
  String name;

  WorkPlace({this.id, this.name});
  factory WorkPlace.internalFromJson(Map jsonMap) {
    return WorkPlace(
      id: jsonMap["id"] as int,
      name: jsonMap["name"] as String);
  }
}
