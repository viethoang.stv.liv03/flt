import 'package:meta/meta.dart';

class Product {
  int id;
  String name;
  String interval;
  int interval_count;
  int trial_date;
  int subtotal;
  int total;
  String description;
  

  Product({
    this.id,
    this.name,
    this.interval,
    this.interval_count,
    this.trial_date,
    this.subtotal,
    this.total,
    this.description,
  });
  factory Product.internalFromJson(Map jsonMap) {
    return Product(
      id: jsonMap["id"] as int,
      name: jsonMap["name"] as String,
      interval: jsonMap["interval"] as String,
      interval_count: jsonMap["interval_count"] as int,
      trial_date: jsonMap["trial_date"] as int,
      subtotal: jsonMap["subtotal"] as int,
      total: jsonMap["total"] as int,
      description: jsonMap["description"] as String,
    );
  }
}
