class Ads {
  int id;
  String title;
  int workplace_id;
  String workplace_name;
  String alc_code;
  String price;
  int amount;
  int expire_date;

  Ads({
    this.id,
    this.title,
    this.workplace_id,
    this.workplace_name,
    this.alc_code,
    this.price,
    this.amount,
    this.expire_date,
  });
  factory Ads.internalFromJson(Map jsonMap) {
    return Ads(
      id: jsonMap["id"] as int,
      title: jsonMap["title"] as String ?? " ",
      workplace_id: jsonMap["workplace_id"] as int,
      workplace_name: jsonMap["workplace_name"] as String ?? " ",
      alc_code: jsonMap["alc_code"] as String ?? " ",
      price: jsonMap["price"] as String ?? " ",
      amount: jsonMap["amount"] as int,
      expire_date: jsonMap["expire_date"] as int,
    );
  }
}
