class CreditCard {
  int id;

  String brand;
  int userId;
  int expMonth;
  int expYear;
  String last4;
  String name;
  bool isSelected;
  CreditCard(
      {this.id,
      this.brand,
      this.name,
      this.last4,
      this.expMonth,
      this.expYear,
      this.userId,
      this.isSelected});
  factory CreditCard.internalFromJson(Map jsonMap) {
    return CreditCard(
      id: jsonMap["id"] as int,
      brand: jsonMap["card_brand"] as String ?? " ",
      name: jsonMap["name"] != null ? jsonMap["name"] as String : " ",
      last4: jsonMap["card_last_four"] as String ?? " ",
      expMonth: jsonMap["expMonth"] as int,
      expYear: jsonMap["expYear"] as int,
      userId: jsonMap["user_id"] as int,
    );
  }
}
