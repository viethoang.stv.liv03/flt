class User {
  int id;
  String username;
  String default_card_id;
  String stripe_customer_id;
  String phone_number;
  String first_name;
  String last_name;

  String email;
  String company;
  String street;
  String city;
  String country;
  String postal_code;
  String email_verified_at;
  String created_ad;
  String updated_at;
  User({
    this.id,
    this.username,
    this.default_card_id,
    this.stripe_customer_id,
    this.phone_number,
    this.first_name,
    this.last_name,
    this.email,
    this.company,
    this.street,
    this.city,
    this.country,
    this.postal_code,
    this.email_verified_at,
    this.created_ad,
    this.updated_at,
  });
  factory User.internalFromJson(Map jsonMap) {
    return User(
      id: jsonMap["id"] as int,
      username: jsonMap["username"] as String ?? " ",
      default_card_id: jsonMap["default_card_id"] as String ?? " ",
      stripe_customer_id: jsonMap["stripe_customer_id"] as String ?? " ",
      phone_number: jsonMap["phone_number"] as String ?? " ",
      first_name: jsonMap["first_name"] as String ?? " ",
      last_name: jsonMap["last_name"] as String ?? " ",
      email: jsonMap["email"] as String ?? " ",
      company: jsonMap["company"] as String ?? " ",
      street: jsonMap["street"] as String ?? " ",
      city: jsonMap["city"] as String ?? " ",
      country: jsonMap["country"] as String ?? " ",
      postal_code: jsonMap["postal_code"] as String ?? " ",
      email_verified_at: jsonMap["email_verified_at"] as String ?? " ",
      created_ad: jsonMap["created_at"] as String ?? " ",
      updated_at: jsonMap["updated_at"] as String ?? " ",
    );
  }
}
