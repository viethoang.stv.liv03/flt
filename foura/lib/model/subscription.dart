class Subscription {
  int id;
  String status;
  int next_payment_date;
  int total;
  Subscription({
    this.id,
    this.status,
    this.next_payment_date,
    this.total,
  });
  factory Subscription.internalFromJson(Map jsonMap) {
    return Subscription(
      id: jsonMap["id"] as int,
      status: jsonMap["status"] as String ?? " ",
      next_payment_date: jsonMap["next_payment_date"] as int,
      total: jsonMap["total"] as int,
    );
  }
}
