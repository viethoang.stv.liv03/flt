class Order {
  int product_id;
  String first_name;
  String phone;
  String country;
  String last_name;
  String company;
  String siren;
  String rpps;
  String street;
  String city;
  String email;
  int card_id;
  String created_date;
  String next_payment;
  int total;
  String product_name;
  int product_subtotal;
  int product_total;
  int status;
  Order({
    this.product_id,
    this.first_name,
    this.phone,
    this.country,
    this.last_name,
    this.company,
    this.siren,
    this.rpps,
    this.street,
    this.city,
    this.email,
    this.card_id,
    this.created_date,
    this.next_payment,
    this.total,
    this.product_name,
    this.product_subtotal,
    this.product_total,
    this.status,
  });
  factory Order.internalFromJson(Map jsonMap) {
    return Order(
      product_id: jsonMap["product_id"] as int,
      first_name: jsonMap["first_name"] as String ?? " ",
      phone: jsonMap["phone"] as String ?? " ",
      country: jsonMap["country"] as String ?? " ",
      last_name: jsonMap["last_name"] as String ?? " ",
      company: jsonMap["company"] as String ?? " ",
      siren: jsonMap["siren"] as String ?? " ",
      rpps: jsonMap["rpps"] as String ?? " ",
      street: jsonMap["street"] as String ?? " ",
      city: jsonMap["city"] as String ?? " ",
      email: jsonMap["email"] as String ?? " ",
      card_id: jsonMap["card_id"] as int,
      created_date: jsonMap["created_date"] as String ?? " ",
      next_payment: jsonMap["next_payment"] as String ?? " ",
      total: jsonMap["total"] as int,
      product_name: jsonMap["product_name"] as String ?? " ",
      product_subtotal: jsonMap["product_subtotal"] as int,
      product_total: jsonMap["product_total"] as int,
      status: jsonMap["status"] as int,
    );
  }
}
