import 'dart:convert';

import 'package:foura/model/card.dart';
import 'package:http/http.dart' as http;

class ViewCard {
  Future<List<CreditCard>> viewCard(String accessToken) async {
    var response = await http.get(
      "http://foura.stdiohue.com/api/payment/view-cards",
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );
    print('token: ' + accessToken.toString());
    print('message: ' + json.decode(response.body)['message'].toString());
    print('data: ' + json.decode(response.body)['data'].toString());
    if (response.statusCode == 200) {
      print('111');
      if (json.decode(response.body)['data'] != []) {
        print('333');
        final parsedCard =
            json.decode(response.body)['data'].cast<Map<String, dynamic>>();
        return parsedCard
            .map<CreditCard>((value) => CreditCard.internalFromJson(value))
            .toList();
      }
    }
    return Future.error("error");
  }
}
