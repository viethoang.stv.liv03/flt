import 'dart:convert';

import 'package:http/http.dart' as http;

class PostAddCardMethod {
  Future addCardPayment(String accessToken, String stripeCardToken) async {
    var response = await http.post(
        "http://foura.stdiohue.com/api/payment/add-card-via-token",
        headers: {
          "Authorization": "Bearer $accessToken",
        },
        body: {
          "stripe_card_token": stripeCardToken,
        });
        print('data: ' + json.decode(response.body)["message"].toString());
    print('data: ' + json.decode(response.body)["data"].toString());
    return json.decode(response.body);
  }
}
