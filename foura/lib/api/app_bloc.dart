import 'dart:async';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';
import 'package:foura/api/app_state.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:foura/model/user.dart';

class AppBloc {
  int check = 0;
  final _appState = BehaviorSubject<AppState>();
  updateUser(AppState state) {
    _appState.add(state);
  }

  Stream<AppState> get appState => _appState.stream;
  bool isLoading = false;
  String _accessTokenLogin;
  Future<bool> saveLogged() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoadingSave =
        (sharedPreferences.getBool('isLoadingSave') ?? isLoading);
    if (check == 1) isLoadingSave = isLoading;
    await sharedPreferences.setBool('isLoadingSave', isLoadingSave);
    return isLoadingSave;
  }

  Future<String> saveaccessToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String saveAccess =
        (sharedPreferences.getString('saveAccess') ?? _accessTokenLogin);
    if (check == 1) saveAccess = _accessTokenLogin;
    print(check);
    await sharedPreferences.setString('saveAccess', saveAccess);
    return saveAccess;
  }

  Future postSignin(String email, String password) async {
    var response = await http.post("http://foura.stdiohue.com/api/auth/login",
        
        body: {'email': email, 'password': password});
        print(json.decode(response.body)["message"]);
    if (response.statusCode == 200) {
      // print('ok');
      print(json.decode(response.body)["data"]["access_token"]);
      if (json.decode(response.body)["message"] == "Successfully Login") {
        print('done');
        // _accessTokenLogin =
        //     await json.decode(response.body)["data"]["access_token"];

        return json.decode(response.body);
      }
      // else {
      //   print('failed');
      // }

      return json.decode(response.body)["message"];
    } else {
      print(json.decode(response.body)["message"]);
      return json.decode(response.body)["message"];
    }
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    updateUser(AppState(isLoading));

    return Future.error("error");
  }

  Future<String> postSignup(
    String username,
    String email,
    String password,
  ) async {
    var response = await http.post("http://foura.stdiohue.com/api/auth/signup",
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        body: {'email': email, 'password': password, 'username': username});
    if (response.statusCode == 200) {
      print(json.decode(response.body)["message"]);
      _accessTokenLogin =
          await json.decode(response.body)["data"]["access_token"];

      return json.decode(response.body)["message"];
    }
    return Future.error("error");
  }

  Future<String> postAds() async {
    var response = await http.post(
      "http://foura.stdiohue.com/api/ad/get-list?page=1&limit=20",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    );
    if (response.statusCode == 200) {
      if (json.decode(response.body)['message'] == "success") {
        print("acc");
        // _accessTokenLogin =
        //     await
        print(json.decode(response.body)["data"]);
      }
    }
    return Future.error("error");
  }

  // Future<String> getUser(String access_token) async {
  //   var response = await http.get(
  //     "http://foura.stdiohue.com/api/auth/me",
  //     headers: {
  //       "Content-Type": "application/x-www-form-urlencoded",
  //       "Authorization": "Bearer $access_token"
  //     },
  //   );
  //   print(response.body);
  //   if (response.statusCode == 200) {
  //     print(json.decode(response.body)["message"]);
  //     if (json.decode(response.body)["message"] == "Retrieve success") {
  //       print(json.decode(response.body)["data"]);
  //     }

  //     return json.decode(response.body)["message"];
  //   } else {
  //     print('wtf');
  //     print(json.decode(response.body)["message"]);
  //     return json.decode(response.body)["message"];
  //   }
  //   print("a");
  //   return Future.error("error");
  // }

  String _accessTokenRegister;

  bool getIsLoading() {
    saveLogged().then((value) {
      isLoading = value;
    });
    saveLogged();
    return isLoading;
  }

  String getAccessToken() {
    saveaccessToken().then((onValue) {
      _accessTokenLogin = onValue;
    });
    print(_accessTokenLogin);
    saveaccessToken().then((onValue) {
      _accessTokenLogin = onValue;
    });
    return _accessTokenLogin;
  }
}
