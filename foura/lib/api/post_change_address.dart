import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

class AddAddress {
  Future<String> addAddress(
      String first_name,
      String last_name,
      String company,
      String street,
      String city,
      String country,
      String postal_code,
      String phone,
      String accessToken) async {
    var response = await http
        .post("http://foura.stdiohue.com/api/payment/changeAddress", headers: {
      "Authorization": "Bearer $accessToken",
    }, body: {
      'first_name': first_name,
      'last_name': last_name,
      'company': company,
      'street': street,
      'city': city,
      'country': country,
      'postal_code': postal_code,
      'phone': phone
    });
    if (response.statusCode == 200) {
    print(json.decode(response.body)["message"].toString());
    print(
        'data: ' + json.decode(response.body)["data"].toString() + 'hhhhhhhh');

    return json.decode(response.body);
    }
    // return json.decode(response.body)["message"].toString();
  }
}
