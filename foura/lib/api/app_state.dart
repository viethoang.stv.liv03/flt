class AppState {
  AppState(this.isLogin);

  final bool isLogin;

  bool get isLogged => isLogin == true;
}
