import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:foura/model/ad.dart';

class ListAds{
  Future<List<Ads>> getListAds() async {
    var response =
        await http.post("http://foura.stdiohue.com/api/ad/get-list?page=1&limit=300");
    
    print(json.decode(response.body)['message'].toString());
    print(json.decode(response.body)['data'].toString().substring(0,500) + '11111111111111111');
    if (response.statusCode == 200) {
      if (json.decode(response.body)['message'] == "Success") {
       final parsedAds =
           json.decode(response.body)['data']['data'].cast<Map<dynamic, dynamic>>();
       return parsedAds
           .map<Ads>((value) => Ads.internalFromJson(value))
           .toList();
      }
    }
    return Future.error("error");
  }
}