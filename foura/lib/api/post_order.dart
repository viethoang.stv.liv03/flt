import 'dart:convert';
import 'package:http/http.dart' as http;

class Order {
  Future postOrder(
    int product_id,
    String first_name,
    String phone,
    String country,
    String last_name,
    String company,
    int siren,
    String rpps,
    String street,
    String city,
    String email,
    int card_id,
    String accessToken,
  ) async {
    var response = await http
        .post("http://foura.stdiohue.com/api/payment/order", headers: {
      "Authorization": "Bearer $accessToken",
    }, body: {
      'product_id': product_id,
      'first_name': first_name,
      'phone': phone,
      'country': country,
      'last_name': last_name,
      'company': company,
      'siren': siren,
      'rpps': rpps,
      'street': street,
      'city': city,
      'email': email,
      'card_id': card_id,
    });
    print('message: ' + json.decode(response.body)["message"].toString());
    print('order: ' + json.decode(response.body)["data"]["order"].toString());
    print('subscription: ' + json.decode(response.body)["data"]["subscription"].toString());
    print('address: ' + json.decode(response.body)["data"]["address"].toString());

    if (response.statusCode == 200) {
      print('message: ' + json.decode(response.body)["message"].toString());
      print('data address: ' + json.decode(response.body)["data"]["address"].toString());
      if (json.decode(response.body)['message'] == "Create successfully") {
        print(json.decode(response.body)["message"].toString() + ' !!!');
        print('data subscription: ' +
            json.decode(response.body)["data"]["subscription"].toString() +
            ' hehe');
      }
      return json.decode(response.body);
    }
  }
}
