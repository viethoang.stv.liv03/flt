import 'dart:convert';

import 'package:foura/model/product.dart';
import 'package:http/http.dart' as http;

class ViewProduct {
  Future viewProduct(int id, String accessToken) async {
    var response = await http.get(
      "http://foura.stdiohue.com/api/product/view-detail/$id",
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );

    print('message: ' + json.decode(response.body)['message'].toString());
    print('data: ' + json.decode(response.body)['data'].toString());

    if (response.statusCode == 200) {
      print('111');
      if (json.decode(response.body)['message'] == "Successfully") {
        print('222');
        print('data: ' + json.decode(response.body)['data'].toString());
      }
      return Product.internalFromJson(json.decode(response.body)["data"]);
    }
  }
}
