import 'dart:convert';

import 'package:foura/model/product.dart';
import 'package:http/http.dart' as http;

class ListProducts {
  Future<List<Product>> getListProducts(
      String stripeCardToken, String accessToken) async {
    var response = await http
        .get("http://foura.stdiohue.com/api/product/get-list", headers: {
      "Authorization": "Bearer $accessToken",
    }, 
    // body: {
    //   "stripe_card_token": stripeCardToken,
    // }
    );

    print(json.decode(response.body)['message'].toString());
    print(json.decode(response.body)['data'].toString() +
        '22222222222');
    if (response.statusCode == 200) {
      if (json.decode(response.body)['message'] == "Successfully") {
        final parsedProduct = json
            .decode(response.body)['data']['data']
            .cast<Map<dynamic, dynamic>>();
        return parsedProduct
            .map<Product>((value) => Product.internalFromJson(value))
            .toList();
      }
    }
    return Future.error("error");
  }
}
