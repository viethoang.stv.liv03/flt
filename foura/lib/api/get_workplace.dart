import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:foura/model/workplace.dart';

class GetWorkplace {
   Future<List<WorkPlace>> getWorkplace(String accessToken) async {
    var response = await http.get(
      "https://groupbuy.vn/api/v1/user/show_address_user",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Bearer $accessToken"
      },
    );
    print(response.body);
    if (response.statusCode == 200) {
      final parsedAddress =
          json.decode(response.body)['data'].cast<Map<String, dynamic>>();
      return parsedAddress
          .map<WorkPlace>((value) => WorkPlace.internalFromJson(value))
          .toList();
    }
    return Future.error("error");
  }

}