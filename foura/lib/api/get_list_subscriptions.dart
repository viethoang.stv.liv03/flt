import 'dart:convert';

import 'package:foura/model/order.dart';

import 'package:http/http.dart' as http;

class ListSubscriptions {
  Future<List<Order>> getListSubscriptions(String accessToken) async {
    var response = await http.get(
      "http://foura.stdiohue.com/api/payment/view-list-subscriptions",
      headers: {
        "Authorization": "Bearer $accessToken",
      },
    );

    print(json.decode(response.body)['message'].toString());
    print(json.decode(response.body)['data'].toString() + ' list order');
    if (response.statusCode == 200) {
      if (json.decode(response.body)['message'] == "Successfully") {
        final parsedOrder = json
            .decode(response.body)['data']['data']
            .cast<Map<dynamic, dynamic>>();
        return parsedOrder
            .map<Order>((value) => Order.internalFromJson(value))
            .toList();
      }
    }
    return Future.error("error");
  }
}
