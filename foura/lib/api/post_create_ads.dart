import 'dart:convert';
import 'package:http/http.dart' as http;

class CreateAds {
  Future createAds(
    String title,
    String alccode,
    String price,
    String quantity,
    int expireday,
    String workplaceId,
    String accessToken,
  ) async {
    var response =
        await http.post("http://foura.stdiohue.com/api/ad/create-ad", headers: {
      "Authorization": "Bearer $accessToken",
    }, body: {
      'title': title,
      'alc_code': alccode,
      'price': price,
      'quantity': quantity,
      'expire_date': expireday.toString(),
      'workplace_id': workplaceId
    });
    print('data: ' + json.decode(response.body)["message"].toString());
    print('data: ' + json.decode(response.body)["data"].toString());

    if (response.statusCode == 200) {
      print('data: ' + json.decode(response.body)["message"].toString());
      print('data: ' + json.decode(response.body)["data"].toString());
      if (json.decode(response.body)['message'] == "Create successfully") {
        print(json.decode(response.body)["message"].toString() + 'aaaa');
        print('data: ' +
            json.decode(response.body)["data"].toString() +
            'hhhhhhhh');
      }
      return json.decode(response.body);
    }
  }
}
