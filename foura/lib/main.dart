import 'package:flutter/material.dart';

import 'package:foura/login_reg/foura_s0.dart';
import 'package:foura/api/app_bloc.dart';

// void main() => runApp(MyApp());
void main() {
  AppBloc appBloc = AppBloc();
  runApp(new MyApp(
    appBloc: appBloc,
  ));
}

class MyApp extends StatelessWidget {
  AppBloc appBloc = AppBloc();

  MyApp({
    Key key,
    this.appBloc,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
      theme: ThemeData(primaryColor: Colors.white, hintColor: Colors.white),
    );
  }
}

// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       home: MyHomePage(title: 'Flutter Demo Home Page'),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);

//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   DateTime selectedDate = DateTime.now();
//   // final f = new DateFormat('yyyy-MM-dd hh:mm');
//   DateFormat _format = DateFormat("d/M/y");

//   Future<Null> _selectDate(BuildContext context) async {
//     final DateTime picked = await showDatePicker(
//         context: context,
//         initialDate: selectedDate,
//         firstDate: DateTime(2019, 1,1),
//         lastDate: DateTime(2101));
//     if (picked != null && picked != selectedDate)
//       setState(() {
//         selectedDate = picked;
//       });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Container(
//               child: Row(children: <Widget>[
//                 Text("${_format.format(selectedDate)}"),
//                 IconButton(
//                   icon: Icon(Icons.calendar_today),
//                    onPressed: () => _selectDate(context),
//                 )
//               ],),
//             ),
//             Text("${_format.format(selectedDate)}"),
//             SizedBox(height: 20.0,),
//             RaisedButton(
//               onPressed: () => _selectDate(context),
//               child: Text('Select date'),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
