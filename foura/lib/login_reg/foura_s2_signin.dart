import 'package:foura/login_reg/foura_s1_signup.dart';
import 'package:flutter/material.dart';
import 'package:foura/tab/tabs_screen.dart';
import 'package:foura/api/app_bloc.dart';

class SignInScreen extends StatefulWidget {
  final AppBloc appBloc;

  const SignInScreen({Key key, this.appBloc}) : super(key: key);
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  AppBloc _appBloc = AppBloc();
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);

  String _email;
  String _password;

  String _errorMessage;
  bool _isLoading = false;

  Widget showCircularProgress() {
    if (_isLoading) {
      print('aaaaaaaaaaaaaaaa');
      return Center(
          child: CircularProgressIndicator(
        backgroundColor: Colors.white,
      ));
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _colorB,
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 50.0),
              child: Image.asset(
                "images/foura_logo.png",
                fit: BoxFit.cover,
              )),
          Center(
            child: Container(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    cursorColor: _colorWhite,
                    style: TextStyle(color: _colorWhite),
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.email,
                          color: _colorWhite,
                          size: 18,
                        ),
                        hintText: 'Email',
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w200, color: _colorWhite)),
                    onChanged: (value) {
                      print(value);
                      _email = value;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    autofocus: false,
                    cursorColor: _colorWhite,
                    style: TextStyle(color: _colorWhite),
                    obscureText: true,
                    decoration: InputDecoration(
                        icon: Icon(
                          Icons.lock,
                          color: _colorWhite,
                          size: 18,
                        ),
                        hintText: 'Password',
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w200, color: _colorWhite)),
                    onChanged: (value) {
                      print(value);
                      _password = value;
                    },
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 5),
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "${_errorMessage ?? ""}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.w300),
                      )),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 150, left: 20, right: 20),
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).pushReplacement(
                //     MaterialPageRoute(builder: (_) => TabsScreen()));
                setState(() {
                  _isLoading = true;
                });

                _appBloc
                    .postSignin(_email, _password)
                    .then((accesstoken) async {
                  setState(() {
                    _isLoading = false;
                  });
                  if (accesstoken["message"] == "Successfully Login") {
                    // _appBloc.getAccessToken();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (_) => TabsScreen(
                                accessToken: accesstoken["data"]
                                    ["access_token"])));
                  } else
                    setState(() {
                      _errorMessage = accesstoken["message"];
                    });
                });
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                decoration: BoxDecoration(
                    color: _colorWhite, borderRadius: BorderRadius.circular(6)),
                child: Center(
                    child: Text(
                  'Sign In',
                  style: TextStyle(
                      color: _colorB,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                )),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 120.0),
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Don\'t have account?',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: _colorWhite,
                        fontSize: 13)),
                GestureDetector(
                  onTap: () =>
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                          builder: (_) => SignUpScreen(
                                appBloc: widget.appBloc,
                              ))),
                  child: Container(
                    height: 15,
                    width: 50,
                    margin: EdgeInsets.only(left: 5, right: 0),
                    child: Text('Sign up',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: _colorWhite,
                            fontSize: 13)),
                  ),
                )
              ],
            ),
          ),
          _isLoading
              ? Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.height)
              : Container(
                  height: 0.0,
                  width: 0.0,
                ),
          showCircularProgress()
        ],
      ),
    );
  }
}
