import 'package:flutter/material.dart';
import 'package:foura/login_reg/foura_s1_signup.dart';
import 'package:foura/login_reg/foura_s2_signin.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  Color _colorB = Color.fromARGB(255, 0,162,224);
   Color _colorWhite = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: _colorB,
        body: new Stack(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 100.0),
                child: 
                Image.asset(
                  'images/foura_logo.png',
                  fit: BoxFit.cover,
                 
                )
                ),
            Container(
              alignment: Alignment(0, 0.65),
              child: RaisedButton(
                  padding: EdgeInsets.only(
                      left: 100.0, right: 100.0, top: 0.0, bottom: 0.0),
                  color: _colorWhite,
                  child: Text(
                    'Sign Up',
                    style: TextStyle(color: _colorB, fontSize: 15.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => SignUpScreen()));
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(6.0))),
            ),
            Container(
              alignment: Alignment(0, 0.8),
              child: RaisedButton(
                  padding: EdgeInsets.only(
                      left: 100.0, right: 100.0, top: 0.0, bottom: 0.0),
                  color: _colorWhite,
                  child: Text(
                    'Sign In',
                    style: TextStyle(color: _colorB, fontSize: 16.0),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => SignInScreen()));
                  },
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(6.0))),
            ),
          ],
        ));
  }
}
