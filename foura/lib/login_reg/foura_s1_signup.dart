import 'package:foura/login_reg/foura_s2_signin.dart';
import 'package:flutter/material.dart';
import 'package:foura/api/app_bloc.dart';
import 'package:foura/tab/tabs_screen.dart';

class SignUpScreen extends StatefulWidget {
  final AppBloc appBloc;

  const SignUpScreen({Key key, this.appBloc}) : super(key: key);
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  AppBloc _appBloc = AppBloc();
  String _errorMessage = "";
  bool isLoading = false;
  bool isChecked = false;
  String msg = "";
  Color _colorWhite = Colors.white;
  Color _colorB = Color.fromARGB(255, 0, 162, 224);
  String errorCheck = "politique de confidentialité de Foura";
  String _username;
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: _colorB,
      body: Stack(
        children: <Widget>[
          Container(
            color: _colorB,
            child: Stack(
              children: <Widget>[
                Container(
                    padding:
                        EdgeInsets.only(left: 40.0, right: 40.0, top: 50.0),
                    child: Image.asset(
                      "images/foura_logo.png",
                      fit: BoxFit.cover,
                    )),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextField(
                        cursorColor: _colorWhite,
                        style: TextStyle(color: _colorWhite),
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.person,
                              color: _colorWhite,
                              size: 18,
                            ),
                            hintText: 'Name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorWhite)),
                        onChanged: (value) {
                          print(value);
                          _username = value;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        cursorColor: _colorWhite,
                        style: TextStyle(color: _colorWhite),
                        // obscureText: true,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.email,
                              color: _colorWhite,
                              size: 18,
                            ),
                            hintText: 'Email',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorWhite)),
                        onChanged: (value) {
                          print(value);
                          _email = value;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        cursorColor: _colorWhite,
                        style: TextStyle(color: _colorWhite),
                        obscureText: true,
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.lock,
                              color: _colorWhite,
                              size: 18,
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorWhite)),
                        onChanged: (value) {
                          print(value);
                          _password = value;
                        },
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Checkbox(
                              activeColor: _colorWhite,
                              checkColor: _colorB,
                              value: isChecked,
                              onChanged: (value) {
                                setState(() {
                                  isChecked = value;
                                });
                              },
                            ),
                            Text(
                              'J\'accepte les termes du système et la',
                              style: TextStyle(color: _colorWhite),
                            )
                          ],
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 30),
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            isChecked == false ? "$errorCheck" : "",
                            style: TextStyle(
                                color: Colors.amber,
                                fontSize: 12,
                                fontWeight: FontWeight.w300),
                          )),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        alignment: Alignment.bottomRight,
                        child: Text(
                          "$_errorMessage",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10.0),
                        alignment: Alignment.bottomRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text('Already have an account?',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: _colorWhite,
                                    fontSize: 13)),
                            GestureDetector(
                              onTap: () => Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                      builder: (_) => SignInScreen(
                                            appBloc: widget.appBloc,
                                          ))),
                              child: Container(
                                height: 15,
                                width: 60,
                                margin: EdgeInsets.only(left: 5, right: 0),
                                child: Text('Sign in',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: _colorWhite,
                                        fontSize: 13)),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20, left: 20, right: 20,bottom: 130),
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        isLoading = true;
                      });
                      _appBloc
                          .postSignup(_username, _email, _password)
                          .then((String message) async {
                        setState(() {
                          isChecked = true;
                          isLoading = false;
                        });

                        while (isChecked == false) {
                          print("aaa");
                        }
                        if (message == "Sign up successfully") {
                          // _appBloc.getAccessToken();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => TabsScreen(
                                      accessToken: _appBloc.getAccessToken())));
                        } else
                          setState(() {
                            isLoading = false;
                            _errorMessage = message;
                          });
                      });
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          color: _colorWhite,
                          borderRadius: BorderRadius.circular(6)),
                      child: Center(
                          child: Text(
                        'Create Account',
                        style: TextStyle(
                            color: _colorB,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      )),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
