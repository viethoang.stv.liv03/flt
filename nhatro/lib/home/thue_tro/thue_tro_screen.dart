import 'package:flutter/material.dart';
import 'package:nhatro/home/thue_tro/tao_phong.dart';
import 'package:nhatro/services/call.dart';
import 'package:nhatro/services/service.dart';

class ThueTroScreen extends StatefulWidget {
  @override
  _ThueTroScreenState createState() => _ThueTroScreenState();
}

class _ThueTroScreenState extends State<ThueTroScreen> {
  final CallsAndMessagesService _service = locator<CallsAndMessagesService>();
  Color _colorApp = Color.fromARGB(255, 45, 53, 110);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: RaisedButton(
              color: Colors.grey[400],
              child: Text('Đăng phòng trọ'),
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => CreateRoomScreen()));
              },
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            color: Colors.white,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  // color: Colors.grey,
                  // height: 50,
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            'https://www.kairoscanada.org/wp-content/uploads/2017/01/mountain-300x225.jpeg'),
                        radius: 15,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('Abc'),
                      )
                    ],
                  ),
                ),
                Container(
                  // color: Colors.blue,
                  padding: EdgeInsets.all(10),
                  child: Image.asset(
                    'images/logo.jpg',
                    fit: BoxFit.fill,
                    width: double.infinity,
                    height: 200.0,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5, right: 10),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.home),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Nhà trọ"),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Icon(Icons.attach_money,color: Colors.red,),
                          Text(
                            "1000000đ / 1 tháng",
                            style: TextStyle(color: Colors.red, fontSize: 15,fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.supervisor_account),
                      SizedBox(
                        width: 10,
                      ),
                      Text("3")
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.border_color),
                      SizedBox(
                        width: 10,
                      ),
                      Text("20m2")
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.location_on),
                      SizedBox(
                        width: 10,
                      ),
                      Text("143 Phan Bội Châu - TP.HUẾ")
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.info),
                      SizedBox(
                        width: 10,
                      ),
                      Text("Wifi, vệ sinh trong, tự do, ...")
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 5),
                  alignment: Alignment.bottomLeft,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.phone),
                      SizedBox(
                        width: 10,
                      ),
                      Text("0123456789")
                    ],
                  ),
                ),
                Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      color: _colorApp,
                      child: Text(
                        'Liên hệ',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        dialogButton(context);
                      },
                    )),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
  Future<bool> dialogButton(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            titlePadding: EdgeInsets.all(1),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            contentPadding: EdgeInsets.all(4),
            title: ListTile(
                onTap: () {},
                title: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                      height: 30,
                      child: Text('Liên hệ'),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: FlatButton(
                              child: Text(
                                'Gọi',
                                style: TextStyle(color: _colorApp),
                              ),
                              onPressed: () {
                                _service.call('0123456789');
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                          Text('|'),
                          Container(
                            child: FlatButton(
                              child: Text(
                                'Nhắn tin',
                                style: TextStyle(color:_colorApp),
                              ),
                              onPressed: () {
                               _service.sendSms('0123456789');
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        ]),
                  ],
                )),
          );
        });
  }
}
