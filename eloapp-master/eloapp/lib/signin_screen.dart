import 'package:eloapp/screen2_ranking.dart';
import 'package:eloapp/screen2and5_bottombar.dart';
import 'package:eloapp/services/authentication.dart';
import 'package:eloapp/signup_screen.dart';
// import 'package:eloapp/skip.dart';
import 'package:eloapp/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:eloapp/after_register.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  Color _color = Colors.white;
  List<Color> _listColor = [
    Color.fromARGB(200, 0, 203, 230),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 203, 230),
  ];

  BaseAuth auth = Auth();
  String _errorMessage;
  bool _isLoading = false;

  String _email;
  String _password;

int currentPage = 0;
  bool lastPage = false;

  @override
  Widget build(BuildContext context) {
    Widget _showCircularProgress() {
      if (_isLoading) {
        print('aaaaaaaaaaaaaaaa');
        return Center(child: CircularProgressIndicator());
      }
      return Container(
        height: 0.0,
        width: 0.0,
      );
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.cyan,
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: _listColor,
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 50),
                  alignment: Alignment.topCenter,
                  child: Text(
                    'Sign In',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: _color),
                  ),
                ),
                Align(
                  child: Image.asset('images/logo.png'),
                  alignment: Alignment(0, -0.7),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
               
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextField(
                        cursorColor: _color,
                        style: TextStyle(color: _color),
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/username_icon.png',
                              height: 19.44,
                            ),
                            hintText: 'User Name',
                            hintStyle: TextStyle(fontWeight: FontWeight.w200)),
                        onChanged: (value) {
                          print(value);
                          _email = value;
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        cursorColor: _color,
                        style: TextStyle(color: _color),
                        obscureText: true,
                        decoration: InputDecoration(
                            icon: Image.asset(
                              'images/password_icon.png',
                              width: 17.61,
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(fontWeight: FontWeight.w200)),
                        onChanged: (value) {
                          print(value);
                          _password = value;
                        },
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 5, left: 80),
                          alignment: Alignment.bottomRight,
                          child: Text(
                            "${_errorMessage ?? ""}",
                            style: TextStyle(
                                color: Colors.grey[300],
                                fontSize: 10,
                                fontWeight: FontWeight.w300),
                          ))
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 40, left: 20, right: 20),
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                    onTap: () async {
                      setState(() {
                        _isLoading = true;
                      });
                      print('sign innnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn');
                      String userId = "";
                      try {
                        userId = await auth.signIn(_email, _password);
                        print('Signed in: $userId');
                        setState(() {
                          _isLoading = false;
                        });
                        if (userId.length > 0 && userId != null) {
                         
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (_) => SplashScreen()));
                        }
                      } catch (e) {
                        print('Error: $e');
                        setState(() {
                          _isLoading = false;

                          _errorMessage = e.message;
                        });
                      }
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Color.fromARGB(1000, 77, 208, 225),
                          borderRadius: BorderRadius.circular(2)),
                      child: Center(
                          child: Text(
                        'Sign In',
                        style: TextStyle(color: _color,fontWeight: FontWeight.w900,),
                      )),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 15),
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Dont\'t have an account?',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: _color,
                            fontSize: 11),
                      ),
                      GestureDetector(
                        onTap: () => Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (_) => SignUpScreen())),
                        child: Container(
                          height: 12,
                          width: 40,
                          margin: EdgeInsets.only(left: 15, right: 5),
                          decoration: BoxDecoration(
                              border:
                                  Border(bottom: BorderSide(color: _color))),
                          child: Text('Sign Up',
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: _color,
                                  fontSize: 11,decoration: TextDecoration.underline,)),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          _isLoading
              ? Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.height)
              : Container(
                  height: 0.0,
                  width: 0.0,
                ),
          _showCircularProgress()
        ],
      ),
    );
  }
}
