import 'package:eloapp/screen2and5_bottombar.dart';
import 'package:flutter/material.dart';

class Screen3Challenge extends StatefulWidget {
  @override
  _Screen3ChallengeState createState() => _Screen3ChallengeState();
}

class _Screen3ChallengeState extends State<Screen3Challenge> {
  TabController _controller;
  Color _color = Colors.white;
  Color _colorSub = Colors.grey[400];

  Widget _container(String title, String sub) => Container(
        height: 44,
        padding: EdgeInsets.only(
            left: 16, right: MediaQuery.of(context).size.width - 351),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey[200])),
            color: _color),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title),
            Row(
              children: <Widget>[
                Text(
                  sub,
                  style: TextStyle(color: Colors.grey[400]),
                ),
                Icon(
                  Icons.chevron_right,
                  color: Colors.grey[400],
                )
              ],
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            'Challenge',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),
          ),
        ),
        centerTitle: true,
        elevation: 0,
        bottom: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 15),
          child: Container(
            color: _color,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          _container('Style', 'Gi'),
          _container('Rules', 'Gi'),
          _container('Day', '02/10/2019'),
          _container('Location', 'USA'),
          Container(
            height: 100,
            padding: EdgeInsets.only(
                left: 16, right: MediaQuery.of(context).size.width - 351),
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey[200])),
                color: _color),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Comment'),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Gourmet cooking is a style of food preparation that deals with the finest and freshest possible ingredients. ',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                ),
                SizedBox(height: 20,)
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 200, left: 22, right: 22),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // crossAxisAlignment: C,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    print('cancellllll');
                  },
                  child: Container(
                    height: 54,
                    width: 149,
                    decoration: BoxDecoration(
                        color: Color.fromARGB(1000, 77, 208, 225),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                        child: Text(
                      'Cancel',
                      style: TextStyle(color: _color),
                    )),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Screen2and5()));
                },
                  child: Container(
                    height: 54,
                    width: 149,
                    decoration: BoxDecoration(
                        color: Color.fromARGB(1000, 77, 208, 225),
                        borderRadius: BorderRadius.circular(4)),
                    child: Center(
                        child: Text(
                      'Send',
                      style: TextStyle(color: _color),
                    )),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
