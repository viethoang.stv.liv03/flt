import 'package:eloapp/screen4_player.dart';
import 'package:flutter/material.dart';

class Screen2Ranking extends StatefulWidget {
  @override
  _Screen2RankingState createState() => _Screen2RankingState();
}

class _Screen2RankingState extends State<Screen2Ranking>
    with SingleTickerProviderStateMixin {
  Color _color = Colors.white;
  TabController _controller;

  @override
  void initState() {
    _controller = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  int _listRankingLength = 10;

  List<String> _listNameRanking = [
    'Charlotte Thomas',
    'Cordelia Caldwell',
    'Nellie Powers',
    'Steven Bridges',
    'Virgie Abbott',
    'Cora Townsend',
    'Nancy Rodriguez',
    'Ophelia Flores',
    'Walter Bates',
    'Isabella Fields'
  ];
  List<String> _listAvatarRanking = [
    'images/screen3_avatar1.png',
    'images/screen3_avatar2.png',
    'images/screen3_avatar3.png',
    'images/screen3_avatar4.png',
    'images/screen3_avatar2.png',
    'images/screen3_avatar3.png',
    'images/screen3_avatar1.png',
    'images/screen3_avatar1.png',
    'images/screen3_avatar4.png',
    'images/screen3_avatar1.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 65),
          child: Container(
            color: _color,
          ),
        ),
        title: Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            'Ranking',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w400),
          ),
        ),
        centerTitle: true,
        elevation: 0,
        flexibleSpace: CustomPaint(
          painter: CurvePainter(),
                  child: Container(
            margin: EdgeInsets.only(top: 105),
            height: 41,
            child: TabBar(
              indicatorColor: Colors.grey[600],
              indicatorWeight: 3,
              indicatorPadding: EdgeInsets.only(bottom: 0, top: 5),
              unselectedLabelColor: Colors.grey,
              labelStyle: TextStyle(fontWeight: FontWeight.w300, fontSize: 15),
              unselectedLabelStyle:
                  TextStyle(fontWeight: FontWeight.w300, fontSize: 15),
              controller: _controller,
              tabs: <Widget>[
                Tab(
                  text: 'Gi',
                ),
                Tab(
                  text: 'No Gi',
                )
              ],
            ),
          ),
        ),
      ),
      body: Container(
        child: TabBarView(
          controller: _controller,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 0),
              child: ListView.builder(
                itemCount: _listNameRanking.length,
                itemBuilder: (context, index) {
                  return Container(
                      color: _color,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                                color: Colors.grey[200], width: 0.5),
                          ),
                        ),
                        child: ListTile(
                          onTap: () =>
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) => Screen4Player(
                                        name: _listNameRanking[index],
                                      ))),
                          trailing: index == 0
                              ? Image.asset(
                                  'images/screen3_goldmedal.png',
                                  height: 43.17,
                                  width: 21,
                                )
                              : index == 1
                                  ? Image.asset(
                                      'images/screen3_sivelmedal.png',
                                      height: 43.17,
                                      width: 21,
                                    )
                                  : index == 2
                                      ? Image.asset(
                                          'images/screen3_bronzemedal.png',
                                          height: 43.17,
                                          width: 21,
                                        )
                                      : SizedBox(),
                          contentPadding: EdgeInsets.only(
                              left: 19,
                              right: MediaQuery.of(context).size.width - 339),
                          leading: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            height: 40,
                            width: 40,
                            child: Image.asset(_listAvatarRanking[index]),
                          ),
                          title: Text(_listNameRanking[index]),
                        ),
                      ));
                },
              ),
            ),
            Container(
              color: Colors.grey,
            )
          ],
        ),
      ),
    );
  }
}
// 
class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.grey[200];
    paint.strokeWidth = 3;
    canvas.drawLine(
      Offset(0, size.height-1.5),
      Offset(size.width, size.height-1.5),
      paint,
    );

    // paint.color = Colors.blue;
    // paint.style = PaintingStyle.stroke;
    // canvas.drawCircle(
    //     Offset(size.width / 2, size.height / 2), size.width / 4, paint);

    // paint.color = Colors.green;

    // var path = Path();
    // path.moveTo(size.width / 3, size.height * 3 / 4);
    // path.lineTo(size.width / 2, size.height * 5 / 6);
    // path.lineTo(size.width * 3 / 4, size.height * 4 / 6);
    // path.close();

    // paint.style = PaintingStyle.fill;

    // canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
