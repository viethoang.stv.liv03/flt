import 'package:eloapp/screen4_player.dart';
import 'package:flutter/material.dart';

class Screen5UserProfilePaper extends StatefulWidget {
  final Widget child;

  Screen5UserProfilePaper({Key key, this.child}) : super(key: key);

  _Screen5UserProfilePaperState createState() =>
      _Screen5UserProfilePaperState();
}

class _Screen5UserProfilePaperState extends State<Screen5UserProfilePaper> {
  var name = 'Nathan Robbins';

  _itemChallenge(String challenge) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => Screen4Player(
                name: name,
              ))),
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 10, left: 12, right: 12),
        height: 50,
        width: 341,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 5),
                child: Text(
                  'Challenge',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 5),
                child: Text(
                  challenge,
                  style: TextStyle(fontSize: 15),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.grey[50],
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(70)),
                          margin: EdgeInsets.only(top: 30),
                          child: Image.asset(
                            'images/Mask.png',
                            height: 129,
                            width: 129,
                          )),
                      // Positioned(
                      //   top: 70,
                      //   child: Container(
                      //     width: 130,
                      //     color: Colors.transparent,
                      //     height: 50,
                      //   ),
                      // ),
                      Positioned(
                        bottom: 0,
                        child: Opacity(
                          opacity: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Color.fromARGB(140, 13, 13, 13),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(100),
                                  bottomRight: Radius.circular(100)),
                            ),
                            height: 60,
                            width: 125,
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 18,
                          child: Text(
                            'Edit',
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ))
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text(
                      name,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 3, right: 3),
                          child: Image.asset(
                            'images/star.png',
                            height: 29.90,
                            width: 30,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 3, right: 3),
                          child: Image.asset(
                            'images/star.png',
                            height: 29.90,
                            width: 30,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 3, right: 3),
                          child: Image.asset(
                            'images/star.png',
                            height: 29.90,
                            width: 30,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 3, right: 3),
                          child: Image.asset(
                            'images/star.png',
                            height: 29.90,
                            width: 30,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 3, right: 3),
                          child: Image.asset(
                            'images/star.png',
                            height: 29.90,
                            width: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            _itemChallenge('Status'),
            _itemChallenge('Status'),
            _itemChallenge('Status'),
            _itemChallenge('Status'),
            _itemChallenge('Status'),
          ],
        ),
      ),
    );
  }
}

class CurvePainter extends CustomPainter {
  double height = 0;
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Color.fromARGB(140, 13, 13, 13);
    paint.strokeWidth = 1;
    // double dx = 3;
    canvas.drawCircle(Offset(size.width / 2, -85), 65, paint);
    // canvas.draw
    // for (double index = 0; index <= 40; index++) {
    //   if (index < 10) {
    //     canvas.drawLine(
    //       Offset(dx, index),
    //       Offset(size.width - dx, index),
    //       paint,
    //     );
    //     dx += 0.5;
    //   } else if (index < 20) {
    //     canvas.drawLine(
    //       Offset(dx, index),
    //       Offset(size.width - dx, index),
    //       paint,
    //     );
    //     dx += 0.7;
    //   } else if (index < 30) {
    //     canvas.drawLine(
    //       Offset(dx, index),
    //       Offset(size.width - dx, index),
    //       paint,
    //     );
    //     dx += 1.2;
    //   } else if (index < 35) {
    //     canvas.drawLine(
    //       Offset(dx, index),
    //       Offset(size.width - dx, index),
    //       paint,
    //     );
    //     dx += 1.8;
    //   }else {
    //     canvas.drawLine(
    //       Offset(dx, index),
    //       Offset(size.width - dx, index),
    //       paint,
    //     );
    //     dx += 2.5;
    //   }
    // }
    // canvas.drawLine(
    //   Offset(3, height),
    //   Offset(size.width - 3, height),
    //   paint,
    // );
    // canvas.drawLine(
    //   Offset(3.5, 1),
    //   Offset(size.width - 3.5, 1),
    //   paint,
    // );
    // canvas.drawLine(
    //   Offset(4, 2),
    //   Offset(size.width - 4, 2),
    //   paint,
    // );
    // canvas.drawLine(
    //   Offset(4.5, 3),
    //   Offset(size.width - 4.5, 3),
    //   paint,
    // );
    // canvas.drawLine(
    //   Offset(5, 4),
    //   Offset(size.width - 5, 4),
    //   paint,
    // );
    // paint.color = Colors.red;
    // paint.style = PaintingStyle.stroke;
    // canvas.drawCircle(Offset(size.width / 2, size.height / 2), 60, paint);
    // canvas.drawArc(Rect.fromCircle(center: Offset(size.width / 2, size.height / 2),radius: 60),-3.14/2,10.0,false,paint);
    // paint.color = Colors.blue;
    // paint.style = PaintingStyle.stroke;
    // canvas.drawCircle(
    //     Offset(size.width / 2, size.height / 2), size.width / 4, paint);

    // paint.color = Colors.green;

    // var path = Path();
    // path.moveTo(size.width / 3, size.height * 3 / 4);
    // path.lineTo(size.width / 2, size.height * 5 / 6);
    // path.lineTo(size.width * 3 / 4, size.height * 4 / 6);
    // path.close();

    // paint.style = PaintingStyle.fill;

    // canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
