import 'dart:async';
import 'package:eloapp/after_register.dart';
import 'package:eloapp/screen2and5_bottombar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  startTime() async {
    var _duration = new Duration(milliseconds: 10);
    return Timer(_duration, navigationPage);
  }

  Future navigationPage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    int count = (sharedPreferences.getInt('count') ?? 0);
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseUser user = await _auth.currentUser();
    if (user.uid != null) {
      if (count == 1) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) => AfterRegister()));
        setState(() {
          count = 1;
        });
      } else {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (_) => Screen2and5()));
       
        await sharedPreferences.setInt('count', count);
      }
     
    }
  }
 

  @override
  void initState() {
    super.initState();
    startTime();
//    navigationPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      body: Center(
        child: Container(
          child: Image.asset('images/logo.png'),
          margin: EdgeInsets.only(left: 20, right: 20),
        ),
      ),
    );
  }
}
