import 'package:eloapp/signin_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:eloapp/services/authentication.dart';
import 'package:eloapp/screen2and5_bottombar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class AfterRegister extends StatefulWidget {
  final Widget child;
  FirebaseUser user;
  String userID;
  String userName;

  AfterRegister({Key key, this.child, this.user, this.userID, this.userName})
      : super(key: key);
  @override
  AfterRegisterState createState() => AfterRegisterState();
}

enum Answers { a, b, c, d, e, f }
enum Answers1 { a1, b1, c1, d1, e1, f1 }

class AfterRegisterState extends State<AfterRegister> {
  Color _color = Colors.white;
  Color _colorGrey = Colors.grey;
  Color _colorBlack54 = Colors.black54;
  Color _colorBlack87 =Colors.black87;
  List<Color> _listColor = [
    Color.fromARGB(200, 0, 203, 230),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 203, 230),
  ];

  BaseAuth auth = Auth();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  String _firstName;
  String _lastName;
  String _birthday="Enter your birthday";
  String _age;
  String _height;
  String _weight;
  String _insName;
  String _schoolName;
  String _schoolAdd;
  bool _isLoading;
  String _errorMessage;

  String _nogi = 'Enter your NoGi';
  // DateTime date;
  void _setValue(String nogi) => setState(() => _nogi = nogi);
  Future _askNogi() async {
    switch (await showDialog(
      context: context,
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        child: SimpleDialog(
          title: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        'How many year of total grappling',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 15),
                      child: Text(
                        'experience do you have?(Please',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(bottom: 10, left: 5),
                      padding: EdgeInsets.only(right: 15),
                      child: Text(
                        ' include all grappling styles such',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(bottom: 10, left: 5),
                      padding: EdgeInsets.only(right: 40),

                      child: Text(
                        ' as judo, wrestling and BJJ)',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 30,
                  ),
                  child: IconButton(
                    icon: Icon(
                      Icons.close,
                      size: 18,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
          // new Text("How many year of total grappling experience do you have?(Please include all grappling styles such as judo, wrestling and BJJ)",style: TextStyle(fontSize: 15),),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text(
                "Novice (Under 1 year)",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.a);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Beginer (Between 1-2 years)",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.b);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Low Intermediate (Between 2-3 years)",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.c);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "High Intermediate (Between 3-5 years)",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.d);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Advanced (Over 5)",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.e);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "I will not compete in Gi",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers.f);
              },
            ),
          ],
        ),
      ),
    )) {
      case Answers.a:
        _setValue('Novice (Under 1 year)');
        break;
      case Answers.b:
        _setValue("Beginer (Between 1-2 years");
        break;
      case Answers.c:
        _setValue("Low Intermediate (Between 2-3 years)");
        break;
      case Answers.d:
        _setValue("High Intermediate (Between 3-5 years)");
        break;
      case Answers.e:
        _setValue("Advanced (Over 5)");
        break;
      case Answers.f:
        _setValue("I will not  compete in Gi");
        break;
    }
  }

  String _gi = 'Enter your Gi';

  void _setGi(String gi) => setState(() => _gi = gi);
  Future _askGi() async {
    switch (await showDialog(
      context: context,
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
        child: SimpleDialog(
          title: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              // mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right: 10),
                      child: Text(
                        'What belt do you currently hold in',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 150),
                      child: Text(
                        'BJJ?',
                        style: TextStyle(fontSize: 11),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(
                    bottom: 30,
                  ),
                  child: IconButton(
                    icon: Icon(
                      Icons.close,
                      size: 18,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
          children: <Widget>[
            new SimpleDialogOption(
              child: new Text(
                "White",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.a1);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Blue",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.b1);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Purple",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.c1);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Brown",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.d1);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "Black",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.e1);
              },
            ),
            SizedBox(
              height: 10,
            ),
            new SimpleDialogOption(
              child: new Text(
                "I will not  compete in Gi",
                style: TextStyle(fontSize: 12),
              ),
              onPressed: () {
                Navigator.pop(context, Answers1.f1);
              },
            ),
          ],
        ),
      ),
    )) {
      case Answers1.a1:
        _setGi('White');
        break;
      case Answers1.b1:
        _setGi("Blue");
        break;
      case Answers1.c1:
        _setGi("Purple");
        break;
      case Answers1.d1:
        _setGi("Brown");
        break;
      case Answers1.e1:
        _setGi("Black");
        break;
      case Answers1.f1:
        _setGi("I will not compete in Gi");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.grey[200],
        title: Center(
          child: Text('Profile'),
        ),
      ),
      backgroundColor: Colors.grey[200],
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                // gradient: LinearGradient(
                //     colors: _listColor,
                //     begin: Alignment.topCenter,
                //     end: Alignment.bottomCenter)
                ),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10),
                      color: _color),
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: ListView(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'First Name',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your first name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _firstName = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Last Name',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorBlack54,
                        style: TextStyle(color: _colorBlack54),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your last name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _lastName = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Birthday',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      Container(
                        height: 30,
                        decoration: BoxDecoration(color: _color),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Text(
                                _birthday
                                // date.day.toString() +
                                //         '/' +
                                //         date.month.toString() +
                                //         '/' +
                                //         date.year.toString()
                                // 'Enter your birthday'
                                ,
                                style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    color: _colorBlack87,
                                    fontSize: 15),
                              ),
                            ),
                            Container(
                              child: IconButton(
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 15,
                                onPressed: () {
                                  DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime: DateTime(1950, 1, 1),
                                      maxTime: DateTime(2100, 12, 30),
                                      theme: DatePickerTheme(
                                          cancelStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16),
                                          backgroundColor: Colors.white,
                                          itemStyle: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                          doneStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16)),

                                      //     onChanged: (date) {
                                      //   print(date.day.toString()+'/'+date.month.toString()+'/'+date.year.toString());
                                      // },
                                      onChanged: (DateTime date) {
                                        setState(() {
                                         _birthday= date.day.toString() +
                                        '/' +
                                        date.month.toString() +
                                        '/' +
                                        date.year.toString();
                                        });

                                      },
                                  //     onConfirm: (date) async {
                                  //   print('confirm $date');
                                    

                                  //   print(date.day.toString() +
                                  //       '/' +
                                  //       date.month.toString() +
                                  //       '/' +
                                  //       date.year.toString());
                                  // },
                                      currentTime: DateTime.now(),
                                      locale: LocaleType.en);
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'NoGi Experience',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      Container(
                        height: 30,
                        decoration: BoxDecoration(color: _color),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Text(
                                _nogi
                                // 'Enter your NoGi'
                                ,
                                style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    color: _colorBlack87,
                                    fontSize: 15),
                              ),
                            ),
                            Container(
                              child: IconButton(
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 15,
                                onPressed: () {
                                  _askNogi();
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Gi Experience',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      Container(
                        height: 30,
                        decoration: BoxDecoration(color: _color),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Text(
                                _gi
                                // 'Enter your Gi'
                                ,
                                style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    color: _colorBlack87,
                                    fontSize: 15),
                              ),
                            ),
                            Container(
                              child: IconButton(
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 15,
                                onPressed: () {
                                  _askGi();
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Height',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your Height',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _height = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Competition Weight',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        // obscureText: true,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your Competition Weight',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _weight = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'School Name',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        // obscureText: true,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your School Name',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _schoolName = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'School Address',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your School Address',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _schoolAdd = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          'Instructor Name',
                          style: TextStyle(color: _colorBlack87),
                        ),
                      ),
                      TextField(
                        cursorWidth: 1,
                        cursorColor: _colorGrey,
                        style: TextStyle(color: _colorBlack54),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 5, top: 5),
                            hintText: 'Enter your School Address',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w200,
                                color: _colorBlack87)),
                        onChanged: (value) {
                          print(value);
                          _insName = value;
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: 20, left: 20, right: 20, bottom: 20),
                        alignment: Alignment.bottomCenter,
                        child: GestureDetector(
                          onTap: () async {
                            setState(() {
                              _isLoading = true;
                            });
                            print('okkkkkkkkkkkk');

                            try {
                              FirebaseUser user = await _auth.currentUser();
                              String a=user.uid;
                              if (user.uid != null && user.uid != "") {
                                Firestore.instance
                                    .collection('Players')
                                    .document(a)
                                    .setData({
                                  // 'UID': user.uid,
                                  // 'Username': user.displayName,
                                  'First Name': _firstName,
                                  'Last Name': _lastName,
                                  'Instructor Name': _insName,
                                  'SchoolName': _schoolName,
                                  'Birthday':_birthday,
                                  'GiExperience': _gi,
                                  'School Address': _schoolAdd,
                                  // 'Age': _age,
                                  'Height': _height,
                                  'NoGiExperience': _nogi,
                                  'Weight': _weight,
                                  // 'Record': "",
                                  // 'Star Review': "",
                                });
                              }
                            } catch (e) {
                              print('Error: $e');
                              setState(() {
                                _isLoading = false;
                                _errorMessage = e.message;
                              });
                            }
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (_) => Screen2and5()));
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: 50,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(1000, 77, 208, 225),
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                                child: Text(
                              'Save',
                              style: TextStyle(color: _color),
                            )),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
