import 'package:eloapp/signin_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ELO',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.white, hintColor: Colors.white),
      home: SignInScreen(),
    );
  }
}


// import 'package:flutter/material.dart';
// import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

// void main() => runApp(new MyApp());

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return new MaterialApp(
//       title: 'Flutter Demo',
//       theme: new ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: new HomePage(),
//     );
//   }
// }

// class HomePage extends StatelessWidget {
//   String _a = "";
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Datetime Picker'),
//       ),
//       body: Center(
//         child: Column(
//           children: <Widget>[
//             FlatButton(
//                 onPressed: () {
//                   DatePicker.showDatePicker(context,
//                       showTitleActions: true,
//                       minTime: DateTime(1950, 1, 1),
//                       maxTime: DateTime(2100, 12, 30),
//                       theme: DatePickerTheme(
//                         cancelStyle: TextStyle(color: Colors.black, fontSize: 16),
//                           backgroundColor: Colors.white,
//                           itemStyle: TextStyle(
//                               color: Colors.black, fontWeight: FontWeight.bold),
//                           doneStyle:
//                               TextStyle(color: Colors.black, fontSize: 16)),
                              
//                   //     onChanged: (date) {
//                   //   print(date.day.toString()+'/'+date.month.toString()+'/'+date.year.toString());
//                   // }, 
//                   onConfirm: (date) {
//                     print('confirm $date');
//                     print(date.day.toString()+'/'+date.month.toString()+'/'+date.year.toString());
//                   }, currentTime: DateTime.now(), locale: LocaleType.en);
//                 },
//                 child: Text(
//                   'show date picker(custom theme &date time range)',
//                   style: TextStyle(color: Colors.blue),
//                 )),
//           ],
//         ),
//       ),
//     );
//   }
// }
