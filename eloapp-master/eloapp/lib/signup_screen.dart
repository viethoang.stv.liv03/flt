import 'package:eloapp/screen3_challenge.dart';
import 'package:eloapp/services/authentication.dart';
import 'package:eloapp/signin_screen.dart';
import 'package:eloapp/signup_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  var _controller = TextEditingController();
  Color _color = Colors.white;
  List<Color> _listColor = [
    Color.fromARGB(200, 0, 203, 230),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 135, 153),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 158, 179),
    Color.fromARGB(200, 0, 180, 204),
    Color.fromARGB(200, 0, 203, 230),
  ];

  String _userName;
  String _email;
  String _password;

  String _errorMessage;
  bool _isLoading = false;

  BaseAuth auth = Auth();

  @override
  Widget build(BuildContext context) {
    // final _formKey = new GlobalKey<FormState>();

    // bool _validateAndSave() {
    //   final form = _formKey.currentState;
    //   if (form.validate()) {
    //     form.save();
    //     return true;
    //   }
    //   return false;
    // }

    void _showVerifyEmailSentDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Verify your account"),
            content:
                new Text("Link to verify account has been sent to your email"),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Dismiss"),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => SignInScreen()));
                },
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.cyan,
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: _listColor,
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter)),
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 50),
              alignment: Alignment.topCenter,
              child: Text(
                'Sign Up',
                style: TextStyle(
                    fontWeight: FontWeight.w400, fontSize: 16, color: _color),
              ),
            ),
            Align(
              child: Image.asset('images/logo.png'),
              alignment: Alignment(0, -0.8),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextField(
                    // controller: _controller,
                    cursorColor: _color,
                    style: TextStyle(color: _color),
                    decoration: InputDecoration(
                        icon: Image.asset(
                          'images/username_icon.png',
                          height: 19.44,
                        ),
                        hintText: 'User Name',
                        hintStyle: TextStyle(fontWeight: FontWeight.w200)),
                    onChanged: (value) {
                      print(value);
                      _userName = value;
                    },
                    // validator: (value) =>
                    //     value.isEmpty ? 'User name can\'t be empty' : null,
                    // onSaved: (value) {_email = value;
                    // print(_controller.text);} ,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    keyboardType: TextInputType.emailAddress,
                    cursorColor: _color,
                    style: TextStyle(color: _color),
                    decoration: InputDecoration(
                        icon: Image.asset(
                          'images/email_icon.png',
                          height: 19.44,
                          width: 16.88,
                        ),
                        hintText: 'Email',
                        hintStyle: TextStyle(fontWeight: FontWeight.w200)),
                    // validator: (value) =>
                    //     value.isEmpty ? 'Email can\'t be empty' : null,
                    // onSaved: (value) => _email = value,
                    onChanged: (value) {
                      print(value);
                      _email = value;
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    cursorColor: _color,
                    style: TextStyle(color: _color),
                    obscureText: true,
                    decoration: InputDecoration(
                        icon: Image.asset(
                          'images/password_icon.png',
                          width: 17.61,
                        ),
                        hintText: 'Password',
                        hintStyle: TextStyle(fontWeight: FontWeight.w200)),
                    // validator: (value) =>
                    //     value.isEmpty ? 'Password can\'t be empty' : null,
                    // onSaved: (value) => _email = value,
                    onChanged: (value) {
                      print(value);
                      _password = value;
                    },
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 5),
                    alignment: Alignment.bottomRight,
                      child: Text(
                    "${_errorMessage ?? ""}",
                    style: TextStyle(color: Colors.grey[300], fontSize: 10,fontWeight: FontWeight.w300),
                  ))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 40, left: 20, right: 20),
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () async {
                  // setState(() {
                  //         _isLoading = false;
                  //       });
                  print('sign upppppppppppppppppppppppppp');
                  setState(() {
                    _errorMessage = "";
                    _isLoading = true;
                  });

                  
                  String userId = "";
                  try {
                    userId = await auth.signUp(_email, _password);
                    
                    
                    auth.sendEmailVerification();
                    _showVerifyEmailSentDialog();
                    print('Signed up user: $userId');
                    setState(() {
                      _isLoading = false;
                    });
                  } catch (e) {
                    print('Error: $e');
                    setState(() {
                      _isLoading = false;
                      _errorMessage = e.message;
                      print('Errorrrrrrrrr: $_errorMessage');
                    });
                  }
                  // }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(1000, 77, 208, 225),
                      borderRadius: BorderRadius.circular(2)),
                  child: Center(
                      child: Text(
                    'Sign Up',
                    style: TextStyle(color: _color,fontWeight: FontWeight.w900,),
                  )),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 15),
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Already have an account?',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: _color,
                        fontSize: 11),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => SignInScreen())),
                    child: Container(
                      height: 12,
                      width: 37,
                      margin: EdgeInsets.only(left: 15, right: 5),
                      decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: _color))),
                      child: Text('Sign In',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: _color,
                              fontSize: 11,
                              decoration: TextDecoration.underline,)),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

