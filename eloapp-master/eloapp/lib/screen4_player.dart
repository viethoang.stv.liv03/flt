import 'package:eloapp/screen3_challenge.dart';
import 'package:eloapp/screen5_user_profile_paper.dart';
import 'package:flutter/material.dart';

class Screen4Player extends StatefulWidget {
  final Widget child;
  String name;

  Screen4Player({Key key, this.child,this.name}) : super(key: key);

  _Screen4PlayerState createState() => _Screen4PlayerState();
}

class _Screen4PlayerState extends State<Screen4Player> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        color: Colors.grey[50],
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.grey[50],
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 30),
                              child: Image.asset(
                                'images/Mask.png',
                                height: 129,
                                width: 129,
                              )),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Text(
                              '${widget.name}',
                              style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 3, right: 3),
                                  child: Image.asset(
                                    'images/star.png',
                                    height: 29.90,
                                    width: 30,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 3, right: 3),
                                  child: Image.asset(
                                    'images/star.png',
                                    height: 29.90,
                                    width: 30,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 3, right: 3),
                                  child: Image.asset(
                                    'images/star.png',
                                    height: 29.90,
                                    width: 30,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 3, right: 3),
                                  child: Image.asset(
                                    'images/star.png',
                                    height: 29.90,
                                    width: 30,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 3, right: 3),
                                  child: Image.asset(
                                    'images/star.png',
                                    height: 29.90,
                                    width: 30,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.grey[50],
                      height: 103,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            color: Colors.white,
                            width: 94,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Age',
                                  style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text('34', style: TextStyle(fontSize: 15)),
                              ],
                            ),
                          ),
                          Container(
                            //  margin: EdgeInsets.only(left: 20),
                            color: Colors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Score',
                                    style: TextStyle(
                                      fontSize: 15,fontWeight: FontWeight.bold
                                    )),
                                SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('Gi',
                                            style: TextStyle(
                                              fontSize: 15,
                                            )),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('700',
                                            style: TextStyle(
                                              fontSize: 15,
                                            ))
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('No Gi',
                                            style: TextStyle(
                                              fontSize: 15,
                                            )),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text('894',
                                            style: TextStyle(
                                              fontSize: 15,
                                            ))
                                      ],
                                    ),
                                  ],
                                ),
                                //         SizedBox(
                                //    height: 5,
                                //  ),
                              ],
                            ),
                            width: 95,
                          ),
                          Container(
                            //  margin: ,
                            color: Colors.white,
                            width: 95,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'H: 1.80m',
                                  style: TextStyle(fontSize: 15),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'W: 75Kg',
                                  style: TextStyle(fontSize: 15),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 15, left: 15),
                      child: Text(
                        'Profile Information',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 15, left: 15),
                      child: Text(
                        'Gym',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 15, left: 15),
                      child: Text(
                        'City',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 15, left: 15),
                      child: Text(
                        'Affiliation',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 50, left: 25, right: 25),
              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Screen3Challenge()));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 54,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(1000, 77, 208, 225),
                      borderRadius: BorderRadius.circular(2)),
                  child: Center(
                      child: Text(
                    'Challenge',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
