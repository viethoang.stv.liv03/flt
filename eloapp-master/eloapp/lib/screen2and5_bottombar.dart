import 'package:eloapp/screen2_ranking.dart';
import 'package:eloapp/screen5_user_profile_paper.dart';
import 'package:eloapp/signin_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'presentation/my_flutter_app_icons.dart' as CustomIcon ;

class Screen2and5 extends StatefulWidget {
  // FirebaseUser user;
  // Screen2and5({Key key, this.user}) : super(key: key);
  @override
  _Screen2and5State createState() => _Screen2and5State();
}

class _Screen2and5State extends State<Screen2and5>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  TabController _controller;
// void _logout ()  {
// //  SharedPreferences sharedPreferences =  await SharedPreferences.getInstance();
//     int count = 1;
//     final FirebaseAuth _auth = FirebaseAuth.instance;
    
//     if(count==1) {
//       Navigator.of(context).pushReplacement(
//             MaterialPageRoute(builder: (_) => SignInScreen()));
//       setState(() {
//         count=0;
//       });
//     }
// }
  

  @override
  void initState() {
    _controller = TabController(length: 3, vsync: this);
  
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
   
    var iconHeight = 22.5;
    return Scaffold(
      bottomNavigationBar: Container(
        height: 60,
        child: TabBar(
          indicatorColor: Colors.white,
          labelColor: Color.fromARGB(1000, 77, 208, 225),
          unselectedLabelColor: Colors.grey[600],
          labelStyle: TextStyle(fontWeight: FontWeight.w400),
          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w400),
          controller: _controller,
          tabs: <Widget>[
            Tab(
              // icon: _controller.index==0 ? Image.asset('images/rank_chose.png',height: iconHeight,) : Image.asset('images/rank_not_chose.png',height: iconHeight,),
              icon: Icon(CustomIcon.MyFlutterApp.trophy,size: 24,),
              text: 'Ranking',
            ),
            Tab(
              text: 'Profile',
              // icon: Icon(Icons.person),
              icon: Icon(CustomIcon.MyFlutterApp.user,size: iconHeight),
              // icon: _controller.index==1 ? Image.asset('images/profile_chose.png',height: iconHeight,) : Image.asset('images/profile_not_chose.png',height: iconHeight),
            ),
            Tab(
              text: 'Setting',
              // icon: Image.asset('images/menu_not_chose.png',height: iconHeight),
              icon: Icon(CustomIcon.MyFlutterApp.menu,size: iconHeight),
              // icon: Icon(Icons.menu_elo),
            )
          ],
        ),
      ),
      body: TabBarView(
        controller: _controller,
        children: <Widget>[
          Screen2Ranking(),
          Screen5UserProfilePaper(),
          Container(
            
          )
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: _currentIndex,
      //   onTap: (int index) {
      //     setState(() {
      //       _currentIndex = index;
      //     });
      //   },
      //   items: [
      //     BottomNavigationBarItem(
      //         title: Text('Ranking'),
              // icon: Icon(Icons.people),
      //         activeIcon: Screen2Ranking()),
      //     BottomNavigationBarItem(
      //         title: Text('Profile'),
      //         icon: Icon(Icons.people_outline),
      //         activeIcon: Screen5UserProfilePaper()),
      //   ],
      // ),
    );
  }
}
